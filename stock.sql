/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50711
Source Host           : localhost:3306
Source Database       : stock

Target Server Type    : MYSQL
Target Server Version : 50711
File Encoding         : 65001

Date: 2016-08-01 06:04:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `category`
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `idcategory` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idcategory`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', 'Accessories');

-- ----------------------------
-- Table structure for `issue_detail`
-- ----------------------------
DROP TABLE IF EXISTS `issue_detail`;
CREATE TABLE `issue_detail` (
  `idissue_detail` int(11) NOT NULL AUTO_INCREMENT,
  `product` int(11) NOT NULL,
  `qty` varchar(45) DEFAULT NULL,
  `issuing_header` int(11) NOT NULL,
  `product_has_uom` int(11) NOT NULL,
  PRIMARY KEY (`idissue_detail`),
  KEY `fk_issue_detail_product1_idx` (`product`) USING BTREE,
  KEY `fk_issue_detail_issuing_header1_idx` (`issuing_header`) USING BTREE,
  KEY `fk_issue_detail_product_has_uom1_idx` (`product_has_uom`),
  CONSTRAINT `fk_issue_detail_product_has_uom1` FOREIGN KEY (`product_has_uom`) REFERENCES `product_has_uom` (`idproduct_has_uom`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `issue_detail_ibfk_1` FOREIGN KEY (`issuing_header`) REFERENCES `issuing_header` (`idissuing_header`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `issue_detail_ibfk_2` FOREIGN KEY (`product`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of issue_detail
-- ----------------------------
INSERT INTO `issue_detail` VALUES ('15', '87', '1', '15', '0');
INSERT INTO `issue_detail` VALUES ('16', '155', '2', '15', '0');
INSERT INTO `issue_detail` VALUES ('17', '124', '1', '15', '0');
INSERT INTO `issue_detail` VALUES ('18', '119', '8', '15', '0');
INSERT INTO `issue_detail` VALUES ('19', '156', '1', '15', '0');
INSERT INTO `issue_detail` VALUES ('20', '87', '1', '16', '0');
INSERT INTO `issue_detail` VALUES ('21', '155', '2', '16', '0');
INSERT INTO `issue_detail` VALUES ('22', '124', '1', '16', '0');
INSERT INTO `issue_detail` VALUES ('23', '119', '8', '16', '0');
INSERT INTO `issue_detail` VALUES ('24', '58', '2', '16', '0');
INSERT INTO `issue_detail` VALUES ('25', '63', '2', '16', '0');
INSERT INTO `issue_detail` VALUES ('26', '389', '1', '16', '0');
INSERT INTO `issue_detail` VALUES ('27', '390', '1', '16', '0');
INSERT INTO `issue_detail` VALUES ('28', '562', '1', '17', '0');
INSERT INTO `issue_detail` VALUES ('29', '304', '1', '17', '0');
INSERT INTO `issue_detail` VALUES ('30', '420', '1', '17', '0');
INSERT INTO `issue_detail` VALUES ('31', '87', '1', '18', '0');
INSERT INTO `issue_detail` VALUES ('32', '155', '2', '18', '0');
INSERT INTO `issue_detail` VALUES ('33', '124', '1', '18', '0');
INSERT INTO `issue_detail` VALUES ('34', '118', '8', '18', '0');
INSERT INTO `issue_detail` VALUES ('35', '3', '1', '18', '0');
INSERT INTO `issue_detail` VALUES ('36', '565', '', '19', '0');
INSERT INTO `issue_detail` VALUES ('37', '565', '1', '20', '0');
INSERT INTO `issue_detail` VALUES ('38', '421', '1', '21', '0');
INSERT INTO `issue_detail` VALUES ('44', '161', '1', '24', '0');
INSERT INTO `issue_detail` VALUES ('45', '87', '1', '25', '0');
INSERT INTO `issue_detail` VALUES ('46', '155', '2', '25', '0');
INSERT INTO `issue_detail` VALUES ('47', '124', '1', '25', '0');
INSERT INTO `issue_detail` VALUES ('48', '119', '8', '25', '0');
INSERT INTO `issue_detail` VALUES ('49', '245', '2', '26', '0');
INSERT INTO `issue_detail` VALUES ('50', '421', '1', '27', '0');
INSERT INTO `issue_detail` VALUES ('51', '81', '1', '28', '0');
INSERT INTO `issue_detail` VALUES ('52', '401', '1', '28', '0');
INSERT INTO `issue_detail` VALUES ('53', '399', '1', '28', '0');
INSERT INTO `issue_detail` VALUES ('54', '420', '1', '29', '0');
INSERT INTO `issue_detail` VALUES ('55', '302', '1', '29', '0');
INSERT INTO `issue_detail` VALUES ('56', '448', '2', '29', '0');
INSERT INTO `issue_detail` VALUES ('57', '453', '2', '30', '0');
INSERT INTO `issue_detail` VALUES ('58', '421', '1', '31', '0');
INSERT INTO `issue_detail` VALUES ('59', '157', '1', '32', '0');
INSERT INTO `issue_detail` VALUES ('60', '157', '1', '33', '0');
INSERT INTO `issue_detail` VALUES ('61', '32', '1', '34', '0');
INSERT INTO `issue_detail` VALUES ('62', '36', '1', '34', '0');
INSERT INTO `issue_detail` VALUES ('63', '582', '1', '35', '0');
INSERT INTO `issue_detail` VALUES ('64', '55', '1', '35', '0');
INSERT INTO `issue_detail` VALUES ('65', '427', '1', '35', '0');
INSERT INTO `issue_detail` VALUES ('66', '398', '1', '35', '0');
INSERT INTO `issue_detail` VALUES ('67', '87', '1', '35', '0');
INSERT INTO `issue_detail` VALUES ('68', '155', '2', '35', '0');
INSERT INTO `issue_detail` VALUES ('69', '124', '1', '35', '0');
INSERT INTO `issue_detail` VALUES ('70', '118', '8', '35', '0');
INSERT INTO `issue_detail` VALUES ('71', '3', '1', '35', '0');
INSERT INTO `issue_detail` VALUES ('72', '414', '1', '35', '0');
INSERT INTO `issue_detail` VALUES ('73', '423', '1', '36', '0');
INSERT INTO `issue_detail` VALUES ('74', '55', '1', '36', '0');
INSERT INTO `issue_detail` VALUES ('75', '421', '1', '37', '0');
INSERT INTO `issue_detail` VALUES ('76', '113', '2', '37', '0');
INSERT INTO `issue_detail` VALUES ('77', '62', '1', '37', '0');
INSERT INTO `issue_detail` VALUES ('78', '78', '1', '37', '0');
INSERT INTO `issue_detail` VALUES ('79', '411', '1', '37', '0');
INSERT INTO `issue_detail` VALUES ('80', '3', '15', '38', '1');

-- ----------------------------
-- Table structure for `issuing_header`
-- ----------------------------
DROP TABLE IF EXISTS `issuing_header`;
CREATE TABLE `issuing_header` (
  `idissuing_header` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `issue_no` varchar(45) DEFAULT NULL,
  `recieved_by` varchar(45) DEFAULT NULL,
  `vehicle_no` varchar(255) DEFAULT NULL,
  `Date_Added` datetime DEFAULT NULL,
  `Added_By` varchar(45) DEFAULT NULL,
  `Date_Updated` datetime DEFAULT NULL,
  `Updated_By` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `job_ref` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`idissuing_header`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of issuing_header
-- ----------------------------
INSERT INTO `issuing_header` VALUES ('15', '2016-06-25', '15:13:31', 'ISN000005\n', 'KK-9337', 'Asela', '2016-06-25 15:13:31', '1', '2016-06-25 15:13:31', '1', 'DONE', '');
INSERT INTO `issuing_header` VALUES ('16', '2016-07-06', '15:03:01', 'ISN000006\n', 'Dinusha', 'KH-0735', '2016-07-06 15:03:01', '1', '2016-07-06 15:03:01', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('17', '2016-07-06', '15:21:53', 'ISN000007\n', 'Menaka', 'KH-6450', '2016-07-06 15:21:53', '1', '2016-07-06 15:21:53', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('18', '2016-07-06', '15:22:37', 'ISN000008\n', 'Supun', 'KK-4118', '2016-07-06 15:22:37', '1', '2016-07-06 15:22:38', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('19', '2016-07-06', '15:43:40', 'ISN000009\n', 'Sumudu', 'GG-9424', '2016-07-06 15:43:40', '1', '2016-07-06 15:43:40', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('20', '2016-07-06', '15:44:09', 'ISN000010\n', 'Sumudu', 'GG-9424', '2016-07-06 15:44:09', '1', '2016-07-06 15:44:09', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('21', '2016-07-06', '15:44:53', 'ISN000011\n', 'Dinusha', 'KH-0735', '2016-07-06 15:44:53', '1', '2016-07-06 15:44:53', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('24', '2016-07-07', '15:12:45', 'ISN000012\n', 'Naveen', 'KK-5904', '2016-07-07 15:12:45', '1', '2016-07-07 15:12:45', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('25', '2016-07-07', '15:15:08', 'ISN000013\n', 'Dinusha & Asela', 'PF-5780', '2016-07-07 15:15:08', '1', '2016-07-07 15:15:08', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('26', '2016-07-07', '15:26:14', 'ISN000014\n', 'Dinusha', 'KI-0223', '2016-07-07 15:26:14', '1', '2016-07-07 15:26:14', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('27', '2016-07-07', '15:27:01', 'ISN000015\n', 'Asanka', 'KR-1275', '2016-07-07 15:27:01', '1', '2016-07-07 15:27:01', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('28', '2016-07-07', '15:31:29', 'ISN000016\n', 'Kumara', 'KJ-6751', '2016-07-07 15:31:29', '1', '2016-07-07 15:31:29', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('29', '2016-07-07', '15:37:49', 'ISN000017\n', 'Menaka', 'KH-6450', '2016-07-07 15:37:49', '1', '2016-07-07 15:37:49', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('30', '2016-07-07', '15:39:46', 'ISN000018\n', 'Naveen', 'KP-8554', '2016-07-07 15:39:46', '1', '2016-07-07 15:39:46', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('31', '2016-07-07', '15:40:32', 'ISN000019\n', 'Naveen', 'KH-9388', '2016-07-07 15:40:32', '1', '2016-07-07 15:40:32', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('32', '2016-07-07', '15:41:05', 'ISN000020\n', 'Asela', 'KH-4874', '2016-07-07 15:41:05', '1', '2016-07-07 15:41:05', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('33', '2016-07-07', '15:41:33', 'ISN000021\n', 'Asela', 'KK-1402', '2016-07-07 15:41:33', '1', '2016-07-07 15:41:33', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('34', '2016-07-07', '15:42:55', 'ISN000022\n', 'Naveen', 'KJ-4654', '2016-07-07 15:42:55', '1', '2016-07-07 15:42:55', '1', 'DONE', '4510');
INSERT INTO `issuing_header` VALUES ('35', '2016-07-08', '17:00:46', 'ISN000023\n', 'Asela & Chanaka', 'KK-8071', '2016-07-08 17:00:46', '1', '2016-07-08 17:00:46', '1', 'DONE', '4510');
INSERT INTO `issuing_header` VALUES ('36', '2016-07-08', '17:02:28', 'ISN000024\n', 'Kumara', 'KJ-6751', '2016-07-08 17:02:28', '1', '2016-07-08 17:02:28', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('37', '2016-07-08', '17:17:44', 'ISN000025\n', 'Dinusha, Supun & Dinusha', 'KK-8071', '2016-07-08 17:17:44', '1', '2016-07-08 17:17:44', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('38', '2016-07-31', '15:18:53', 'ISN000026\n', 'Akila', 'WP-GA-0022', '2016-07-31 15:18:53', '1', '2016-07-31 15:18:54', '1', 'DONE', '123456');

-- ----------------------------
-- Table structure for `migration`
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m000000_000000_base', '1465738643');
INSERT INTO `migration` VALUES ('m130524_201442_init', '1465738647');

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `idproduct` int(11) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(45) DEFAULT NULL,
  `product_name` varchar(45) DEFAULT NULL,
  `reorder_level` varchar(45) DEFAULT NULL,
  `unit_of_measure` varchar(45) DEFAULT NULL,
  `product_type` enum('Consumable','Service','Stockable') DEFAULT NULL,
  `barcode` varchar(45) DEFAULT NULL,
  `sub_category` int(11) NOT NULL,
  `availble_qty` int(11) DEFAULT NULL,
  `buying_price` decimal(10,2) DEFAULT NULL,
  `selling_price` decimal(10,2) DEFAULT NULL,
  `Added_By` varchar(45) DEFAULT NULL,
  `Date_Updated` datetime DEFAULT NULL,
  `Updated_By` varchar(45) DEFAULT NULL,
  `idwarehouse` int(11) NOT NULL,
  PRIMARY KEY (`idproduct`),
  KEY `fk_product_sub_category1_idx` (`sub_category`) USING BTREE,
  KEY `fk_product_warehouse1_idx` (`idwarehouse`) USING BTREE,
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`sub_category`) REFERENCES `sub_category` (`idsub_category`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `product_ibfk_2` FOREIGN KEY (`idwarehouse`) REFERENCES `warehouse` (`idwarehouse`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=585 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('3', 'A01', 'Air filter Kyron', '5', 'Box', 'Stockable', '', '1', '14', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('4', 'A02', 'Air filter Rexton', '5', 'Box', 'Stockable', '', '1', '3', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('5', 'M03', 'Air filter Rexton G', '', 'Box', 'Stockable', '', '1', '2', null, null, '', '0000-00-00 00:00:00', '', '2');
INSERT INTO `product` VALUES ('6', 'A04', 'Air filterAqua (JS) A - 21031', '5', 'Box', 'Stockable', '', '1', '5', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('7', 'A05', 'Air filter Honda -  GP5 (JS - 28013)', '5', 'Box', 'Stockable', '', '1', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('8', 'A06', 'Air filter Prius (JS -A- 1010) ', '2', 'Box', 'Stockable', '', '1', '2', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('9', 'A07', 'Air filter Toyota Prado (G)  (17801 - 30040)', '2', 'Box', 'Stockable', '', '1', '2', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('10', 'A08', 'Air filter Prado (JS -A-194 J)', '2', 'Box', 'Stockable', '', '1', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('11', 'A09', 'Air filter Sakura (A - 1181)', '2', 'Box', 'Stockable', '', '1', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('12', 'A10', 'Air filter Sakura (A - 2816)', '2', 'Box', 'Stockable', '', '1', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('13', 'A11', 'Air filter Sakura (A - 1174)', '', 'Box', 'Stockable', '', '1', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('14', 'A12', 'Air filter Sakura (A - 1180)', '', 'Box', 'Stockable', '', '1', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('15', 'A13', 'Air filter Sakura (A- 1123)', '', 'Box', 'Stockable', '', '1', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('16', 'A14', 'Air filter Wagon- R AZUMI (A- 22976)', '5', 'Box', 'Stockable', '', '1', '20', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('17', 'A15', 'Air Filter Korando(G)', '5', 'Box', 'Stockable', '', '1', '8', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('18', 'A16', 'Air filter KIA Sorento', '3', 'Box', 'Stockable', '', '1', '3', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('19', 'A17', 'Air Filter KIA Sportage', '3', 'Box', 'Stockable', '', '1', '4', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('20', 'A18', 'Air filter Tivoli', '3', 'Box', 'Stockable', '', '1', '3', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('21', 'A19', 'Air filter Free Lander 2 (Britpart)', '2', 'Box', 'Stockable', '', '1', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('22', 'A20', 'Air filter Free Lander 2 (G)', '2', 'Box', 'Stockable', '', '1', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('23', 'A21', 'Air filter Discover 4 (G)', '1', 'Box', 'Stockable', '', '1', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('24', 'A22', 'Air filter Discover 4 (Britpart)', '3', 'Box', 'Stockable', '', '1', '3', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('25', 'A23', 'Air filter Evoque (MANN)', '1', 'Box', 'Stockable', '', '1', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('26', 'A24', 'Brake pad set -Front Mando Kyron and Rexton', '5', 'Box', 'Stockable', '', '2', '12', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('27', 'A25', 'Rear brake pad set - Mando Kyron and Rexton', '5', 'Box', 'Stockable', '', '2', '10', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('28', 'A26', 'Rear brake pad set - TRW Kyron and Rexton', '', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('29', 'A27', 'Rear brake pad set - SB Kyron and Rexton', '', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('30', 'A28', 'Rear brake pad set - NIBK Kyron and Rexton', '', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('31', 'A29', 'Rear brake pad set  - HI-Q Kyron and Rexton', '5', 'Box', 'Stockable', '', '2', '9', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('32', 'A30', 'Front brake pad set - Mando korando & sorento', '5', 'Box', 'Stockable', '', '2', '6', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('33', 'A31', 'Rear brake pad set - Mando KIA Sorento', '', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('35', 'A32', 'Rear brake pad set - Mando Korando', '', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('36', 'A33', 'Rear brake pad set - HI-Q Korando', '5', 'Box', 'Stockable', '', '2', '22', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('37', 'A34', 'Front brake pad set Discover 4 - Britpart', '2', 'Box', 'Stockable', '', '2', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('38', 'A35', 'Rear brake pad set Discovery 4 - Britpart', '2', 'Box', 'Stockable', '', '2', '2', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('39', 'A36', 'Front brake pad set Discovery 4 - TRW', '1', 'Box', 'Stockable', '', '2', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('40', 'A37', 'Rear brake pad set Discovery 4 - TRW', '', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('41', 'A38', 'Front brake pad set Discovery 4 (G)', '', 'Box', 'Stockable', '', '2', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('42', 'A39', 'Front brake pad set -Free Lander 2 - Britpart', '', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('43', 'A40', 'Rear brake pad set - Free Lander 2 - Britpart', '', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('44', 'A41', 'Front brake pad set - Free Lander 2 - Ferado', '1', 'Box', 'Stockable', '', '2', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('45', 'A42', 'Rear brake pad set - Free Lander 2 - Ferado', '1', 'Box', 'Stockable', '', '2', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('46', 'A43', 'Front brake pad set - Free Lander 2 - Delphi', '1', 'Box', 'Stockable', '', '2', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('47', 'A44', 'Rear brake pad set - Free Lander 2 - Delphi', '', 'Box', 'Stockable', '', '2', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('48', 'A45', 'Front brak pad set - Evoque - Britpart', '', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('49', 'A46', 'Rear brake pad set - Evoque - Britpart', '', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('50', 'A47', 'Front brake pad set - Evoque - TRW', '', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('51', 'A48', 'Rear brake pad set - Evoque - TRW', '1', 'Box', 'Stockable', '', '2', '2', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('52', 'A49', 'Front brake pad set -Defender 2.4 - TRW ', '', 'Box', 'Stockable', '', '2', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('53', 'A50', 'Rear brake pad set - Defender 2.4 - TRW', '', 'Box', 'Stockable', '', '2', '2', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('54', '', 'H - 27 Fog bulb', '', 'Box', 'Stockable', '', '3', '11', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('55', '', 'H - 7 Dim light', '', 'Box', 'Stockable', '', '3', '31', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('56', '', 'H - 1 Head light', '', 'Box', 'Stockable', '', '3', '18', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('57', '', 'H - 4 Bulb', '', 'Box', 'Stockable', '', '3', '7', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('58', '', 'Double filament bulb', '', 'bulbs', 'Stockable', '', '3', '80', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('59', '', 'Single filament bulb', '', 'bulbs', 'Stockable', '', '3', '28', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('60', '', 'Double filament capless', '', 'bulbs', 'Stockable', '', '3', '11', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('61', '', 'Single filament capless', '', 'bulbs', 'Stockable', '', '3', '7', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('62', '', 'Hood Bulb', '', 'bulbs', 'Stockable', '', '3', '72', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('63', '', 'Capless parking bulb', '', 'bulbs', 'Stockable', '', '3', '163', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('64', '', 'Capless small', '', 'bulbs', 'Stockable', '', '3', '6', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('65', '', 'Capless medium', '', 'bulbs', 'Stockable', '', '3', '10', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('66', '', 'Capless orange', '', 'bulbs', 'Stockable', '', '3', '6', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('67', '', 'Dash board light', '', 'bulbs', 'Stockable', '', '3', '20', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('68', '', 'Signal bulb big', '', 'bulbs', 'Stockable', '', '3', '29', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('69', '', 'Signal bulb small', '', 'bulbs', 'Stockable', '', '3', '11', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('70', '', 'H - B3 Fog (LR)', '', 'bulbs', 'Stockable', '', '3', '6', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('71', '', 'H - 4 (LR)', '', 'bulbs', 'Stockable', '', '3', '10', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('72', '', 'HID H-7', '', 'bulbs', 'Stockable', '', '3', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('73', '', 'HID H-1', '', 'bulbs', 'Stockable', '', '3', '2', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('74', '', 'HID H - 1 Blaster', '', 'units', 'Stockable', '', '3', '2', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('76', '', '25 A - Big ', '', 'units', 'Stockable', '', '4', '13', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('78', '', '20 A - small ', '', 'units', 'Stockable', '', '4', '58', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('79', '', '20 A - Big', '', 'units', 'Stockable', '', '4', '7', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('80', '', '30 A - Big ', '', 'units', 'Stockable', '', '4', '18', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('81', '', '15 A- Small', '', 'units', 'Stockable', '', '4', '103', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('82', '', '7.5 A - Small', '', 'units', 'Stockable', '', '4', '84', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('83', '', '15 A - Big', '', 'units', 'Stockable', '', '4', '10', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('84', '', '10 A - Big', '', 'units', 'Stockable', '', '4', '27', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('86', '', '5 A - Big', '', 'units', 'Stockable', '', '4', '17', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('87', '', 'Oil filter Ssangyong', '10', 'Box', 'Stockable', '', '5', '8', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('88', '', 'Oil filter Korando', '', 'Box', 'Stockable', '', '5', '16', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('89', '', 'Oil filter Sorento', '5', 'Box', 'Stockable', '', '5', '14', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('90', '', 'Oil filter Sportage', '2', 'Box', 'Stockable', '', '5', '2', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('91', '', 'Oil filter (JS C-809J)', '5', 'Box', 'Stockable', '', '5', '17', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('92', '', 'Oil filter Honda (G) C-809', '5', 'Box', 'Stockable', '', '5', '15', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('93', '', 'Oil filter Waggon R (JS C-932)', '', 'Box', 'Stockable', '', '5', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('94', '', 'Oil filter Waggon R (G) C-932', '5', 'Box', 'Stockable', '', '5', '13', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('95', '', 'Oil filter Waggon R (Azumi - 932)', '5', 'Box', 'Stockable', '', '5', '9', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('96', '', 'Oil filter Toyota (G) - 110', '5', 'Box', 'Stockable', '', '5', '15', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('97', '', 'Oil filter Toyota (G) - 117', '', 'Box', 'Stockable', '', '5', '7', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('98', '', 'Oil filter Mitsubishi outlander (G)', '', 'Box', 'Stockable', '', '5', '2', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('99', '', 'Oil filter Mitsubishi Montero Sport (G)', '', 'Box', 'Stockable', '', '5', '2', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('100', '', 'Oil filter Nissan X-Trail (G)', '', 'Box', 'Stockable', '', '5', '4', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('101', '', 'Oil filter JS - C 415', '', 'Box', 'Stockable', '', '5', '5', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('102', '', 'Oil filter JS - C 111', '', 'Box', 'Stockable', '', '5', '2', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('103', '', 'Oil filter JS - OE 116 J', '', 'Box', 'Stockable', '', '5', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('104', 'M01', 'Air Filter KIA Sportage', '1', 'Box', 'Stockable', '', '1', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('105', '', 'Upper arm bush', '12', 'units', 'Stockable', '', '6', '28', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('106', '', 'Link mount bush', '10', 'Units', 'Stockable', '', '6', '23', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('107', '', 'Lower arm big bush', '12', 'Units', 'Stockable', '', '6', '24', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('108', '', 'Lower arm small bush', '12', 'Units', 'Stockable', '', '6', '24', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('324', '', 'Rear link - Power Up L/H', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('325', '', 'rear link - Power Up R/H', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('326', '', 'Return hose ', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('327', '', ' Rack boot', '', 'units', 'Stockable', '', '15', '10', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('328', '', 'Seperator hose ', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('329', '', 'shockabsober - Front - Kyron', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('330', '', 'shockabsober - Rear - Kyron', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('331', '', 'shockabsober - Front - Rexton', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('332', '', 'shockabsober -Rear - Rexton', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('333', '', 'Sun visor - Rex', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('338', '', 'Side mirror glass - Kyron - L/H', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('339', '', 'Side mirror glass - Kyron - R/H', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('340', '', 'Side mirror glass - Rexton - L/H', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('341', '', 'Side mirror glass - Rexton - R/H', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('342', '', 'Sump nut', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('343', '', 'Shock mount ', '', 'units', 'Stockable', '', '15', '6', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('344', '', ' Shock boot ', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('345', '', 'Striker assy T- gate', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('346', '', 'Screw plug', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('347', '', 'Selector lever bush', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('349', '', 'Timing chain tensioner', '', 'units', 'Stockable', '', '15', '15', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('350', '', 'Tie rod end - L/H', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('351', '', 'Tie rod end - R/H', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('352', '', 'Transponder immobilizer', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('353', '', 'Track rod - Power Up', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('354', '', 'Turbo inter cooler', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('355', '', 'Turbo hose', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('356', '', 'Timing chain ', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('357', '', 'Timing chain guard', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('358', '', 'Under guard - Kyron', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('359', '', 'Under guard - Rexton', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('360', '', 'Upper arm bush - Power Up', '', 'units', 'Stockable', '', '7', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('361', '', 'Vacum modulator ', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('362', '', 'Vacum hose - Inlet ', '', 'units', 'Stockable', '', '15', '12', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('363', '', 'Vacum hose - Outlet', '', 'units', 'Stockable', '', '15', '17', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('364', '', 'Vacum pump ', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('365', '', 'Vacum pump \'O\' ring', '', 'units', 'Stockable', '', '15', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('368', '', 'Wiper washer tank motor - Kyron', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('369', '', 'Wiper washer tank motor - Rexton', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('371', '', 'Washer (black)', '', 'units', 'Stockable', '', '15', '13', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('372', '', 'Injector washer', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('373', '', 'Wheel Cup - Kyron', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('374', '', 'Wheel Cup - Rexton', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('375', '', 'Radiator washer', '', 'units', 'Stockable', '', '15', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('376', '', 'Wheel nut', '', 'units', 'Stockable', '', '15', '6', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('377', '', 'Wheel arch cover - Rexton', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('378', '', 'Wiper arm front L/H', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('379', '', 'Washer and Tube (6011308000)', '', 'units', 'Stockable', '', '15', '20', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('380', '', 'Washer and Tube (6013308000)', '', 'units', 'Stockable', '', '15', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('381', '', 'Washer and Tube (6015309A00)', '', 'units', 'Stockable', '', '15', '20', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('382', '', 'Washer and Tube (6016308002)', '', 'units', 'Stockable', '', '15', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('383', '', 'Window regulator ', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('384', '', 'Wheel bolt', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('385', '', 'Wiper arm cap', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('387', '', '10 A - Small', '', 'units', 'Stockable', '', '4', '66', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('388', '', 'Hybrid fuse', '', 'units', 'Stockable', '', '4', '43', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('389', '', 'Wiper blade (Max clear) \"19\"', '', 'units', 'Stockable', '', '16', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('390', '', 'Wiper blade (Max clear) \"21\"', '', 'units', 'Stockable', '', '16', '0', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('391', '', 'Wiper blade (Max clear) \"16\"', '', 'units', 'Stockable', '', '16', '14', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('392', '', 'Wiper blade (Max clear) \"24\"', '', 'units', 'Stockable', '', '16', '8', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('393', '', 'Wiper blade (Max clear) \"26\"', '', 'units', 'Stockable', '', '16', '5', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('394', '', 'Wiper blade (Boneless) \"16\"', '', 'units', 'Stockable', '', '16', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('395', '', 'Wiper blade (Boneless) \"14\"', '', 'units', 'Stockable', '', '16', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('396', '', 'Wiper blade (KCW) \"525\"', '', 'units', 'Stockable', '', '16', '6', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('397', '', 'Wiper blade (KCW) \"450\"', '', 'units', 'Stockable', '', '16', '5', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('398', '', 'Rear wiper blade (G)', '', 'units', 'Stockable', '', '16', '8', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('399', '', 'Door lock motor - Rexton Front (L/H)', '', 'Box', 'Stockable', '', '17', '5', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('400', '', 'Door lock motor - Rexton Front (R/H)', '', 'Box', 'Stockable', '', '17', '6', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('401', '', 'Door lock motor - Rexton Rear (L/H)', '', 'Box', 'Stockable', '', '17', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('402', '', 'Door lock motor - Rexton Rear (R/H)', '', 'Box', 'Stockable', '', '17', '6', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('403', '', 'Door lock motor - Kyron Front (L/H)', '', 'units', 'Stockable', '', '17', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('404', '', 'Door lock motor - Kyron Front (R/H)', '', 'units', 'Stockable', '', '17', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('405', '', 'Door lock motor - Kyron Rear (L/H)', '', 'units', 'Stockable', '', '17', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('406', '', 'Door lock motor - Kyron Rear (R/H)', '', 'units', 'Stockable', '', '17', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('407', '', 'Door lock motor - Korando Front (L/H)', '', 'units', 'Stockable', '', '17', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('408', '', 'Door lock motor - Korando Front (R/H)', '', 'units', 'Stockable', '', '17', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('409', '', 'Door lock motor - Korando Rear (L/H)', '', 'units', 'Stockable', '', '17', '0', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('410', '', 'Door lock motor - Korando Rear (R/H)', '', 'units', 'Stockable', '', '17', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('411', '', 'Key housing', '', 'units', 'Stockable', '', '15', '16', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('412', '', 'Heater plug (NGK)', '', 'units', 'Stockable', '', '15', '7', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('413', '', 'Idle pully with groove - Kyron', '', 'Box', 'Stockable', '', '15', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('414', '', 'Idle pully without groove - Kyron', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('415', '', 'Brake light switch', '', 'Box', 'Stockable', '', '15', '12', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('416', '', 'Brake light switch wire', '', 'units', 'Stockable', '', '15', '13', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('417', '', 'Battery water bottle', '', 'bottles', 'Stockable', '', '15', '17', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('418', '', 'Oil cooler housing packing ', '', 'units', 'Stockable', '', '15', '30', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('419', '', 'Thermostat valve', '', 'Box', 'Stockable', '', '15', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('420', '', 'Thermostat valve \"O\" ring', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('421', '', 'Injector Cleaner (MAX 44)', '', 'Bottles', 'Stockable', '', '15', '74', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('422', '', 'Oil cooler hose - Inlet', '', 'units', 'Stockable', '', '15', '23', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('423', '', 'Tail light - Rexton', '', 'units', 'Stockable', '', '7', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('424', '', 'Oil lifter', '', 'units', 'Stockable', '', '15', '56', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('425', '', 'Injector assy - Kyron', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('426', '', 'Front link - Korando', '', 'units', 'Stockable', '', '7', '12', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('427', '', 'Fan belt - Kyron', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('428', '', 'Fan belt - Rexton', '', 'units', 'Stockable', '', '15', '7', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('429', '', 'Fan clutch', '', 'units', 'Stockable', '', '7', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('430', '', 'Gear lever bush - Big', '', 'units', 'Stockable', '', '7', '35', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('431', '', 'Gear lever bush - Small', '', 'units', 'Stockable', '', '7', '123', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('432', '', 'Gear lever bush - 4 speed', '', 'units', 'Stockable', '', '15', '9', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('536', '', 'Upper arm complete set - (R/H)', '', 'units', 'Stockable', '', '7', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('537', '', 'Tie rod end (L/H)', '', 'units', 'Stockable', '', '7', '6', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('538', '', 'Tie rod end (R/H)', '', 'units', 'Stockable', '', '7', '6', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('539', '', 'Front link (R/H)', '', 'units', 'Stockable', '', '7', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('540', '', 'Front \'D\' bush bracket', '', 'units', 'Stockable', '', '7', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('541', '', 'Rack mount', '', 'units', 'Stockable', '', '7', '7', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('542', '', 'Silancer mount - Rear ', '', 'units', 'Stockable', '', '15', '11', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('543', '', 'Silancer mount - Center', '', 'units', 'Stockable', '', '15', '10', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('544', '', 'Turbo - Kyron', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('545', '', 'Turbo - Rexton', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('546', '', 'Turbo - Rexton', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('547', '', 'Bonnet badge ', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('548', '', 'Radiator upper mount', '', 'units', 'Stockable', '', '15', '10', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('549', '', 'Radiator lower mount', '', 'units', 'Stockable', '', '15', '10', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('550', '', 'Silancer mount - Upper', '', 'units', 'Stockable', '', '15', '10', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('551', '', 'Wheel cup - Kyron', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('552', '', 'Wheel cup - Rexton', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('553', '', 'Oil cooler ', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('554', '', 'Brake oil - Dot 4', '', 'units', 'Stockable', '', '8', '67', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('555', '', 'RF-7 oil treatment (BG)', '', 'units', 'Stockable', '', '8', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('556', '', 'Radiator cooling system cleaner (BG)', '', 'units', 'Stockable', '', '8', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('557', '', 'Brake oil - Dot 3', '', 'units', 'Stockable', '', '8', '6', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('558', '', 'Diesel filter Free Lander 2 - (Coopers)', '', 'Box', 'Stockable', '', '14', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('559', '', 'Diesel filter Free Lander 2 - (County)', '', 'units', 'Stockable', '', '14', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('560', '', 'Diesel filter - Discovery 4  (G) 2.7L & 3L', '', 'Box', 'Stockable', '', '14', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('561', '', 'Air filter Free Lander 2 (MAHLE)', '', 'units', 'Stockable', '', '1', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('562', '', 'Selant (ABRO)', '', 'units', 'Stockable', '', '15', '13', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('563', '', 'Petrol injector cleaner CF5 (BG)', '', 'Bottles', 'Stockable', '', '15', '14', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('564', '', 'Oil filter - Prado JS (OE-116)', '', 'units', 'Stockable', '', '5', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('565', '', 'Front brake pad set - Prado ', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('566', '', 'Axle C.V boot - Prado', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('567', '', 'Vaccum Modulator', '', 'Box', 'Stockable', '', '15', '5', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('568', '', '10 mm bolt', '', 'units', 'Stockable', '', '23', '78', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('569', '', '10 mm bolt (1.5 inch)', '', 'units', 'Stockable', '', '23', '29', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('570', '', '10 mm washer', '', 'units', 'Stockable', '', '23', '99', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('571', '', '10 mm nut', '', 'units', 'Stockable', '', '23', '88', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('572', '', '10 mm washer - Big', '', 'units', 'Stockable', '', '23', '95', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('573', '', '8 mm bolt', '', 'units', 'Stockable', '', '23', '42', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('574', '', '8 mm nut', '', 'units', 'Stockable', '', '23', '42', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('575', '', '8 mm washer', '', 'units', 'Stockable', '', '23', '29', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('576', '', '5 mm Allen key bolt', '', 'units', 'Stockable', '', '23', '26', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('577', '', 'Fan clutch bolt', '', 'units', 'Stockable', '', '23', '16', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('578', '', 'Adjuster washer ', '', 'units', 'Stockable', '', '23', '103', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('579', '', '12 mm washer', '', 'units', 'Stockable', '', '23', '21', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('580', '', '10 mm bolt (2 inch)', '', 'units', 'Stockable', '', '23', '24', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('581', '', 'LED capless bulb', '', 'bulbs', 'Stockable', '', '3', '59', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('582', '', 'LED capless bulb - Blue', '', 'bulbs', 'Stockable', '', '3', '28', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('583', '', 'Dash board light LED - Blue', '', 'bulbs', 'Stockable', '', '3', '9', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('584', '', 'Dash board light LED - Yellow', '', 'bulbs', 'Stockable', '', '3', '9', null, null, null, null, null, '1');

-- ----------------------------
-- Table structure for `product_has_uom`
-- ----------------------------
DROP TABLE IF EXISTS `product_has_uom`;
CREATE TABLE `product_has_uom` (
  `idproduct_has_uom` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL,
  `unit_of_measure_idunit_of_measure` int(11) NOT NULL,
  `base_unit` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idproduct_has_uom`),
  KEY `fk_product_has_unit_of_measure_unit_of_measure1_idx` (`unit_of_measure_idunit_of_measure`),
  KEY `fk_product_has_unit_of_measure_product1_idx` (`product_idproduct`),
  CONSTRAINT `fk_product_has_unit_of_measure_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_has_unit_of_measure_unit_of_measure1` FOREIGN KEY (`unit_of_measure_idunit_of_measure`) REFERENCES `unit_of_measure` (`idunit_of_measure`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_has_uom
-- ----------------------------
INSERT INTO `product_has_uom` VALUES ('1', '3', '1', '1');
INSERT INTO `product_has_uom` VALUES ('2', '3', '2', '0');

-- ----------------------------
-- Table structure for `product_stock`
-- ----------------------------
DROP TABLE IF EXISTS `product_stock`;
CREATE TABLE `product_stock` (
  `idproduct_stock` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL,
  `product_has_uom` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`idproduct_stock`),
  KEY `fk_product_stock_product1_idx` (`product_idproduct`),
  KEY `fk_product_stock_product_has_uom1_idx` (`product_has_uom`),
  CONSTRAINT `fk_product_stock_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_stock_product_has_uom1` FOREIGN KEY (`product_has_uom`) REFERENCES `product_has_uom` (`idproduct_has_uom`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_stock
-- ----------------------------

-- ----------------------------
-- Table structure for `purchases_detail`
-- ----------------------------
DROP TABLE IF EXISTS `purchases_detail`;
CREATE TABLE `purchases_detail` (
  `idpurchases_detail` int(11) NOT NULL AUTO_INCREMENT,
  `Purchasing_Quantity` varchar(45) DEFAULT NULL,
  `Purchasing_Price` varchar(45) DEFAULT NULL,
  `Selling_Price` varchar(45) DEFAULT NULL,
  `Total_Amount` varchar(45) DEFAULT NULL,
  `product` int(11) NOT NULL,
  `purchases_header` int(11) NOT NULL,
  `product_has_uom` int(11) NOT NULL,
  PRIMARY KEY (`idpurchases_detail`),
  KEY `fk_purchases_detail_product1_idx` (`product`) USING BTREE,
  KEY `fk_purchases_detail_purchases_header1_idx` (`purchases_header`) USING BTREE,
  KEY `fk_purchases_detail_product_has_uom1_idx` (`product_has_uom`),
  CONSTRAINT `fk_purchases_detail_product_has_uom1` FOREIGN KEY (`product_has_uom`) REFERENCES `product_has_uom` (`idproduct_has_uom`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `purchases_detail_ibfk_1` FOREIGN KEY (`product`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `purchases_detail_ibfk_2` FOREIGN KEY (`purchases_header`) REFERENCES `purchases_header` (`idpurchases_header`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of purchases_detail
-- ----------------------------
INSERT INTO `purchases_detail` VALUES ('7', '12', '', '', '', '122', '5', '0');
INSERT INTO `purchases_detail` VALUES ('8', '12', '', '', '', '122', '6', '0');
INSERT INTO `purchases_detail` VALUES ('9', '100', '', '', '', '124', '7', '0');
INSERT INTO `purchases_detail` VALUES ('10', '10', '', '', '', '160', '8', '0');
INSERT INTO `purchases_detail` VALUES ('11', '12', '', '', '', '161', '9', '0');
INSERT INTO `purchases_detail` VALUES ('12', '20', '', '', '', '55', '10', '0');
INSERT INTO `purchases_detail` VALUES ('13', '4', '', '', '', '32', '11', '0');
INSERT INTO `purchases_detail` VALUES ('14', '10', '', '', '', '3', '12', '0');
INSERT INTO `purchases_detail` VALUES ('16', '20', '', '', '', '424', '14', '0');
INSERT INTO `purchases_detail` VALUES ('17', '15', null, null, '', '3', '15', '1');

-- ----------------------------
-- Table structure for `purchases_header`
-- ----------------------------
DROP TABLE IF EXISTS `purchases_header`;
CREATE TABLE `purchases_header` (
  `idpurchases_header` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `purchases_no` varchar(45) DEFAULT NULL,
  `Total_Amount` varchar(45) DEFAULT NULL,
  `Date_Added` datetime DEFAULT NULL,
  `Added_By` varchar(45) DEFAULT NULL,
  `Date_Updated` datetime DEFAULT NULL,
  `Updated_By` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `supplier_idsupplier` int(11) NOT NULL,
  PRIMARY KEY (`idpurchases_header`),
  KEY `fk_purchases_header_supplier1_idx` (`supplier_idsupplier`) USING BTREE,
  CONSTRAINT `purchases_header_ibfk_1` FOREIGN KEY (`supplier_idsupplier`) REFERENCES `supplier` (`idsupplier`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of purchases_header
-- ----------------------------
INSERT INTO `purchases_header` VALUES ('5', '2016-06-22', '11:42:19', 'PN000001\n', null, '2016-06-22 11:42:19', '1', '2016-06-22 11:42:19', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('6', '2016-06-23', '03:33:37', 'PN000002\n', null, '2016-06-23 03:33:37', '1', '2016-06-23 03:33:37', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('7', '2016-07-04', '14:30:55', 'PN000003\n', null, '2016-07-04 14:30:55', '1', '2016-07-04 14:30:55', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('8', '2016-07-04', '15:26:16', 'PN000004\n', null, '2016-07-04 15:26:16', '1', '2016-07-04 15:26:16', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('9', '2016-07-04', '15:26:49', 'PN000005\n', null, '2016-07-04 15:26:49', '1', '2016-07-04 15:26:49', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('10', '2016-07-04', '15:27:25', 'PN000006\n', null, '2016-07-04 15:27:25', '1', '2016-07-04 15:27:25', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('11', '2016-07-04', '15:28:39', 'PN000007\n', null, '2016-07-04 15:28:39', '1', '2016-07-04 15:28:39', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('12', '2016-07-04', '17:10:02', 'PN000008\n', null, '2016-07-04 17:10:02', '1', '2016-07-04 17:10:02', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('14', '2016-07-07', '15:35:01', 'PN000009\n', null, '2016-07-07 15:35:01', '1', '2016-07-07 15:35:01', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('15', '2016-07-31', '15:56:31', 'PN000010\n', null, '2016-07-31 15:56:31', '1', '2016-07-31 15:56:31', '1', 'DONE', '1');

-- ----------------------------
-- Table structure for `return_detail`
-- ----------------------------
DROP TABLE IF EXISTS `return_detail`;
CREATE TABLE `return_detail` (
  `idreturn_detail` int(11) NOT NULL AUTO_INCREMENT,
  `qty` int(11) DEFAULT NULL,
  `product` int(11) NOT NULL,
  `product_has_uom` int(11) NOT NULL,
  `stock_return_header` int(11) NOT NULL,
  PRIMARY KEY (`idreturn_detail`),
  KEY `fk_return_detail_product1_idx` (`product`),
  KEY `fk_return_detail_product_has_uom1_idx` (`product_has_uom`),
  KEY `fk_return_detail_stock_return_header1_idx` (`stock_return_header`),
  CONSTRAINT `fk_return_detail_product1` FOREIGN KEY (`product`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_return_detail_product_has_uom1` FOREIGN KEY (`product_has_uom`) REFERENCES `product_has_uom` (`idproduct_has_uom`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_return_detail_stock_return_header1` FOREIGN KEY (`stock_return_header`) REFERENCES `stock_return_header` (`idstock_return_header`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of return_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `stock_return_header`
-- ----------------------------
DROP TABLE IF EXISTS `stock_return_header`;
CREATE TABLE `stock_return_header` (
  `idstock_return_header` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `return_no` varchar(45) DEFAULT NULL,
  `recieved_by` varchar(45) DEFAULT NULL,
  `job_ref` varchar(55) DEFAULT NULL,
  `issuing_header` int(11) NOT NULL,
  PRIMARY KEY (`idstock_return_header`),
  KEY `fk_stock_return_header_issuing_header1_idx` (`issuing_header`),
  CONSTRAINT `fk_stock_return_header_issuing_header1` FOREIGN KEY (`issuing_header`) REFERENCES `issuing_header` (`idissuing_header`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stock_return_header
-- ----------------------------

-- ----------------------------
-- Table structure for `sub_category`
-- ----------------------------
DROP TABLE IF EXISTS `sub_category`;
CREATE TABLE `sub_category` (
  `idsub_category` int(11) NOT NULL AUTO_INCREMENT,
  `sub_category_name` varchar(45) DEFAULT NULL,
  `category` int(11) NOT NULL,
  PRIMARY KEY (`idsub_category`),
  KEY `fk_sub_category_category_idx` (`category`) USING BTREE,
  CONSTRAINT `sub_category_ibfk_1` FOREIGN KEY (`category`) REFERENCES `category` (`idcategory`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sub_category
-- ----------------------------
INSERT INTO `sub_category` VALUES ('1', 'Air filter', '1');
INSERT INTO `sub_category` VALUES ('2', 'Brake pads', '1');
INSERT INTO `sub_category` VALUES ('3', 'Bulbs', '1');
INSERT INTO `sub_category` VALUES ('4', 'Fuse', '1');
INSERT INTO `sub_category` VALUES ('5', 'Oil filter', '1');
INSERT INTO `sub_category` VALUES ('6', 'Suspension bush', '1');
INSERT INTO `sub_category` VALUES ('7', 'Suspension parts', '1');
INSERT INTO `sub_category` VALUES ('8', 'Oils', '1');
INSERT INTO `sub_category` VALUES ('10', 'Sesnsors', '1');
INSERT INTO `sub_category` VALUES ('11', 'Gaskets', '1');
INSERT INTO `sub_category` VALUES ('12', 'Coolent', '1');
INSERT INTO `sub_category` VALUES ('13', 'Oil seals', '1');
INSERT INTO `sub_category` VALUES ('14', 'Diesel filters', '1');
INSERT INTO `sub_category` VALUES ('15', 'Ssangyong Parts', '1');
INSERT INTO `sub_category` VALUES ('16', 'Wiper blades', '1');
INSERT INTO `sub_category` VALUES ('17', 'Door lock motors', '1');
INSERT INTO `sub_category` VALUES ('18', 'Used Parts', '1');
INSERT INTO `sub_category` VALUES ('19', 'Hose clips', '1');
INSERT INTO `sub_category` VALUES ('20', 'Shockabsober', '1');
INSERT INTO `sub_category` VALUES ('22', 'Land Rover', '1');
INSERT INTO `sub_category` VALUES ('23', 'Nut, Bolt & Washer', '1');

-- ----------------------------
-- Table structure for `supplier`
-- ----------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier` (
  `idsupplier` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(45) DEFAULT NULL COMMENT 'Supplier Name',
  `address` varchar(245) DEFAULT NULL,
  `contact_person` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `Fax` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsupplier`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of supplier
-- ----------------------------
INSERT INTO `supplier` VALUES ('1', 'Advanced', '1452/6, Malabe rd, Pannipitiya', '', '', '', '');
INSERT INTO `supplier` VALUES ('2', 'Missex International', 'Pagoda', '', '', '', '');

-- ----------------------------
-- Table structure for `unit_of_measure`
-- ----------------------------
DROP TABLE IF EXISTS `unit_of_measure`;
CREATE TABLE `unit_of_measure` (
  `idunit_of_measure` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(5) DEFAULT NULL,
  `description` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`idunit_of_measure`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of unit_of_measure
-- ----------------------------
INSERT INTO `unit_of_measure` VALUES ('1', 'LTR', 'Litre');
INSERT INTO `unit_of_measure` VALUES ('2', 'ML', 'Milliliter');

-- ----------------------------
-- Table structure for `uom_conversion`
-- ----------------------------
DROP TABLE IF EXISTS `uom_conversion`;
CREATE TABLE `uom_conversion` (
  `iduom_conversion` int(11) NOT NULL AUTO_INCREMENT,
  `bproduct_has_uom` int(11) NOT NULL,
  `nos` int(11) DEFAULT NULL,
  `product_has_uom` int(11) NOT NULL,
  PRIMARY KEY (`iduom_conversion`),
  KEY `fk_uom_conversion_product_has_uom1_idx` (`bproduct_has_uom`),
  KEY `fk_uom_conversion_product_has_uom2_idx` (`product_has_uom`),
  CONSTRAINT `fk_uom_conversion_product_has_uom1` FOREIGN KEY (`bproduct_has_uom`) REFERENCES `product_has_uom` (`idproduct_has_uom`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_uom_conversion_product_has_uom2` FOREIGN KEY (`product_has_uom`) REFERENCES `product_has_uom` (`idproduct_has_uom`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of uom_conversion
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE,
  UNIQUE KEY `email` (`email`) USING BTREE,
  UNIQUE KEY `password_reset_token` (`password_reset_token`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'Dt8N6klRpIjtaHilHmFB-_in0Y7Z_6b-', '$2y$13$43u7l5HgrgaJ7aidNWZg4.iR/9/qt5h0L93NH364S/E8QrMPI5IZ2', null, 'akilamaxi@gmail.com', '10', '1465754738', '1465754738');

-- ----------------------------
-- Table structure for `warehouse`
-- ----------------------------
DROP TABLE IF EXISTS `warehouse`;
CREATE TABLE `warehouse` (
  `idwarehouse` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idwarehouse`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of warehouse
-- ----------------------------
INSERT INTO `warehouse` VALUES ('1', 'Advanced', 'A');
INSERT INTO `warehouse` VALUES ('2', 'Missex International', 'M');
