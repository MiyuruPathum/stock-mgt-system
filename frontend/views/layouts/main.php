<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\DashboardAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

DashboardAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="fixed-navbar fixed-sidebar">
<?php $this->beginBody() ?>

 
    <!-- Main navbar -->
    <div class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.html"><img src="assets/images/logo_light.png" alt=""></a>

            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

             
            </ul>

             

            <ul class="nav navbar-nav navbar-right">
                 

             
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img src="assets/images/placeholder.jpg" alt="">
                        <span><?=Yii::$app->user->identity->username; ?></span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">                      
                        <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
                        <li> <?=Html::a( '<i class="icon-switch2"></i> Logout', ['site/logout'], ['data-method'=>'post']) ?></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->


    <!-- Page header -->
    <div class="page-header">
        <div class="breadcrumb-line breadcrumb-line-wide">
            <ul class="breadcrumb">
                <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active"><?=$this->title?></li>
            </ul>

            <ul class="breadcrumb-elements">
             
                
            </ul>
        </div>

        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - <?=$this->title?> <small>Hello, <?=Yii::$app->user->identity->username; ?>!</small></h4>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                     
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-default">
                <div class="sidebar-content">

                    <!-- Main navigation -->
                    <div class="sidebar-category sidebar-category-visible">
                        <div class="category-title h6">
                            <span>Main navigation</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content sidebar-user">
                            <div class="media">
                                <a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                                <div class="media-body">
                                    <span class="media-heading text-semibold"><?=Yii::$app->user->identity->username; ?></span>
                                    <div class="text-size-mini text-muted">
                                        <i class="icon-pin text-size-small"></i> &nbsp;Colombo, Sri Lanka
                                    </div>
                                </div>

                                <div class="media-right media-middle">
                                    <ul class="icons-list">
                                        <li>
                                            <a href="#"><i class="icon-cog3"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="category-content no-padding">
                            <ul class="navigation navigation-main navigation-accordion">

                                <!-- Main -->
                                <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                                <li class="active"><a href="index.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                                <li><a href="index.php?r=purchases-header"><i class="icon-wallet"></i> <span>Purchase Note</span></a></li>
                                <li><a href="index.php?r=issuing-header"><i class="icon-bookmark"></i> <span>Issue Note</span></a></li>
                                <li><a href="index.php?r=stock-return-header"><i class="icon-reply"></i> <span>Stock Return Note</span></a></li>
                               
                                <li>
                                    <a href="#"><i class="icon-cube"></i> <span>Product Management</span></a>
                                    <ul>
                                        <li><a href="index.php?r=category">Category</a></li> 
                                        <li><a href="index.php?r=sub-category">Sub Category</a></li> 
                                        <li><a href="index.php?r=product">Product</a></li> 
                                        <li><a href="index.php?r=unit-of-measure">Unit Of measure</a></li>
                                        <li><a href="index.php?r=product-has-uom">Product UOM</a></li> 

                                    </ul>
                                </li> 
                                <li><a href="index.php?r=supplier"><i class="icon-users2"></i> <span>Supplier</span></a></li>
                                 <li><a href="index.php?r=warehouse"><i class="icon-home"></i> <span>Warehouse</span></a></li>                                <li>
                                    <a href="#"><i class="icon-graph"></i> <span>Reports</span></a>
                                    <ul>
                                        <li><a href="index.php?r=site/analysis">Analysis</a></li> 
                                        <li><a href="index.php?r=site/stock">Stock Balance</a></li> 
                                        <li><a href="index.php?r=site/reorder">Reorder Level</a></li> 
                                        <li><a href="index.php?r=site/job-report">Job Report</a></li> 
                                        
                                    </ul>
                                </li> 
                                 

                            </ul>
                        </div>
                    </div>
                    <!-- /main navigation -->

                </div>
            </div>
            <!-- /main sidebar -->


            <!-- Main content -->
            <div class="content-wrapper">

              <?= $content ?>

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->


        <!-- Footer -->
        <div class="footer text-muted">
            &copy; 2015. <a href="#"> </a>
        </div>
        <!-- /footer -->

    </div>
    <!-- /page container -->


<?php $this->endBody() ?>
<script type="text/javascript">
  
$('.select-search1').select2() .on("change", function (e) {
       var sel = $(this).parents('td').next('td').find('select');         
                    $.ajax({
                        type: "GET",
                        url: "index.php?r=issuing-header/uom&id="+$(this).val(),                         
                    }).done(function (msg) {
                        
                           sel.html(msg);   
                    });                              
});

                                     

 $('.daterange-basic').daterangepicker({
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default'
    });


 
      
</script>


</body>
</html>
<?php $this->endPage() ?>
