<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Supplier */

$this->title = 'Update Supplier: ' . ' ' . $model->supplier_name;
$this->params['breadcrumbs'][] = ['label' => 'Suppliers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idsupplier, 'url' => ['view', 'id' => $model->idsupplier]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="supplier-update">

   
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
