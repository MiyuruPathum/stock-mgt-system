<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\ProductHasUomSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-has-uom-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

  <?php
$product = \Yii::$app->db->createCommand("SELECT product.idproduct, CONCAT( warehouse.`code`, '-', product.product_name ) AS product_name FROM product INNER JOIN warehouse ON product.idwarehouse = warehouse.idwarehouse  ")->queryAll();     
 
    ?>
 <div class="form-group field-producthasuomsearch-product_idproduct">
<label class="control-label" for="producthasuomsearch-product_idproduct">Product</label>
 <select data-placeholder="Select a product..." class="select-search1 form-control"  name="ProductHasUomSearch[product_idproduct]">
                                        <option></option> 
                                        <?php  foreach($product as $row){ ?>
                                            <option value="<?=$row['idproduct']; ?>"><?=$row['product_name']; ?></option>
                                        <?php } ?>  
                                                                                
                                    </select> 
<div class="help-block"></div>
</div>
    

    <?php
$uoms = \Yii::$app->db->createCommand("SELECT * FROM unit_of_measure")->queryAll();     
    ?>

 <div class="form-group field-producthasuomsearch-unit_of_measure_idunit_of_measure">
<label class="control-label" for="producthasuomsearch-unit_of_measure_idunit_of_measure">Unit Of Measure</label>
 <select class="form-control" name="ProductHasUomSearch[unit_of_measure_idunit_of_measure]">
                                        <option></option> 
                                        <?php  foreach($uoms as $row){ ?>
                                            <option value="<?=$row['idunit_of_measure']; ?>"><?=$row['code']; ?></option>
                                        <?php } ?>  
                                                                                
                                    </select> 
<div class="help-block"></div>
</div>
    <?= $form->field($model, 'base_unit') ?>

 

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
