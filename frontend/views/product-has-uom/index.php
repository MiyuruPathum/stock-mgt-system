<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ProductHasUomSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product UOM';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-has-uom-index">

    <div class="panel panel-flat  panel-collapsed">
        <div class="panel-heading">
            <h6 class="panel-title">Product UOM<span class="text-semibold">Search</span></h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a class="" data-action="collapse"></a></li>                   
                   
                </ul>
            </div>
        <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
        
        <div style="display: block;" class="panel-body">
             <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>


    <p>
        <?= Html::a('Assign Product Uom', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           
             [
                'attribute' => 'product_idproduct',
                'value' => 'product.product_name', 
            ],
             [
                'attribute' => 'unit_of_measure_idunit_of_measure',
                'value' => 'uom.code', 
            ],
            'base_unit',
            'qty',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
