<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\ProductHasUom */

$this->title = 'Update Product Has Uom: ' . ' ' . $model->idproduct_has_uom;
$this->params['breadcrumbs'][] = ['label' => 'Product Has Uoms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idproduct_has_uom, 'url' => ['view', 'id' => $model->idproduct_has_uom]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-has-uom-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
          'product' => $product,
    ]) ?>

</div>
