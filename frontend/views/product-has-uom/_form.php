<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model frontend\models\ProductHasUom */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-has-uom-form">

    <?php $form = ActiveForm::begin(); ?>

   

<?php
$listData=ArrayHelper::map($product,'idproduct','product_name');

echo $form->field($model, 'product_idproduct')->dropDownList(
                                $listData, 
                                ['prompt'=>'Select...','class' => 'select-search1']);
$uoms = \Yii::$app->db->createCommand("SELECT * FROM unit_of_measure")->queryAll();   

$listData1=ArrayHelper::map($uoms,'idunit_of_measure','code');

echo $form->field($model, 'unit_of_measure_idunit_of_measure')->dropDownList(
                                $listData1, 
                                ['prompt'=>'Select...']);

    ?>

 


    <?= $form->field($model, 'base_unit')->textInput() ?>

    <?= $form->field($model, 'qty')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
