<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Category */

$this->title = 'Update Category: ' . ' ' . $model->category_name;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcategory, 'url' => ['view', 'id' => $model->idcategory]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="category-update">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
