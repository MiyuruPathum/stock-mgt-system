<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\SubCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-category-index">

     
    <div class="panel panel-flat  panel-collapsed">
        <div class="panel-heading">
            <h6 class="panel-title">Sub Category <span class="text-semibold">Search</span></h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a class="" data-action="collapse"></a></li>                   
                   
                </ul>
            </div>
        <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
        
        <div style="display: block;" class="panel-body">
             <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>


    <p>
        <?= Html::a('Create Sub Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idsub_category',
            'sub_category_name',
            // 'category',
             [
                'attribute' => 'category',
                'value' => 'mcategory.category_name', 
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
