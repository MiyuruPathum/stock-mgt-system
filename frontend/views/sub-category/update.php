<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\SubCategory */

$this->title = 'Update Sub Category: ' . ' ' . $model->sub_category_name;
$this->params['breadcrumbs'][] = ['label' => 'Sub Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idsub_category, 'url' => ['view', 'id' => $model->idsub_category]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sub-category-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
