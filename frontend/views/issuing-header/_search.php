<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\IssuingHeaderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="issuing-header-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'time') ?>

    <?= $form->field($model, 'issue_no') ?>

    <?= $form->field($model, 'recieved_by') ?>

     <?= $form->field($model, 'vehicle_no') ?>

    <?php  echo $form->field($model, 'job_ref') ?>

    <?php // echo $form->field($model, 'Added_By') ?>

    <?php // echo $form->field($model, 'Date_Updated') ?>

    <?php // echo $form->field($model, 'Updated_By') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
