<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\IssuingHeader */

$this->title = $model->issue_no;
$this->params['breadcrumbs'][] = ['label' => 'Issuing Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="issuing-header-view">
 
 

    <div class="panel panel-white">
                    <div class="panel-heading">
                        <h6 class="panel-title"> Issue Note </h6>
                        <div class="heading-elements">
                                           <button type="button" class="btn btn-default btn-xs heading-btn"><i class="icon-printer position-left"></i> Print</button>
                        </div>
                    <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>

                    <div class="panel-body no-padding-bottom">
                        <div class="row">
                            <div class="col-md-6 content-group">
                                <img src="assets/images/logo_demo.png" class="content-group mt-10" alt="" style="width: 120px;">
                                <ul class="list-condensed list-unstyled">
                                    <li>Advanced Car Diagnostics</li>
                                    <li>1452/6 Malabe road,</li>
                                    <li>Pannipitiya</li>
                                </ul>
                            </div>

                            <div class="col-md-6 content-group">
                                <div class="invoice-details">
                                    <h5 class="text-uppercase text-semibold">Issue No #<?=$model->issue_no?></h5>
                                    <ul class="list-condensed list-unstyled">
                                        <li>Date: <span class="text-semibold"><?=$model->date?></span></li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>

                        
                    </div>

                    <div class="table-responsive">
                        <table class="table table-lg">
                            <thead>
                                <tr>
                                    <th>Description</th>
                                    <th class="col-sm-1">UOM</th> 
                                    <th class="col-sm-1">Qty</th> 
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $detial = \Yii::$app->db->createCommand("SELECT
                                        product.product_code,
                                        product.product_name,
                                        issue_detail.qty,
                                        unit_of_measure.`code` AS uom
                                        FROM
                                        issue_detail
                                        INNER JOIN product ON issue_detail.product = product.idproduct
                                        INNER JOIN product_has_uom ON issue_detail.product_has_uom = product_has_uom.idproduct_has_uom
                                        INNER JOIN unit_of_measure ON product_has_uom.unit_of_measure_idunit_of_measure = unit_of_measure.idunit_of_measure
                                        WHERE
                                            issue_detail.issuing_header= '$model->idissuing_header' ")->queryAll(); 
 
                            foreach ($detial as $row) {                                
                           
                            ?>
                                <tr>
                                    <td>
                                        <h6 class="no-margin"><?=$row['product_code']?></h6>
                                        <span class="text-muted"><?=$row['product_name']?></span>
                                    </td>
                                    <td><?=$row['uom']?></td>
                                    <td><?=$row['qty']?></td>
                               </tr>
                             <?php } ?>   
                            </tbody>
                        </table>
                    </div>

                    <div class="panel-body">
                        <div class="row invoice-payment">
                            <div class="col-sm-5">
                                <h5> Recieved by :
                                <?=$model->recieved_by?>
                                </h5>
                            </div>
                             <div class="col-sm-5">
                                <h5> Vehicle No :
                                <?=$model->vehicle_no?>
                                </h5>
                            </div

                            <div class="col-sm-5">
                                <div class="content-group">
                                     
                                    <div class="table-responsive no-border">
                                        <table class="table">
                                            <tbody>
                                               
                                                 
                                            </tbody>
                                        </table>
                                    </div>

                                    
                                </div>
                            </div>
                        </div>

                       <!--  <h6>Other information</h6>
                        <p class="text-muted">Thank you for using Limitless. This invoice can be paid via PayPal, Bank transfer, Skrill or Payoneer. Payment is due within 30 days from the date of delivery. Late payment is possible, but with with a fee of 10% per month. Company registered in England and Wales #6893003, registered office: 3 Goodman Street, London E1 8BF, United Kingdom. Phone number: 888-555-2311</p>
                  -->   </div>
                </div>

 
</div>
