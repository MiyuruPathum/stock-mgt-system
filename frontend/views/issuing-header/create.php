<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\IssuingHeader */

$this->title = 'Create Issue Note';
$this->params['breadcrumbs'][] = ['label' => 'Issuing Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="issuing-header-create"> 

     <?= $this->render('_form', [
        'model' => $model,
	'product' => $product,
    ]) ?>

</div>
   
 

 