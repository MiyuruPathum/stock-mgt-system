<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\IssuingHeaderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Issue Note';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="issuing-header-index">

     <div class="panel panel-flat  panel-collapsed">
        <div class="panel-heading">
            <h6 class="panel-title">Issue Note <span class="text-semibold">Search</span></h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a class="" data-action="collapse"></a></li>                   
                   
                </ul>
            </div>
        <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
        
        <div style="display: block;" class="panel-body">
             <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>

    <p>
        <?= Html::a('Create Issue Note', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idissuing_header',
            'date',
            'time',
            'issue_no',
            'recieved_by',
            'vehicle_no',
            'job_ref',
            // 'Added_By',
            // 'Date_Updated',
            // 'Updated_By',
             'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
