<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\IssuingHeader */

$this->title = 'Update Issuing Header: ' . ' ' . $model->idissuing_header;
$this->params['breadcrumbs'][] = ['label' => 'Issuing Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idissuing_header, 'url' => ['view', 'id' => $model->idissuing_header]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="issuing-header-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
