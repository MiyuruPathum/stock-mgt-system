<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\StockReturnHeaderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stock Return Notes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stock-return-header-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Stock Return Note', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idstock_return_header',
            'date',
            'time',
            'return_no',
            'recieved_by',
             'job_ref',
             'issuing_header',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
