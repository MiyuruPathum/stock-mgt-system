<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\StockReturnHeader */

$this->title = $model->idstock_return_header;
$this->params['breadcrumbs'][] = ['label' => 'Stock Return Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stock-return-header-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idstock_return_header], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idstock_return_header], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idstock_return_header',
            'date',
            'time',
            'return_no',
            'recieved_by',
            'job_ref',
            'issuing_header',
        ],
    ]) ?>

</div>
