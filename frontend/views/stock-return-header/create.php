<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\StockReturnHeader */

$this->title = 'Create Stock Return Note';
$this->params['breadcrumbs'][] = ['label' => 'Stock Return Notes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stock-return-header-create">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
