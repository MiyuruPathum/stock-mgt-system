<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\StockReturnHeader */

$this->title = 'Update Stock Return Header: ' . ' ' . $model->idstock_return_header;
$this->params['breadcrumbs'][] = ['label' => 'Stock Return Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idstock_return_header, 'url' => ['view', 'id' => $model->idstock_return_header]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="stock-return-header-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
