<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\StockReturnHeader */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stock-return-header-form">

    <?php $form = ActiveForm::begin(); ?>

  <div class="panel panel-white">
                    <div class="panel-body no-padding-bottom">
                        <div class="row">
                            <div class="col-md-6 content-group">
                                <img src="assets/images/logo_demo.png" class="content-group mt-10" alt="" style="width: 120px;">
                                <ul class="list-condensed list-unstyled">
                                     <li>Advanced Car Diagnostics</li>
                                    <li>1452/6 Malabe road,</li>
                                    <li>Pannipitiya</li>
                                </ul>
                            </div>

                            <div class="col-md-6 content-group">
                                <div class="invoice-details">
                                    <h5 class="text-uppercase text-semibold">Return Note</h5>
                                    <ul class="list-condensed list-unstyled">
                                        <li>Date: <span class="text-semibold"><?=date("Y-m-d h:i:s")?></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                            
                    </div>

                    <div class="table-responsive">
                        <table class="table table-lg">
                            <thead>
                                <tr>
                                    <th>Description</th>
                                    <th class="col-sm-2">UOM</th>
                                    <th class="col-sm-1">Qty</th>
                                    <th class="col-sm-2">Actions</th>                                   
                                </tr>
                            </thead>
                            <tbody id="tbody">
                                
                            </tbody>
                           
                           
                        </table>
                    </div>

                    <div class="panel-body">
                        <div class="row invoice-payment">
                            <div class="col-sm-12">
                                <div class="content-group col-sm-4">                                    
                                    <div class="mb-15 mt-15">
                                                   
                                    </div>                                                                       
                                </div>
                                <div class="content-group col-sm-4">                                    
                                    
                                    <div  class="mb-15 mt-15">
                                     
                                    </div>
                                </div>
                                <div class="content-group col-sm-4">                                    
                                    
                                    <div  class="mb-15 mt-15">
                                     
                                    </div>

                                     
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="content-group">
                                      

                                    <div class="text-right" style="padding-right: 1%;">
                                       <?= Html::submitButton($model->isNewRecord ? '<i class="icon-paperplane"></i> Submit' : '<i class="icon-paperplane"></i> Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-success']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /invoice template -->

 

    <?php ActiveForm::end(); ?>

</div>
