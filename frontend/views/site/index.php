<?php
 
/* @var $this yii\web\View */
 
$this->title = 'Dashboard';
?>
 
 <div class="col-lg-6">					 

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">Low stock products</h6>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            		<li><a data-action="close"></a></li>
            	</ul>
        	</div>
		<a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
		
		<div class="panel-body">
			<div class="table-responsive">
						<table class="table">
							<thead>
								<tr class="border-bottom-danger">
									<th>#</th>
									<th>Product </th>
									<th>On stock</th>									 
								</tr>
							</thead>
							<tbody>
								<?php foreach ($reorder as $row) {?>
								 
								<tr class="border-top-danger">
									<td><?=$row['product_code'];?></td>
									<td><?=$row['product_name'];?></td>
									<td><?=$row['availble_qty'];?></td>									 
								</tr>
								 
								<?php }?> 
							</tbody>
						</table>
					</div>
		</div>
	</div>

	 
</div>
<div class="col-lg-6">					 

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">Fast moving products</h6>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            		<li><a data-action="close"></a></li>
            	</ul>
        	</div>
		<a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
		
		<div class="panel-body">
			<div class="table-responsive">
						<table class="table">
							<thead>
								<tr class="border-bottom-danger">
									<th>#</th>
									<th>Product </th>
									<th>On stock</th>
								</tr>
							</thead>
							<tbody>
								 
							</tbody>
						</table>
					</div>
		</div>
	</div>

	 
</div>