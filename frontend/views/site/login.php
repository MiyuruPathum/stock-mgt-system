<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
 
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                  <div class="panel panel-body login-form">
                       <div class="text-center">
                            <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                            <h5 class="content-group">Login to your account <small class="display-block">Enter your credentials below</small></h5>
                        </div>

                <?= $form->field($model, 'username', ['template' => '
                         <div class="form-group has-feedback has-feedback-left">
                             {input}
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                           {error}{hint}
                         </div>
                  '])->textInput(array('placeholder' => 'Username')) ?>        

                 <?= $form->field($model, 'password', ['template' => '
                         <div class="form-group has-feedback has-feedback-left">
                             {input}
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                           {error}{hint}
                         </div>
                  '])->passwordInput(array('placeholder' => 'Password')) ?>  
 
                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div style="color:#999;margin:1em 0">
                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Sign in <i class="icon-circle-right2 position-right"></i>', ['class' => 'btn btn-primary btn-block ', 'name' => 'login-button']) ?>
                </div>
                 </div>
            <?php ActiveForm::end(); ?>
        
 