<?php ?>


<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Stock on back order level</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<div class="panel-body">
									</div>

					<table class="table datatable-basic">
						<thead>
							<tr>
								<th>Product code</th>
								<th>Product Name</th>
								<th>Reorder Level</th>
								<th>Availble Qty</th> 
							</tr>
						</thead>
						<tbody>
							 
								<?php foreach ($reorder as $row) {?>
								 
								<tr class="border-top-danger">
									<td><?=$row['product_code'];?></td>
									<td><?=$row['product_name'];?></td>
									<td><?=$row['reorder_level'];?></td>
									<td><?=$row['availble_qty'];?></td>									 
								</tr>
								<?php }?> 
							 
						 
						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->
