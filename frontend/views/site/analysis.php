<?php 
use frontend\models\Warehouse;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
?>
 

<!-- Basic datatable -->
				<div class="panel panel-flat" onload="myFunction()">
					<div class="panel-heading">
						<h5 class="panel-title">Stock Analysis</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<div class="panel-body"> 
						<form class="form-inline" role="form">
						  <div class="form-group">
						    <label for="idwarehouse">  Warehouse :</label>	 
<?= Html::dropDownList('idwarehouse', null,
     ArrayHelper::map(Warehouse::find()->all(),'idwarehouse','name'),['prompt'=>'Select...','class' => 'form-control']); ?>
	
						  </div>
						  <div class="form-group">
						    <label for="pwd">Range:</label>
						    <div class="input-group">
										<span class="input-group-addon"><i class="icon-calendar22"></i></span>
										<input type="text" class="form-control daterange-basic" value="01/01/2015 - 01/31/2015"> 
									</div>
						  </div>
						   &nbsp;
						  <button type="submit" class="btn btn-primary">Search</button>
						</form>
					</div>

					<table class="table datatable-basic">
						<thead>
							<tr>
								<th>Issued Date</th>
								<th>Product Code</th>
								<th>Product Name</th>								 
								<th>Product Type</th>
								<th>Sub Category</th> 								
								<th>Warehouse</th>
								<th>Issued Qty</th>
							</tr>
						</thead>
						<tbody>
							 
						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->
 