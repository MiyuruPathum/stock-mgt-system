<?php 
use frontend\models\Warehouse;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
?>
 

<!-- Basic datatable -->
				<div class="panel panel-flat" onload="myFunction()">
					<div class="panel-heading">
						<h5 class="panel-title">Job Report</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<div class="panel-body"> 
						 
					</div>

					<table class="table datatable-basic">
						<thead>
							<tr>
								<th>Date</th>
								<th>Time</th>
								<th>Issue note No</th>								 
								<th>Warehouse</th>
								<th>Product name</th>
								<th>Issued Qty</th>
								<th>Vehicle No</th>
							</tr>
						</thead>
						<tbody>
							 <?php foreach ($data as $row) {?>
								 
								<tr class="border-top-info">
									<td><?=$row['date'];?></td>
									<td><?=$row['time'];?></td>
									<td><?=$row['issue_no'];?></td>
									<td><?=$row['wname'];?></td>
									<td><?=$row['product_name'];?></td>
									<td><?=$row['qty'];?></td>
									<td><?=$row['vehicle_no'];?></td>									 
								</tr>
								<?php }?>

						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->
 