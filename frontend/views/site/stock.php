<?php ?>


<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Stock Balance</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<div class="panel-body">
					</div>

					<table class="table datatable-basic">
						<thead>
							<tr>
								<th>Product Code</th>
								<th>Product Name</th>
								<th>UOM</th>
								<th>Product Type</th>
								<th>Sub Category</th> 								
								<th>Warehouse</th>
								<th>Availble Qty</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($data as $row) {?>
								 
								<tr class="border-top-info">
									<td><?=$row['product_code'];?></td>
									<td><?=$row['product_name'];?></td>
									<td><?=$row['unit_of_measure'];?></td>
									<td><?=$row['product_type'];?></td>
									<td><?=$row['sub_category_name'];?></td>
									<td><?=$row['wname'];?></td>
									<td><?=$row['availble_qty'];?></td>									 
								</tr>
								<?php }?>
						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->
