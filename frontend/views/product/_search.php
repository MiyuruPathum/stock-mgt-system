<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\SubCategory;
use frontend\models\Warehouse;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model frontend\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>
 
<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

     

    <?= $form->field($model, 'product_code') ?>

    <?= $form->field($model, 'product_name') ?>

    <?= $form->field($model, 'reorder_level') ?>

    <?= $form->field($model, 'unit_of_measure') ?>

    <?php // echo $form->field($model, 'product_type') ?>

    <?php // echo $form->field($model, 'barcode') ?>

     
     <?php 
        echo $form->field($model, 'sub_category')->dropDownList(
                                ArrayHelper::map(SubCategory::find()->all(),'idsub_category','sub_category_name'), 
                                ['prompt'=>'Select...']);

    ?>

    <?php // echo $form->field($model, 'availble_qty') ?>

    <?php // echo $form->field($model, 'buying_price') ?>

    <?php // echo $form->field($model, 'selling_price') ?>

    <?php // echo $form->field($model, 'Added_By') ?>

    <?php // echo $form->field($model, 'Date_Updated') ?>

    <?php // echo $form->field($model, 'Updated_By') ?>

       <?php 
        echo $form->field($model, 'idwarehouse')->dropDownList(
                                ArrayHelper::map(Warehouse::find()->all(),'idwarehouse','name'), 
                                ['prompt'=>'Select...']);

    ?>
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
