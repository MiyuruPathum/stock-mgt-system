<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">
  <div class="panel panel-flat  panel-collapsed">
        <div class="panel-heading">
            <h6 class="panel-title">Product <span class="text-semibold">Search</span></h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a class="" data-action="collapse"></a></li>                   
                   
                </ul>
            </div>
        <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
        
        <div style="display: block;" class="panel-body">
             <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>


    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
 
            'product_code',
            'product_name',
            [
                'attribute' => 'sub_category',
                'value' => 'subCategory.sub_category_name', 
            ],
             [
                'attribute' => 'idwarehouse',
                'value' => 'mwarehouse.name', 
            ],
             'product_type',
             'availble_qty',
            
            
            // 'barcode',
             //'sub_category',
           
            
            // 'buying_price',
            // 'selling_price',
            // 'Added_By',
            // 'Date_Updated',
            // 'Updated_By',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
