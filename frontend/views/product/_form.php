<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\SubCategory;
use frontend\models\Warehouse;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model frontend\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>
 
<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reorder_level')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'availble_qty')->textInput(['maxlength' => true]) ?>     

    <?= $form->field($model, 'product_type')->dropDownList([ 'Consumable' => 'Consumable', 'Service' => 'Service', 'Stockable' => 'Stockable', ], ['prompt' => '']) ?>

    

    
     <?php 
        echo $form->field($model, 'sub_category')->dropDownList(
                                ArrayHelper::map(SubCategory::find()->all(),'idsub_category','sub_category_name'), 
                                ['prompt'=>'Select...']);

    ?>


  

    
     <?php 
        echo $form->field($model, 'idwarehouse')->dropDownList(
                                ArrayHelper::map(Warehouse::find()->all(),'idwarehouse','name'), 
                                ['prompt'=>'Select...']);

    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
