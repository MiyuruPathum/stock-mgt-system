<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\Supplier;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model frontend\models\PurchasesHeader */
/* @var $form yii\widgets\ActiveForm */
?>
 
<div class="purchases-header-form">

    <?php $form = ActiveForm::begin(); ?> 

<div class="panel panel-white">
                 
                    <div class="panel-body no-padding-bottom">
                        <div class="row">
                            <div class="col-md-6 content-group">
                                <img src="assets/images/logo_demo.png" class="content-group mt-10" alt="" style="width: 120px;">
                                <ul class="list-condensed list-unstyled">
                                     <li>Advanced Car Diagnostics</li>
                                    <li>1452/6 Malabe road,</li>
                                    <li>Pannipitiya</li>
                                </ul>
                            </div>

                            <div class="col-md-6 content-group">
                                <div class="invoice-details">
                                    <h5 class="text-uppercase text-semibold">Purchase Note</h5>
                                    <ul class="list-condensed list-unstyled">
                                        <li>Date: <span class="text-semibold"><?=date("Y-m-d H:i:s")?></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                            
                    </div>

                    <div class="table-responsive">
                        <table class="table table-lg">
                            <thead>
                                <tr>
                                    <th class="col-sm-6">Description</th>
                                    <th class="col-sm-2">UOM</th>
                                    <th>Qty</th>                                    
                                    <th class="col-sm-3">Actions</th>                                   
                                </tr>
                            </thead>
                            <tbody id="tbody">
                                <tr>
                                    <td>
                                    <select data-placeholder="Select a product..." class="select-search1" name="PurchasesDetail[0][product]">
                                        <option></option> 
                                        <?php  foreach($product as $row){ ?>
                                            <option value="<?=$row['idproduct']; ?>"><?=$row['product_name']; ?></option>
                                        <?php } ?>                                  
                                    </select>   
                                    </td>
                                    <td>
                                         <select class="form-control col-sm-4" name="PurchasesDetail[0][uom]">
                                        <option></option> 
                                       
                                                                    
                                    </select>
                                    </td>
                                    <td><input type="text" name="PurchasesDetail[0][Purchasing_Quantity]"></td>
                                                                     
                                    <td>
                                    <button type="button" onclick="deleteRow(this)" class="btn btn-danger btn-sm"><i class="icon-cross3"></i></button>
                                    </td>
                                </tr>
                                
                            </tbody>
                             <tfoot>
                                <tr>
                                    <td colspan="5" class="text-center"><button type="button" onclick="AddNew()" class="btn btn-primary btn-sm"><i class=" icon-plus2"></i></button></td>

                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="panel-body">
                        <div class="row invoice-payment">
                            <div class="col-sm-12">
                                <div class="content-group col-sm-4">

 <?php 
        echo $form->field($model, 'supplier_idsupplier')->dropDownList(
                                ArrayHelper::map(Supplier::find()->all(),'idsupplier','supplier_name'), 
                                ['prompt'=>'Select...']);

    ?>
                                    
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="content-group">
                                      

                                    <div class="text-right" style="padding-right: 10%;">
                                       <?= Html::submitButton($model->isNewRecord ? '<i class="icon-paperplane"></i> Submit' : '<i class="icon-paperplane"></i> Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-success']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /invoice template -->



    

    <?php ActiveForm::end(); ?>

</div>
 <script type="text/javascript">
  var count = 0;
    function AddNew() {  
         count++;                   
        var row = " <tr><td><select data-placeholder=\"Select a product...\" class=\"select-search\" name=\"PurchasesDetail["+count+"][product]\"><option></option>  </select></td><td><select class=\"form-control col-sm-4\" name=\"PurchasesDetail["+count+"][uom]\"></select></td><td><input type=\"text\" name=\"PurchasesDetail["+count+"][Purchasing_Quantity]\"></td><td><button type=\"button\" onclick=\"deleteRow(this)\" class=\"btn btn-danger btn-sm\"><i class=\"icon-cross3\"></i></button></td></tr>";
        $('#tbody').append(row);
        
        

        $(".select-search").select2({
        minimumInputLength: 2,
        tags: [], 
        ajax: {
            url: 'index.php?r=issuing-header/product',
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (term) {
                return {
                    term: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.text,
                            //slug: item.slug,
                            id: item.id
                        }
                    })
                };
            }
        }
    }).on("change", function (e) {
                              //alert();
                    var sel = $(this).parents('td').next('td').find('select');         
                    $.ajax({
                        type: "GET",
                        url: "index.php?r=issuing-header/uom&id="+$(this).val(),                         
                    }).done(function (msg) {
                        
                           sel.html(msg);   
                    });

    });

    } 
   function deleteRow(btn) {
      var row = btn.parentNode.parentNode;
      row.parentNode.removeChild(row);
    }
 </script>