<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\Supplier;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model frontend\models\PurchasesHeaderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="purchases-header-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    
    <?= $form->field($model, 'date') ?>
 

    <?= $form->field($model, 'purchases_no') ?>


    <?php // echo $form->field($model, 'Date_Added') ?>

    <?php // echo $form->field($model, 'Added_By') ?>

    <?php // echo $form->field($model, 'Date_Updated') ?>

    <?php // echo $form->field($model, 'Updated_By') ?>

    <?php // echo $form->field($model, 'status') ?>

   <?php 
        echo $form->field($model, 'supplier_idsupplier')->dropDownList(
                                ArrayHelper::map(Supplier::find()->all(),'idsupplier','supplier_name'), 
                                ['prompt'=>'Select...']);

    ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
