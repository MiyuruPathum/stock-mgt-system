<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PurchasesHeader */

$this->title = 'Create Purchase Note';
$this->params['breadcrumbs'][] = ['label' => 'Purchases Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="purchases-header-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'product' => $product,
    ]) ?>

</div>
