<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\PurchasesHeader */

$this->title = $model->purchases_no;
$this->params['breadcrumbs'][] = ['label' => 'Purchases Notes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="purchases-header-view">

    


    <div class="panel panel-white">
                    <div class="panel-heading">
                        <h6 class="panel-title"> Purchase Note </h6>
                        <div class="heading-elements">
                                           <button type="button" class="btn btn-default btn-xs heading-btn"><i class="icon-printer position-left"></i> Print</button>
                        </div>
                    <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>

                    <div class="panel-body no-padding-bottom">
                        <div class="row">
                            <div class="col-md-6 content-group">
                                <img src="assets/images/logo_demo.png" class="content-group mt-10" alt="" style="width: 120px;">
                                <ul class="list-condensed list-unstyled">
                                     <li>Advanced Car Diagnostics</li>
                                    <li>1452/6 Malabe road,</li>
                                    <li>Pannipitiya</li>
                                </ul>
                            </div>

                            <div class="col-md-6 content-group">
                                <div class="invoice-details">
                                    <h5 class="text-uppercase text-semibold">Purchase No #<?=$model->purchases_no?></h5>
                                    <ul class="list-condensed list-unstyled">
                                        <li>Date: <span class="text-semibold"><?=$model->date?></span></li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>

                        
                    </div>

                    <div class="table-responsive">
                        <table class="table table-lg">
                            <thead>
                                <tr>
                                    <th>Description</th>
                                     <th class="col-sm-1">UOM</th> 
                                    <th class="col-sm-1">Qty</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $detial = \Yii::$app->db->createCommand("SELECT
                                product.product_name,
                                purchases_detail.Purchasing_Quantity,
                                product.product_code,
                                unit_of_measure.`code` AS uom
                                FROM
                                purchases_detail
                                INNER JOIN product ON purchases_detail.product = product.idproduct
                                INNER JOIN product_has_uom ON purchases_detail.product_has_uom = product_has_uom.idproduct_has_uom
                                INNER JOIN unit_of_measure ON product_has_uom.unit_of_measure_idunit_of_measure = unit_of_measure.idunit_of_measure
                                WHERE
                                    purchases_detail.purchases_header = '$model->idpurchases_header' ")->queryAll(); 
 
                            foreach ($detial as $row) {                                
                           
                            ?>
                                <tr>
                                    <td>
                                        <h6 class="no-margin"><?=$row['product_code']?></h6>
                                        <span class="text-muted"><?=$row['product_name']?></span>
                                    </td>
                                    <td><?=$row['uom']?></td>
                                    <td><?=$row['Purchasing_Quantity']?></td> 
                                </tr>
                             <?php } ?>   
                            </tbody>
                        </table>
                    </div>

                    <div class="panel-body">
                        <div class="row invoice-payment">
                            <div class="col-sm-7">
                                <h5> Supplier :
                                <?php
 $supplier = \Yii::$app->db->createCommand("SELECT * FROM supplier WHERE supplier.idsupplier=$model->supplier_idsupplier ")->queryOne(); 
    echo  $supplier['supplier_name'];
                                ?>
                                </h5>
                            </div>

                            <div class="col-sm-5">
                                <div class="content-group">
                                     
                                    <div class="table-responsive no-border">
                                        <table class="table">
                                            <tbody>
                                               
                                               <!--  <tr>
                                                    <th><h6>Total:</h6></th>
                                                    <td class="text-right text-primary"><h5 class="text-semibold">$8,750</h5></td>
                                                </tr> -->
                                            </tbody>
                                        </table>
                                    </div>

                                    
                                </div>
                            </div>
                        </div>

                       <!--  <h6>Other information</h6>
                        <p class="text-muted">Thank you for using Limitless. This invoice can be paid via PayPal, Bank transfer, Skrill or Payoneer. Payment is due within 30 days from the date of delivery. Late payment is possible, but with with a fee of 10% per month. Company registered in England and Wales #6893003, registered office: 3 Goodman Street, London E1 8BF, United Kingdom. Phone number: 888-555-2311</p>
                  -->   </div>
                </div>

</div>
