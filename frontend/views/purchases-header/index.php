<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PurchasesHeaderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Purchases Notes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="purchases-header-index">

    <div class="panel panel-flat  panel-collapsed">
        <div class="panel-heading">
            <h6 class="panel-title">Purchase Note <span class="text-semibold">Search</span></h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a class="" data-action="collapse"></a></li>                   
                   
                </ul>
            </div>
        <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
        
        <div style="display: block;" class="panel-body">
             <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>

    <p>
        <?= Html::a('Create Purchase Note', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idpurchases_header',
            'date',
            'time',
            'purchases_no',
            //'Total_Amount',
            // 'Date_Added',
            // 'Added_By',
            // 'Date_Updated',
            // 'Updated_By',
            // 'status',
            // 'supplier_idsupplier',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
