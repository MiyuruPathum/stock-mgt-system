<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PurchasesHeader */

$this->title = 'Update Purchases Header: ' . ' ' . $model->idpurchases_header;
$this->params['breadcrumbs'][] = ['label' => 'Purchases Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idpurchases_header, 'url' => ['view', 'id' => $model->idpurchases_header]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="purchases-header-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
