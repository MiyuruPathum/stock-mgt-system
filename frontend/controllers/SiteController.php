<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['about','contact'],
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
         if (\Yii::$app->user->isGuest) {
           
            return $this->redirect(Yii::$app->urlManager->createAbsoluteUrl('site/login'));
        }else{
            
             $reorder = \Yii::$app->db->createCommand('SELECT * FROM product WHERE product.availble_qty < product.reorder_level LIMIT 10
')->queryAll(); 

              $fastmv = \Yii::$app->db->createCommand('SELECT * FROM product WHERE product.availble_qty < product.reorder_level LIMIT 10
')->queryAll(); 
 
        
            return $this->render('index', [
                'reorder' => $reorder,
            ]);
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
         $this->layout = 'loginLayout';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

 
       /**
     * Displays report page.
     *
     * @return mixed
     */
    public function actionReorder()
    {
         $reorder = \Yii::$app->db->createCommand('SELECT * FROM product WHERE product.availble_qty < product.reorder_level')->queryAll(); 
        return $this->render('reorder', [
                'reorder' => $reorder,
            ]);  
    }

       /**
     * Displays report page.
     *
     * @return mixed
     */
    public function actionStock()
    {


         $data = \Yii::$app->db->createCommand('SELECT
                product_code,
                product_name,
                unit_of_measure,
                product_type,
                sub_category_name,
                warehouse.`name` as wname,
                availble_qty
                FROM
                product
                INNER JOIN sub_category ON product.sub_category = sub_category.idsub_category
                INNER JOIN warehouse ON product.idwarehouse = warehouse.idwarehouse')->queryAll(); 
        return $this->render('stock', [
                'data' => $data,
            ]);  
        
    }

     /**
     * Displays report page.
     *
     * @return mixed
     */
    public function actionAnalysis()
    {
        return $this->render('analysis');
    }

     /**
     * Displays report page.
     *
     * @return mixed
     */
    public function actionJobReport()
    {

          $data = \Yii::$app->db->createCommand("SELECT
                 issuing_header.date,
                 issuing_header.time,
                 issuing_header.issue_no,
                 warehouse.name as wname,
                 product.product_name,
                 issue_detail.qty,
                 issuing_header.vehicle_no
                FROM
                    issuing_header
                INNER JOIN issue_detail ON issuing_header.idissuing_header = issue_detail.issuing_header
                INNER JOIN product ON issue_detail.product = product.idproduct
                INNER JOIN warehouse ON product.idwarehouse = warehouse.idwarehouse
                LIMIT 15
                ")->queryAll(); 
        /** WHERE
                    issuing_header.idissuing_header = '34'
                OR issuing_header.vehicle_no = 'KJ-4654' */
         return $this->render('jobreport', [
                'data' => $data,
            ]);  
         
    }
}
