<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ProductHasUom;
use frontend\models\ProductHasUomSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductHasUomController implements the CRUD actions for ProductHasUom model.
 */
class ProductHasUomController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductHasUom models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductHasUomSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductHasUom model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductHasUom model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductHasUom();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idproduct_has_uom]);
        } else {
            $product = \Yii::$app->db->createCommand("SELECT product.idproduct, CONCAT( warehouse.`code`, '-', product.product_name ) AS product_name FROM product INNER JOIN warehouse ON product.idwarehouse = warehouse.idwarehouse  ")->queryAll();     

            return $this->render('create', [
                'model' => $model,
                'product' => $product,
            ]);
        }
    }

    /**
     * Updates an existing ProductHasUom model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idproduct_has_uom]);
        } else {
              $product = \Yii::$app->db->createCommand("SELECT product.idproduct, CONCAT( warehouse.`code`, '-', product.product_name ) AS product_name FROM product INNER JOIN warehouse ON product.idwarehouse = warehouse.idwarehouse  ")->queryAll();     

            return $this->render('update', [
                'model' => $model,
                'product'=>  $product,
            ]);
        }
    }

    /**
     * Deletes an existing ProductHasUom model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductHasUom model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductHasUom the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductHasUom::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
