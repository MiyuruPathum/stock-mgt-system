<?php
 
namespace frontend\controllers;

use Yii;
use frontend\models\IssuingHeader;
use frontend\models\IssuingHeaderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\Product;
use frontend\models\IssueDetail;
 
/**
 * IssuingHeaderController implements the CRUD actions for IssuingHeader model.
 */
class IssuingHeaderController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all IssuingHeader models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IssuingHeaderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single IssuingHeader model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new IssuingHeader model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new IssuingHeader(); 

        if ($model->load(Yii::$app->request->post())) {
 
          $issueno = \Yii::$app->db->createCommand('SELECT SUBSTR(issue_no,4) as issue_no FROM issuing_header ORDER BY issue_no DESC LIMIT 1')->queryOne(); 

          $invNo; 
          $issno = (int) $issueno['issue_no']; 
          if ($issno ==0) {
             $invNo = "ISN".sprintf("%'06d\n", 1);

          }else{ 
             $issno = $issno + 1;   

             $invNo = "ISN".sprintf("%'06d\n", $issno);
          }

         
          $model->date =date("Y-m-d");
          $model->time =date("H:i:s"); 
          $model->issue_no = $invNo;
          $model->Date_Added =date("Y-m-d H:i:s");                   
          $model->Added_By = "".Yii::$app->user->getId(); 
          $model->Updated_By = "".Yii::$app->user->getId();  
          $model->Date_Updated =date("Y-m-d H:i:s");         
          $model->status = "DONE"; 
         

         if ($model->save()) { 
               $submitData = Yii::$app->request->post('IssueDetail'); 
                 foreach ($submitData as $key => $row) {
                     $data = new IssueDetail();
                     $data->product = $row['product'];
                     $data->qty = $row['qty'];
                     $data->product_has_uom = $row['uom'];
                     $data->issuing_header =  $model->idissuing_header;
                     $data->save();
                     \Yii::$app->db->createCommand("UPDATE product SET availble_qty = (availble_qty-$row[qty]) where idproduct = '$row[product]' ")->execute(); 

                }
          }  

            return $this->redirect(['view', 'id' => $model->idissuing_header]);
        } else {
           $product = \Yii::$app->db->createCommand("SELECT product.idproduct, CONCAT( warehouse.`code`, '-', product.product_name ) AS product_name FROM product INNER JOIN warehouse ON product.idwarehouse = warehouse.idwarehouse  ")->queryAll();     
            return $this->render('create', [
                'model' => $model,
		'product' => $product,
            ]);
        }
    }
 
      
    public function actionProduct()
    {
        $q = Yii::$app->request->get('term');
        
	//$product = Product::find()->where('product_name LIKE :query')->addParams([':query'=>'%'.$q['term'].'%'])->all();
     $product = \Yii::$app->db->createCommand("SELECT product.idproduct, CONCAT( warehouse.`code`, '-', product.product_name ) AS product_name FROM product INNER JOIN warehouse ON product.idwarehouse = warehouse.idwarehouse WHERE product.product_name LIKE '%".$q['term']."%' ")->queryAll();     

     //    foreach($product as $row){
     //       echo "<option value=\"".$row['idproduct']  ."\">".$row['product_name'] ."</option>";
   	 // }


	if(count($product ) > 0){
   		foreach($product as $row){

		  $data[] = array('id' => $row['idproduct'], 'text' => $row['product_name']);			 	
 	        } 
	} else {
	   $data[] = array('id' => '0', 'text' => 'No Products Found');
	}

	
	echo json_encode($data);
 
    }	


    /**
     * Updates an existing IssuingHeader model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idissuing_header]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing IssuingHeader model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IssuingHeader model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IssuingHeader the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IssuingHeader::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

     /**
     * Creates a new IssuingHeader model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUom($id)
    {
         
            $uom = \Yii::$app->db->createCommand("SELECT product_has_uom.idproduct_has_uom, unit_of_measure.`code` FROM product_has_uom INNER JOIN unit_of_measure ON product_has_uom.unit_of_measure_idunit_of_measure = unit_of_measure.idunit_of_measure WHERE product_has_uom.product_idproduct ='".$id."' ")->queryAll(); 
            foreach ($uom as $row) {
               echo "<option value=\"".$row['idproduct_has_uom']."\">".$row['code']."</option>";
            }
    }
 
}
