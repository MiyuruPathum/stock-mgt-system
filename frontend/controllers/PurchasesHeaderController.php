<?php

namespace frontend\controllers;

use Yii;
use frontend\models\PurchasesHeader;
use frontend\models\PurchasesHeaderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\Product;
use frontend\models\PurchasesDetail;

/**
 * PurchasesHeaderController implements the CRUD actions for PurchasesHeader model.
 */
class PurchasesHeaderController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PurchasesHeader models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PurchasesHeaderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PurchasesHeader model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PurchasesHeader model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PurchasesHeader();

       
        if ($model->load(Yii::$app->request->post())) {
 

          $purchases_no = \Yii::$app->db->createCommand('SELECT SUBSTR(purchases_no,3) as purchases_no FROM purchases_header ORDER BY purchases_no DESC LIMIT 1')->queryOne(); 

          $invNo;  
          $pono = (int) $purchases_no['purchases_no']; 
          if ($pono ==0) {
             $invNo = "PN".sprintf("%'06d\n", 1);

          }else{ 
             $pono = $pono + 1;   
             
             $invNo = "PN".sprintf("%'06d\n", $pono);
          }

         
          $model->date =date("Y-m-d");
          $model->time =date("H:i:s"); 
          $model->purchases_no = $invNo;
          $model->Date_Added =date("Y-m-d H:i:s");                   
          $model->Added_By = "".Yii::$app->user->getId(); 
          $model->Updated_By = "".Yii::$app->user->getId();  
          $model->Date_Updated =date("Y-m-d H:i:s");         
          $model->status = "DONE"; 
         

         if ($model->save()) { 
               $submitData = Yii::$app->request->post('PurchasesDetail'); 
                 foreach ($submitData as $key => $row) {
                     $data = new PurchasesDetail();
                     $data->product = $row['product'];
                     $data->Purchasing_Quantity = $row['Purchasing_Quantity'];
                     $data->product_has_uom = $row['uom'];
                     $data->Total_Amount = "";
                     $data->purchases_header =  $model->idpurchases_header;
                     $data->save();
                     \Yii::$app->db->createCommand("UPDATE product SET availble_qty = (availble_qty+$row[Purchasing_Quantity]) where idproduct = '$row[product]' ")->execute(); 

                }
          } 



                    return $this->redirect(['view', 'id' => $model->idpurchases_header]);
        } else {
            $product = \Yii::$app->db->createCommand("SELECT product.idproduct, CONCAT( warehouse.`code`, '-', product.product_name ) AS product_name FROM product INNER JOIN warehouse ON product.idwarehouse = warehouse.idwarehouse  ")->queryAll();     
            return $this->render('create', [
                'model' => $model,
                'product' => $product,
            ]);
        }
    }

    /**
     * Updates an existing PurchasesHeader model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idpurchases_header]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PurchasesHeader model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PurchasesHeader model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PurchasesHeader the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PurchasesHeader::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
