<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "unit_of_measure".
 *
 * @property integer $idunit_of_measure
 * @property string $code
 * @property string $description
 *
 * @property ProductHasUom[] $productHasUoms
 */
class UnitOfMeasure extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit_of_measure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'string', 'max' => 5],
            [['description'], 'string', 'max' => 245]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idunit_of_measure' => 'Idunit Of Measure',
            'code' => 'Code',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductHasUoms()
    {
        return $this->hasMany(ProductHasUom::className(), ['unit_of_measure_idunit_of_measure' => 'idunit_of_measure']);
    }
}
