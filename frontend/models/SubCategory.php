<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sub_category".
 *
 * @property integer $idsub_category
 * @property string $sub_category_name
 * @property integer $category
 *
 * @property Product[] $products
 * @property Category $category0
 */
class SubCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category'], 'required'],
            [['category'], 'integer'],
            [['sub_category_name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idsub_category' => 'Idsub Category',
            'sub_category_name' => 'Sub Category Name',
            'category' => 'Category',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['sub_category' => 'idsub_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcategory()
    {
        return $this->hasOne(Category::className(), ['idcategory' => 'category']);
    }
}
