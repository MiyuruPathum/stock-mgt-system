<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "product_has_uom".
 *
 * @property integer $idproduct_has_uom
 * @property integer $product_idproduct
 * @property integer $unit_of_measure_idunit_of_measure
 * @property integer $base_unit
 * @property integer $qty
 *
 * @property IssueDetail[] $issueDetails
 * @property Product $productIdproduct
 * @property UnitOfMeasure $unitOfMeasureIdunitOfMeasure
 * @property ProductStock[] $productStocks
 * @property PurchasesDetail[] $purchasesDetails
 * @property ReturnDetail[] $returnDetails
 * @property UomConversion[] $uomConversions
 * @property UomConversion[] $uomConversions0
 */
class ProductHasUom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_has_uom';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_idproduct', 'unit_of_measure_idunit_of_measure'], 'required'],
            [['product_idproduct', 'unit_of_measure_idunit_of_measure', 'base_unit', 'qty'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idproduct_has_uom' => 'Idproduct Has Uom',
            'product_idproduct' => 'Product',
            'unit_of_measure_idunit_of_measure' => 'Unit Of Measure',
            'base_unit' => 'Base Unit',
            'qty' => 'Qty',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssueDetails()
    {
        return $this->hasMany(IssueDetail::className(), ['product_has_uom' => 'idproduct_has_uom']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['idproduct' => 'product_idproduct']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUom()
    {
        return $this->hasOne(UnitOfMeasure::className(), ['idunit_of_measure' => 'unit_of_measure_idunit_of_measure']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductStocks()
    {
        return $this->hasMany(ProductStock::className(), ['product_has_uom' => 'idproduct_has_uom']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchasesDetails()
    {
        return $this->hasMany(PurchasesDetail::className(), ['product_has_uom' => 'idproduct_has_uom']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnDetails()
    {
        return $this->hasMany(ReturnDetail::className(), ['product_has_uom' => 'idproduct_has_uom']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUomConversions()
    {
        return $this->hasMany(UomConversion::className(), ['bproduct_has_uom' => 'idproduct_has_uom']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUomConversions0()
    {
        return $this->hasMany(UomConversion::className(), ['product_has_uom' => 'idproduct_has_uom']);
    }
}
