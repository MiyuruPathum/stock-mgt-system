<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\IssuingHeader;

/**
 * IssuingHeaderSearch represents the model behind the search form about `frontend\models\IssuingHeader`.
 */
class IssuingHeaderSearch extends IssuingHeader
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idissuing_header'], 'integer'],
            [['date', 'time', 'issue_no', 'recieved_by', 'vehicle_no','Date_Added', 'Added_By', 'Date_Updated', 'Updated_By', 'job_ref','status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IssuingHeader::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idissuing_header' => $this->idissuing_header,
            'date' => $this->date,
            'time' => $this->time,
            'Date_Added' => $this->Date_Added,
            'Date_Updated' => $this->Date_Updated,
        ]);

        $query->andFilterWhere(['like', 'issue_no', $this->issue_no])
            //->andFilterWhere(['like', 'recieved_by', $this->recieved_by])
            ->andFilterWhere(['like', 'vehicle_no', $this->vehicle_no])
            //->andFilterWhere(['like', 'Updated_By', $this->Updated_By])
            //->andFilterWhere(['like', 'status', $this->status]);
            ->andFilterWhere(['like', 'job_ref', $this->job_ref]);

        return $dataProvider;
    }
}
