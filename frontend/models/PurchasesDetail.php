<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "purchases_detail".
 *
 * @property integer $idpurchases_detail
 * @property string $Purchasing_Quantity
 * @property string $Purchasing_Price
 * @property string $Selling_Price
 * @property string $Total_Amount
 * @property integer $product
 * @property integer $purchases_header
 * @property integer $product_has_uom
 *
 * @property ProductHasUom $productHasUom
 * @property Product $product0
 * @property PurchasesHeader $purchasesHeader
 */
class PurchasesDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchases_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product', 'purchases_header', 'product_has_uom'], 'required'],
            [['product', 'purchases_header', 'product_has_uom'], 'integer'],
            [['Purchasing_Quantity', 'Purchasing_Price', 'Selling_Price', 'Total_Amount'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idpurchases_detail' => 'Idpurchases Detail',
            'Purchasing_Quantity' => 'Purchasing  Quantity',
            'Purchasing_Price' => 'Purchasing  Price',
            'Selling_Price' => 'Selling  Price',
            'Total_Amount' => 'Total  Amount',
            'product' => 'Product',
            'purchases_header' => 'Purchases Header',
            'product_has_uom' => 'Product Has Uom',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductHasUom()
    {
        return $this->hasOne(ProductHasUom::className(), ['idproduct_has_uom' => 'product_has_uom']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct0()
    {
        return $this->hasOne(Product::className(), ['idproduct' => 'product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchasesHeader()
    {
        return $this->hasOne(PurchasesHeader::className(), ['idpurchases_header' => 'purchases_header']);
    }
}
