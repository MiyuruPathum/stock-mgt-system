<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "issue_detail".
 *
 * @property integer $idissue_detail
 * @property integer $product
 * @property string $qty
 * @property integer $issuing_header
 * @property integer $product_has_uom
 *
 * @property ProductHasUom $productHasUom
 * @property IssuingHeader $issuingHeader
 * @property Product $product0
 */
class IssueDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'issue_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product', 'issuing_header', 'product_has_uom'], 'required'],
            [['product', 'issuing_header', 'product_has_uom'], 'integer'],
            [['qty'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idissue_detail' => 'Idissue Detail',
            'product' => 'Product',
            'qty' => 'Qty',
            'issuing_header' => 'Issuing Header',
            'product_has_uom' => 'Product Has Uom',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductHasUom()
    {
        return $this->hasOne(ProductHasUom::className(), ['idproduct_has_uom' => 'product_has_uom']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssuingHeader()
    {
        return $this->hasOne(IssuingHeader::className(), ['idissuing_header' => 'issuing_header']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct0()
    {
        return $this->hasOne(Product::className(), ['idproduct' => 'product']);
    }
}
