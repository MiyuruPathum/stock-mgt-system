<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\ProductHasUom;

/**
 * ProductHasUomSearch represents the model behind the search form about `frontend\models\ProductHasUom`.
 */
class ProductHasUomSearch extends ProductHasUom
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idproduct_has_uom', 'product_idproduct', 'unit_of_measure_idunit_of_measure', 'base_unit', 'qty'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductHasUom::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idproduct_has_uom' => $this->idproduct_has_uom,
            'product_idproduct' => $this->product_idproduct,
            'unit_of_measure_idunit_of_measure' => $this->unit_of_measure_idunit_of_measure,
            'base_unit' => $this->base_unit,
            'qty' => $this->qty,
        ]);

        return $dataProvider;
    }
}
