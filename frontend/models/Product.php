<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $idproduct
 * @property string $product_code
 * @property string $product_name
 * @property string $reorder_level
 * @property string $unit_of_measure
 * @property string $product_type
 * @property string $barcode
 * @property integer $sub_category
 * @property integer $availble_qty
 * @property string $buying_price
 * @property string $selling_price
 * @property string $Added_By
 * @property string $Date_Updated
 * @property string $Updated_By
 * @property integer $idwarehouse
 *
 * @property IssueDetail[] $issueDetails
 * @property SubCategory $subCategory
 * @property Warehouse $idwarehouse0
 * @property PurchasesDetail[] $purchasesDetails
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_type'], 'string'],
            [['sub_category', 'idwarehouse'], 'required'],
            [['sub_category', 'availble_qty', 'idwarehouse'], 'integer'],
            [['buying_price', 'selling_price'], 'number'],
            [['Date_Updated'], 'safe'],
            [['product_code', 'product_name', 'reorder_level', 'unit_of_measure', 'barcode', 'Added_By', 'Updated_By'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idproduct' => 'Idproduct',
            'product_code' => 'Product Code',
            'product_name' => 'Product Name',
            'reorder_level' => 'Reorder Level',
            'unit_of_measure' => 'Unit Of Measure',
            'product_type' => 'Product Type',
            'barcode' => 'Barcode',
            'sub_category' => 'Sub Category',
            'availble_qty' => 'Available Qty',
            'buying_price' => 'Buying Price',
            'selling_price' => 'Selling Price',
            'Added_By' => 'Added  By',
            'Date_Updated' => 'Date  Updated',
            'Updated_By' => 'Updated  By',
            'idwarehouse' => 'Warehouse',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssueDetails()
    {
        return $this->hasMany(IssueDetail::className(), ['product' => 'idproduct']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubCategory()
    {
        return $this->hasOne(SubCategory::className(), ['idsub_category' => 'sub_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMwarehouse()
    {
        return $this->hasOne(Warehouse::className(), ['idwarehouse' => 'idwarehouse']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchasesDetails()
    {
        return $this->hasMany(PurchasesDetail::className(), ['product' => 'idproduct']);
    }
}
