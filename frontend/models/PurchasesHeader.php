<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "purchases_header".
 *
 * @property integer $idpurchases_header
 * @property string $date
 * @property string $time
 * @property string $purchases_no
 * @property string $Total_Amount
 * @property string $Date_Added
 * @property string $Added_By
 * @property string $Date_Updated
 * @property string $Updated_By
 * @property string $status
 * @property integer $supplier_idsupplier
 *
 * @property PurchasesDetail[] $purchasesDetails
 * @property Supplier $supplierIdsupplier
 */
class PurchasesHeader extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchases_header';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'time', 'Date_Added', 'Date_Updated'], 'safe'],
            [['supplier_idsupplier'], 'required'],
            [['supplier_idsupplier'], 'integer'],
            [['purchases_no', 'Total_Amount', 'Added_By', 'Updated_By', 'status'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idpurchases_header' => 'Idpurchases Header',
            'date' => 'Date',
            'time' => 'Time',
            'purchases_no' => 'Purchases No',
            'Total_Amount' => 'Total  Amount',
            'Date_Added' => 'Date  Added',
            'Added_By' => 'Added  By',
            'Date_Updated' => 'Date  Updated',
            'Updated_By' => 'Updated  By',
            'status' => 'Status',
            'supplier_idsupplier' => 'Supplier',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchasesDetails()
    {
        return $this->hasMany(PurchasesDetail::className(), ['purchases_header' => 'idpurchases_header']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplierIdsupplier()
    {
        return $this->hasOne(Supplier::className(), ['idsupplier' => 'supplier_idsupplier']);
    }
}
