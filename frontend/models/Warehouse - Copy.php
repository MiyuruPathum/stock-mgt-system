<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "warehouse".
 *
 * @property integer $idwarehouse
 * @property string $name
 *
 * @property Product[] $products
 */
class Warehouse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'warehouse';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idwarehouse' => 'Idwarehouse',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['idwarehouse' => 'idwarehouse']);
    }
}
