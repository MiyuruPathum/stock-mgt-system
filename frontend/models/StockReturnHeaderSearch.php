<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\StockReturnHeader;

/**
 * StockReturnHeaderSearch represents the model behind the search form about `frontend\models\StockReturnHeader`.
 */
class StockReturnHeaderSearch extends StockReturnHeader
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idstock_return_header', 'issuing_header'], 'integer'],
            [['date', 'time', 'return_no', 'recieved_by', 'job_ref'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StockReturnHeader::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idstock_return_header' => $this->idstock_return_header,
            'date' => $this->date,
            'time' => $this->time,
            'issuing_header' => $this->issuing_header,
        ]);

        $query->andFilterWhere(['like', 'return_no', $this->return_no])
            ->andFilterWhere(['like', 'recieved_by', $this->recieved_by])
            ->andFilterWhere(['like', 'job_ref', $this->job_ref]);

        return $dataProvider;
    }
}
