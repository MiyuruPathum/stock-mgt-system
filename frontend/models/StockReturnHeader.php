<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "stock_return_header".
 *
 * @property integer $idstock_return_header
 * @property string $date
 * @property string $time
 * @property string $return_no
 * @property string $recieved_by
 * @property string $job_ref
 * @property integer $issuing_header
 *
 * @property ReturnDetail[] $returnDetails
 * @property IssuingHeader $issuingHeader
 */
class StockReturnHeader extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock_return_header';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'time'], 'safe'],
            [['issuing_header'], 'required'],
            [['issuing_header'], 'integer'],
            [['return_no', 'recieved_by'], 'string', 'max' => 45],
            [['job_ref'], 'string', 'max' => 55]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idstock_return_header' => 'Idstock Return Header',
            'date' => 'Date',
            'time' => 'Time',
            'return_no' => 'Return No',
            'recieved_by' => 'Recieved By',
            'job_ref' => 'Job Ref',
            'issuing_header' => 'Issuing Header',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnDetails()
    {
        return $this->hasMany(ReturnDetail::className(), ['stock_return_header' => 'idstock_return_header']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssuingHeader()
    {
        return $this->hasOne(IssuingHeader::className(), ['idissuing_header' => 'issuing_header']);
    }
}
