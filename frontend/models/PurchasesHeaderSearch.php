<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\PurchasesHeader;

/**
 * PurchasesHeaderSearch represents the model behind the search form about `frontend\models\PurchasesHeader`.
 */
class PurchasesHeaderSearch extends PurchasesHeader
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idpurchases_header', 'supplier_idsupplier'], 'integer'],
            [['date', 'time', 'purchases_no', 'Total_Amount', 'Date_Added', 'Added_By', 'Date_Updated', 'Updated_By', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PurchasesHeader::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idpurchases_header' => $this->idpurchases_header,
            'date' => $this->date,
            'time' => $this->time,
            'Date_Added' => $this->Date_Added,
            'Date_Updated' => $this->Date_Updated,
            'supplier_idsupplier' => $this->supplier_idsupplier,
        ]);

        $query->andFilterWhere(['like', 'purchases_no', $this->purchases_no])
            ->andFilterWhere(['like', 'Total_Amount', $this->Total_Amount])
            ->andFilterWhere(['like', 'Added_By', $this->Added_By])
            ->andFilterWhere(['like', 'Updated_By', $this->Updated_By])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
