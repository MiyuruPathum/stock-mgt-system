<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "supplier".
 *
 * @property integer $idsupplier
 * @property string $supplier_name
 * @property string $address
 * @property string $contact_person
 * @property string $phone
 * @property string $Fax
 * @property string $email
 *
 * @property PurchasesDetail[] $purchasesDetails
 */
class Supplier extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'supplier';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['supplier_name', 'contact_person', 'phone', 'Fax', 'email'], 'string', 'max' => 45],
            [['address'], 'string', 'max' => 245]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idsupplier' => 'Idsupplier',
            'supplier_name' => 'Supplier Name',
            'address' => 'Address',
            'contact_person' => 'Contact Person',
            'phone' => 'Phone',
            'Fax' => 'Fax',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchasesDetails()
    {
        return $this->hasMany(PurchasesDetail::className(), ['supplier' => 'idsupplier']);
    }
}
