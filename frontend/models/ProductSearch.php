<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Product;

/**
 * ProductSearch represents the model behind the search form about `frontend\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idproduct', 'sub_category', 'availble_qty', 'idwarehouse'], 'integer'],
            [['product_code', 'product_name', 'reorder_level', 'unit_of_measure', 'product_type', 'barcode', 'Added_By', 'Date_Updated', 'Updated_By'], 'safe'],
            [['buying_price', 'selling_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idproduct' => $this->idproduct,
            'sub_category' => $this->sub_category,
            'availble_qty' => $this->availble_qty,
            'buying_price' => $this->buying_price,
            'selling_price' => $this->selling_price,
            'Date_Updated' => $this->Date_Updated,
            'idwarehouse' => $this->idwarehouse,
        ]);

        $query->andFilterWhere(['like', 'product_code', $this->product_code])
            ->andFilterWhere(['like', 'product_name', $this->product_name])
            ->andFilterWhere(['like', 'reorder_level', $this->reorder_level])
            ->andFilterWhere(['like', 'unit_of_measure', $this->unit_of_measure])
            ->andFilterWhere(['like', 'product_type', $this->product_type])
            ->andFilterWhere(['like', 'barcode', $this->barcode])
            ->andFilterWhere(['like', 'Added_By', $this->Added_By])
            ->andFilterWhere(['like', 'Updated_By', $this->Updated_By]);

        return $dataProvider;
    }
}
