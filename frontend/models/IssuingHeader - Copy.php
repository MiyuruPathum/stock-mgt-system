<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "issuing_header".
 *
 * @property integer $idissuing_header
 * @property string $date
 * @property string $time
 * @property string $issue_no
 * @property string $recieved_by
 * @property string $Date_Added
 * @property string $Added_By
 * @property string $Date_Updated
 * @property string $Updated_By
 * @property string $status
 *
 * @property IssueDetail[] $issueDetails
 */
class IssuingHeader extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'issuing_header';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['date', 'time', 'Date_Added', 'Date_Updated'], 'safe'],
            [['issue_no', 'recieved_by', 'Added_By', 'Updated_By','vehicle_no', 'status'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idissuing_header' => 'Idissuing Header',
            'date' => 'Date',
            'time' => 'Time',
            'issue_no' => 'Issue No',
            'recieved_by' => 'Recieved By',
            'vehicle_no' => 'Vehicle No',
            'Date_Added' => 'Date  Added',
            'Added_By' => 'Added  By',
            'Date_Updated' => 'Date  Updated',
            'Updated_By' => 'Updated  By',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssueDetails()
    {
        return $this->hasMany(IssueDetail::className(), ['issuing_header' => 'idissuing_header']);
    }
}
