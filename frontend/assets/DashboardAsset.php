<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
           'https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900',
        'assets/css/icons/icomoon/styles.css',
        'assets/css/bootstrap.css',
        'assets/css/core.css',
        'assets/css/components.css',
        'assets/css/colors.css',


    ];
    public $js = [    
        'assets/js/plugins/loaders/pace.min.js',
        'assets/js/core/libraries/jquery.min.js',
        'assets/js/core/libraries/bootstrap.min.js',
        'assets/js/plugins/loaders/blockui.min.js',       
        'assets/js/plugins/visualization/d3/d3.min.js',
        'assets/js/plugins/visualization/d3/d3_tooltip.js',
        'assets/js/plugins/forms/styling/switchery.min.js',
        'assets/js/plugins/forms/styling/uniform.min.js',
         'assets/js/plugins/forms/selects/bootstrap_multiselect.js',
         'assets/js/plugins/ui/moment/moment.min.js',
         'assets/js/plugins/pickers/daterangepicker.js', 
         'assets/js/core/app.js',         
         'assets/js/pages/dashboard.js',
          'assets/js/core/libraries/jquery_ui/interactions.min.js',
         'assets/js/plugins/forms/selects/select2.min.js',
         'assets/js/pages/form_select2.js',
          
         
    ]; 
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}