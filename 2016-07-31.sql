/*
Navicat MySQL Data Transfer

Source Server         : 162.243.123.72_3306
Source Server Version : 50546
Source Host           : 162.243.123.72:3306
Source Database       : stock

Target Server Type    : MYSQL
Target Server Version : 50546
File Encoding         : 65001

Date: 2016-07-31 19:35:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `category`
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `idcategory` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idcategory`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', 'Accessories');

-- ----------------------------
-- Table structure for `issue_detail`
-- ----------------------------
DROP TABLE IF EXISTS `issue_detail`;
CREATE TABLE `issue_detail` (
  `idissue_detail` int(11) NOT NULL AUTO_INCREMENT,
  `product` int(11) NOT NULL,
  `qty` varchar(45) DEFAULT NULL,
  `issuing_header` int(11) NOT NULL,
  PRIMARY KEY (`idissue_detail`),
  KEY `fk_issue_detail_product1_idx` (`product`) USING BTREE,
  KEY `fk_issue_detail_issuing_header1_idx` (`issuing_header`) USING BTREE,
  CONSTRAINT `issue_detail_ibfk_1` FOREIGN KEY (`issuing_header`) REFERENCES `issuing_header` (`idissuing_header`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `issue_detail_ibfk_2` FOREIGN KEY (`product`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of issue_detail
-- ----------------------------
INSERT INTO `issue_detail` VALUES ('15', '87', '1', '15');
INSERT INTO `issue_detail` VALUES ('16', '155', '2', '15');
INSERT INTO `issue_detail` VALUES ('17', '124', '1', '15');
INSERT INTO `issue_detail` VALUES ('18', '119', '8', '15');
INSERT INTO `issue_detail` VALUES ('19', '156', '1', '15');
INSERT INTO `issue_detail` VALUES ('20', '87', '1', '16');
INSERT INTO `issue_detail` VALUES ('21', '155', '2', '16');
INSERT INTO `issue_detail` VALUES ('22', '124', '1', '16');
INSERT INTO `issue_detail` VALUES ('23', '119', '8', '16');
INSERT INTO `issue_detail` VALUES ('24', '58', '2', '16');
INSERT INTO `issue_detail` VALUES ('25', '63', '2', '16');
INSERT INTO `issue_detail` VALUES ('26', '389', '1', '16');
INSERT INTO `issue_detail` VALUES ('27', '390', '1', '16');
INSERT INTO `issue_detail` VALUES ('28', '562', '1', '17');
INSERT INTO `issue_detail` VALUES ('29', '304', '1', '17');
INSERT INTO `issue_detail` VALUES ('30', '420', '1', '17');
INSERT INTO `issue_detail` VALUES ('31', '87', '1', '18');
INSERT INTO `issue_detail` VALUES ('32', '155', '2', '18');
INSERT INTO `issue_detail` VALUES ('33', '124', '1', '18');
INSERT INTO `issue_detail` VALUES ('34', '118', '8', '18');
INSERT INTO `issue_detail` VALUES ('35', '3', '1', '18');
INSERT INTO `issue_detail` VALUES ('36', '565', '', '19');
INSERT INTO `issue_detail` VALUES ('37', '565', '1', '20');
INSERT INTO `issue_detail` VALUES ('38', '421', '1', '21');
INSERT INTO `issue_detail` VALUES ('44', '161', '1', '24');
INSERT INTO `issue_detail` VALUES ('45', '87', '1', '25');
INSERT INTO `issue_detail` VALUES ('46', '155', '2', '25');
INSERT INTO `issue_detail` VALUES ('47', '124', '1', '25');
INSERT INTO `issue_detail` VALUES ('48', '119', '8', '25');
INSERT INTO `issue_detail` VALUES ('49', '245', '2', '26');
INSERT INTO `issue_detail` VALUES ('50', '421', '1', '27');
INSERT INTO `issue_detail` VALUES ('51', '81', '1', '28');
INSERT INTO `issue_detail` VALUES ('52', '401', '1', '28');
INSERT INTO `issue_detail` VALUES ('53', '399', '1', '28');
INSERT INTO `issue_detail` VALUES ('54', '420', '1', '29');
INSERT INTO `issue_detail` VALUES ('55', '302', '1', '29');
INSERT INTO `issue_detail` VALUES ('56', '448', '2', '29');
INSERT INTO `issue_detail` VALUES ('57', '453', '2', '30');
INSERT INTO `issue_detail` VALUES ('58', '421', '1', '31');
INSERT INTO `issue_detail` VALUES ('59', '157', '1', '32');
INSERT INTO `issue_detail` VALUES ('60', '157', '1', '33');
INSERT INTO `issue_detail` VALUES ('61', '32', '1', '34');
INSERT INTO `issue_detail` VALUES ('62', '36', '1', '34');
INSERT INTO `issue_detail` VALUES ('63', '582', '1', '35');
INSERT INTO `issue_detail` VALUES ('64', '55', '1', '35');
INSERT INTO `issue_detail` VALUES ('65', '427', '1', '35');
INSERT INTO `issue_detail` VALUES ('66', '398', '1', '35');
INSERT INTO `issue_detail` VALUES ('67', '87', '1', '35');
INSERT INTO `issue_detail` VALUES ('68', '155', '2', '35');
INSERT INTO `issue_detail` VALUES ('69', '124', '1', '35');
INSERT INTO `issue_detail` VALUES ('70', '118', '8', '35');
INSERT INTO `issue_detail` VALUES ('71', '3', '1', '35');
INSERT INTO `issue_detail` VALUES ('72', '414', '1', '35');
INSERT INTO `issue_detail` VALUES ('73', '423', '1', '36');
INSERT INTO `issue_detail` VALUES ('74', '55', '1', '36');
INSERT INTO `issue_detail` VALUES ('75', '421', '1', '37');
INSERT INTO `issue_detail` VALUES ('76', '113', '2', '37');
INSERT INTO `issue_detail` VALUES ('77', '62', '1', '37');
INSERT INTO `issue_detail` VALUES ('78', '78', '1', '37');
INSERT INTO `issue_detail` VALUES ('79', '411', '1', '37');

-- ----------------------------
-- Table structure for `issuing_header`
-- ----------------------------
DROP TABLE IF EXISTS `issuing_header`;
CREATE TABLE `issuing_header` (
  `idissuing_header` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `issue_no` varchar(45) DEFAULT NULL,
  `recieved_by` varchar(45) DEFAULT NULL,
  `vehicle_no` varchar(255) DEFAULT NULL,
  `Date_Added` datetime DEFAULT NULL,
  `Added_By` varchar(45) DEFAULT NULL,
  `Date_Updated` datetime DEFAULT NULL,
  `Updated_By` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `job_ref` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`idissuing_header`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of issuing_header
-- ----------------------------
INSERT INTO `issuing_header` VALUES ('15', '2016-06-25', '15:13:31', 'ISN000005\n', 'KK-9337', 'Asela', '2016-06-25 15:13:31', '1', '2016-06-25 15:13:31', '1', 'DONE', '');
INSERT INTO `issuing_header` VALUES ('16', '2016-07-06', '15:03:01', 'ISN000006\n', 'Dinusha', 'KH-0735', '2016-07-06 15:03:01', '1', '2016-07-06 15:03:01', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('17', '2016-07-06', '15:21:53', 'ISN000007\n', 'Menaka', 'KH-6450', '2016-07-06 15:21:53', '1', '2016-07-06 15:21:53', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('18', '2016-07-06', '15:22:37', 'ISN000008\n', 'Supun', 'KK-4118', '2016-07-06 15:22:37', '1', '2016-07-06 15:22:38', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('19', '2016-07-06', '15:43:40', 'ISN000009\n', 'Sumudu', 'GG-9424', '2016-07-06 15:43:40', '1', '2016-07-06 15:43:40', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('20', '2016-07-06', '15:44:09', 'ISN000010\n', 'Sumudu', 'GG-9424', '2016-07-06 15:44:09', '1', '2016-07-06 15:44:09', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('21', '2016-07-06', '15:44:53', 'ISN000011\n', 'Dinusha', 'KH-0735', '2016-07-06 15:44:53', '1', '2016-07-06 15:44:53', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('24', '2016-07-07', '15:12:45', 'ISN000012\n', 'Naveen', 'KK-5904', '2016-07-07 15:12:45', '1', '2016-07-07 15:12:45', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('25', '2016-07-07', '15:15:08', 'ISN000013\n', 'Dinusha & Asela', 'PF-5780', '2016-07-07 15:15:08', '1', '2016-07-07 15:15:08', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('26', '2016-07-07', '15:26:14', 'ISN000014\n', 'Dinusha', 'KI-0223', '2016-07-07 15:26:14', '1', '2016-07-07 15:26:14', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('27', '2016-07-07', '15:27:01', 'ISN000015\n', 'Asanka', 'KR-1275', '2016-07-07 15:27:01', '1', '2016-07-07 15:27:01', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('28', '2016-07-07', '15:31:29', 'ISN000016\n', 'Kumara', 'KJ-6751', '2016-07-07 15:31:29', '1', '2016-07-07 15:31:29', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('29', '2016-07-07', '15:37:49', 'ISN000017\n', 'Menaka', 'KH-6450', '2016-07-07 15:37:49', '1', '2016-07-07 15:37:49', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('30', '2016-07-07', '15:39:46', 'ISN000018\n', 'Naveen', 'KP-8554', '2016-07-07 15:39:46', '1', '2016-07-07 15:39:46', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('31', '2016-07-07', '15:40:32', 'ISN000019\n', 'Naveen', 'KH-9388', '2016-07-07 15:40:32', '1', '2016-07-07 15:40:32', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('32', '2016-07-07', '15:41:05', 'ISN000020\n', 'Asela', 'KH-4874', '2016-07-07 15:41:05', '1', '2016-07-07 15:41:05', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('33', '2016-07-07', '15:41:33', 'ISN000021\n', 'Asela', 'KK-1402', '2016-07-07 15:41:33', '1', '2016-07-07 15:41:33', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('34', '2016-07-07', '15:42:55', 'ISN000022\n', 'Naveen', 'KJ-4654', '2016-07-07 15:42:55', '1', '2016-07-07 15:42:55', '1', 'DONE', '4510');
INSERT INTO `issuing_header` VALUES ('35', '2016-07-08', '17:00:46', 'ISN000023\n', 'Asela & Chanaka', 'KK-8071', '2016-07-08 17:00:46', '1', '2016-07-08 17:00:46', '1', 'DONE', '4510');
INSERT INTO `issuing_header` VALUES ('36', '2016-07-08', '17:02:28', 'ISN000024\n', 'Kumara', 'KJ-6751', '2016-07-08 17:02:28', '1', '2016-07-08 17:02:28', '1', 'DONE', null);
INSERT INTO `issuing_header` VALUES ('37', '2016-07-08', '17:17:44', 'ISN000025\n', 'Dinusha, Supun & Dinusha', 'KK-8071', '2016-07-08 17:17:44', '1', '2016-07-08 17:17:44', '1', 'DONE', null);

-- ----------------------------
-- Table structure for `migration`
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m000000_000000_base', '1465738643');
INSERT INTO `migration` VALUES ('m130524_201442_init', '1465738647');

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `idproduct` int(11) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(45) DEFAULT NULL,
  `product_name` varchar(200) DEFAULT NULL,
  `reorder_level` varchar(45) DEFAULT NULL,
  `unit_of_measure` varchar(45) DEFAULT NULL,
  `product_type` enum('Consumable','Service','Stockable') DEFAULT NULL,
  `barcode` varchar(45) DEFAULT NULL,
  `sub_category` int(11) NOT NULL,
  `availble_qty` int(11) DEFAULT NULL,
  `buying_price` decimal(10,2) DEFAULT NULL,
  `selling_price` decimal(10,2) DEFAULT NULL,
  `Added_By` varchar(45) DEFAULT NULL,
  `Date_Updated` datetime DEFAULT NULL,
  `Updated_By` varchar(45) DEFAULT NULL,
  `idwarehouse` int(11) NOT NULL,
  PRIMARY KEY (`idproduct`),
  KEY `fk_product_sub_category1_idx` (`sub_category`),
  KEY `fk_product_warehouse1_idx` (`idwarehouse`),
  CONSTRAINT `fk_product_sub_category1` FOREIGN KEY (`sub_category`) REFERENCES `sub_category` (`idsub_category`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_warehouse1` FOREIGN KEY (`idwarehouse`) REFERENCES `warehouse` (`idwarehouse`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=711 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('3', '', 'Air filter Kyron', '5', 'Box', 'Stockable', '', '1', '3', null, null, '', '2016-07-30 14:59:25', 'admin', '1');
INSERT INTO `product` VALUES ('4', '', 'Air filter Rexton', '5', 'Box', 'Stockable', '', '1', '2', null, null, '', '2016-07-28 20:09:58', 'admin', '1');
INSERT INTO `product` VALUES ('5', '', 'Air filter Rexton G', '', 'Box', 'Stockable', '', '1', '2', null, null, '', '2016-07-26 12:51:21', 'admin', '2');
INSERT INTO `product` VALUES ('6', '', 'Air filterAqua (JS) A - 21031', '5', 'Box', 'Stockable', '', '1', '5', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('7', '', 'Air filter Honda -  GP5 (JS - 28013)', '5', 'Box', 'Stockable', '', '1', '5', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('8', '', 'Air filter Prius (JS -A- 1010) ', '2', 'Box', 'Stockable', '', '1', '2', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('9', '', 'Air filter Toyota Prado (G)  (17801 - 30040)', '2', 'Box', 'Stockable', '', '1', '2', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('10', '', 'Air filter Prado (JS -A-194 J)', '2', 'Box', 'Stockable', '', '1', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('11', '', 'Air filter Sakura (A - 1181)', '2', 'Box', 'Stockable', '', '1', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('12', '', 'Air filter Sakura (A - 2816)', '2', 'Box', 'Stockable', '', '1', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('13', '', 'Air filter Sakura (A - 1174)', '', 'Box', 'Stockable', '', '1', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('14', '', 'Air filter Sakura (A - 1180)', '', 'Box', 'Stockable', '', '1', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('15', '', 'Air filter Sakura (A- 1123)', '', 'Box', 'Stockable', '', '1', '1', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('16', '', 'Air filter Wagon- R AZUMI (A- 22976)', '5', 'Box', 'Stockable', '', '1', '19', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('17', '', 'Air Filter Korando(G)', '5', 'Box', 'Stockable', '', '1', '6', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('18', '', 'Air filter KIA Sorento', '3', 'Box', 'Stockable', '', '1', '3', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('19', '', 'Air Filter KIA Sportage', '3', 'Box', 'Stockable', '', '1', '4', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('20', '', 'Air filter Tivoli', '3', 'Box', 'Stockable', '', '1', '3', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('21', '', 'Air filter Free Lander 2 (Britpart)', '2', 'Box', 'Stockable', '', '22', '0', null, null, '', '2016-07-30 17:34:22', 'admin', '1');
INSERT INTO `product` VALUES ('22', '', 'Air filter Free Lander 2 (G)', '2', 'Box', 'Stockable', '', '22', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('23', '', 'Air filter Discover 4 (G)', '1', 'Box', 'Stockable', '', '22', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('24', '', 'Air filter Discover 4 (Britpart)', '3', 'Box', 'Stockable', '', '22', '3', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('25', '', 'Air filter Evoque (MANN)', '1', 'Box', 'Stockable', '', '22', '1', null, null, '', '2016-07-30 17:34:37', 'admin', '1');
INSERT INTO `product` VALUES ('26', '', 'Brake pad set -Front Mando Kyron and Rexton', '5', 'Box', 'Stockable', '', '2', '3', null, null, '', '2016-07-30 12:54:18', 'admin', '1');
INSERT INTO `product` VALUES ('27', '', 'Brake pad set Rear - Mando Kyron and Rexton', '5', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('28', '', 'Brake pad set Rear - TRW Kyron and Rexton', '', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('29', '', 'Brake pad set Rear - SB Kyron and Rexton', '', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('30', '', 'Brake pad set Rear- NIBK Kyron and Rexton', '', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('31', '', 'Brake pad set  Rear - HI-Q Kyron and Rexton', '5', 'Box', 'Stockable', '', '2', '3', null, null, '', '2016-07-30 12:54:02', 'admin', '1');
INSERT INTO `product` VALUES ('32', '', 'Front brake pad set - Mando korando & sorento', '5', 'Box', 'Stockable', '', '2', '4', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('33', '', 'Rear brake pad set - Mando KIA Sorento', '', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('35', '', 'Rear brake pad set - Mando Korando', '', 'Box', 'Stockable', '', '2', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('36', '', 'Rear brake pad set - HI-Q Korando', '5', 'Box', 'Stockable', '', '2', '22', null, null, '', '2016-07-25 14:13:42', 'admin', '1');
INSERT INTO `product` VALUES ('37', '', 'Brake pad set Front Discover 4 - Britpart', '2', 'Box', 'Stockable', '', '22', '0', null, null, '', '2016-07-30 17:31:24', 'admin', '1');
INSERT INTO `product` VALUES ('38', '', 'Brake pad set Rear  Discovery 4 - Britpart', '2', 'Box', 'Stockable', '', '22', '2', null, null, '', '2016-07-25 14:16:25', 'admin', '1');
INSERT INTO `product` VALUES ('39', '', 'Brake pad set Front Discovery 4 - TRW', '1', 'Box', 'Stockable', '', '22', '1', null, null, '', '2016-07-25 14:16:46', 'admin', '1');
INSERT INTO `product` VALUES ('40', '', 'Brake pad set RearDiscovery 4 - TRW', '', 'Box', 'Stockable', '', '22', '0', null, null, '', '2016-07-25 14:16:54', 'admin', '1');
INSERT INTO `product` VALUES ('41', '', 'Brake pad set Front Discovery 4 (G)', '', 'Box', 'Stockable', '', '22', '1', null, null, '', '2016-07-25 14:17:04', 'admin', '1');
INSERT INTO `product` VALUES ('42', '', 'Brake pad set Front Free Lander 2 / EVOQUE - Britpart', '', 'Box', 'Stockable', '', '22', '0', null, null, '', '2016-07-26 12:52:14', 'admin', '1');
INSERT INTO `product` VALUES ('44', '', 'Brake pad set Front Free Lander 2 - Ferado', '1', 'Box', 'Stockable', '', '22', '2', null, null, '', '2016-07-25 14:18:51', 'admin', '1');
INSERT INTO `product` VALUES ('45', '', 'Brake pad set Rear Free Lander 2 - Ferado', '1', 'Box', 'Stockable', '', '22', '2', null, null, '', '2016-07-25 14:19:41', 'admin', '1');
INSERT INTO `product` VALUES ('46', '', 'Brake pad set Front Free Lander 2 / EVOQUE - Britpart', '1', 'Box', 'Stockable', '', '22', '0', null, null, '', '2016-07-25 16:10:25', 'admin', '1');
INSERT INTO `product` VALUES ('49', '', 'Brake pad set  Rear Free Lander 2 / Evoque - Britpart', '', 'Box', 'Stockable', '', '22', '0', null, null, '', '2016-07-30 17:35:56', 'admin', '1');
INSERT INTO `product` VALUES ('50', '', 'Brake pad set Front - Evoque - TRW', '', 'Box', 'Stockable', '', '22', '0', null, null, '', '2016-07-30 17:36:16', 'admin', '1');
INSERT INTO `product` VALUES ('51', '', 'Brake pad set Rear - Evoque - TRW', '1', 'Box', 'Stockable', '', '2', '2', null, null, '', '2016-07-30 17:36:35', 'admin', '1');
INSERT INTO `product` VALUES ('52', '', 'Brake pad set Front Defender 2.4 - TRW ', '', 'Box', 'Stockable', '', '22', '1', null, null, '', '2016-07-25 14:39:57', 'admin', '1');
INSERT INTO `product` VALUES ('53', '', 'Brake pad set Rear  Defender 2.4 - TRW', '', 'Box', 'Stockable', '', '22', '2', null, null, '', '2016-07-25 14:40:03', 'admin', '1');
INSERT INTO `product` VALUES ('54', '', 'H - 27 Fog bulb', '', 'Box', 'Stockable', '', '3', '11', null, null, '', '2016-07-28 20:10:43', 'admin', '1');
INSERT INTO `product` VALUES ('55', '', 'H - 7 Dim light', '', 'Box', 'Stockable', '', '3', '18', null, null, '', '2016-07-26 17:15:06', 'admin', '1');
INSERT INTO `product` VALUES ('56', '', 'H - 1 Head light', '', 'Box', 'Stockable', '', '3', '11', null, null, '', '2016-07-30 13:14:45', 'admin', '1');
INSERT INTO `product` VALUES ('57', '', 'H - 4 Bulb', '', 'Box', 'Stockable', '', '3', '13', null, null, '', '2016-07-25 16:12:43', 'admin', '1');
INSERT INTO `product` VALUES ('58', '', 'Double filament bulb', '', 'bulbs', 'Stockable', '', '3', '72', null, null, '', '2016-07-30 12:53:08', 'admin', '1');
INSERT INTO `product` VALUES ('59', '', 'Single filament bulb', '', 'bulbs', 'Stockable', '', '3', '24', null, null, '', '2016-07-29 14:48:05', 'admin', '1');
INSERT INTO `product` VALUES ('60', '', 'Double filament capless', '', 'bulbs', 'Stockable', '', '3', '11', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('61', '', 'Single filament capless', '', 'bulbs', 'Stockable', '', '3', '7', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('62', '', 'Hood Bulb', '', 'bulbs', 'Stockable', '', '3', '65', null, null, '', '2016-07-30 14:59:08', 'admin', '1');
INSERT INTO `product` VALUES ('63', '', 'Capless parking bulb', '', 'bulbs', 'Stockable', '', '3', '129', null, null, '', '2016-07-28 20:10:29', 'admin', '1');
INSERT INTO `product` VALUES ('64', '', 'Capless small', '', 'bulbs', 'Stockable', '', '3', '6', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('65', '', 'Capless medium', '', 'bulbs', 'Stockable', '', '3', '7', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('66', '', 'Capless orange', '', 'bulbs', 'Stockable', '', '3', '6', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('67', '', 'Dash board light', '', 'bulbs', 'Stockable', '', '3', '22', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('68', '', 'Signal bulb big', '', 'bulbs', 'Stockable', '', '3', '27', null, null, '', '2016-07-25 16:18:10', 'admin', '1');
INSERT INTO `product` VALUES ('69', '', 'Signal bulb small', '', 'bulbs', 'Stockable', '', '3', '11', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('70', '', 'H - B3 Fog (LR)', '', 'bulbs', 'Stockable', '', '3', '6', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('71', '', 'H - 4 (LR)', '', 'bulbs', 'Stockable', '', '3', '10', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('72', '', 'HID H-7', '', 'bulbs', 'Stockable', '', '3', '3', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('73', '', 'HID H-1', '', 'bulbs', 'Stockable', '', '3', '2', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('74', '', 'HID H - 1 Blaster', '', 'units', 'Stockable', '', '3', '8', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('76', '', '25 A - Big ', '', 'units', 'Stockable', '', '4', '13', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('78', '', '20 A - small ', '', 'units', 'Stockable', '', '4', '50', null, null, '', '2016-07-25 16:43:54', 'admin', '1');
INSERT INTO `product` VALUES ('79', '', '20 A - Big', '', 'units', 'Stockable', '', '4', '7', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('80', '', '30 A - Big ', '', 'units', 'Stockable', '', '4', '18', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('81', '', '15 A- Small', '', 'units', 'Stockable', '', '4', '102', null, null, '', '2016-07-25 16:38:40', 'admin', '1');
INSERT INTO `product` VALUES ('82', '', '7.5 A - Small', '', 'units', 'Stockable', '', '4', '84', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('83', '', '15 A - Big', '', 'units', 'Stockable', '', '4', '10', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('84', '', '10 A - Big', '', 'units', 'Stockable', '', '4', '27', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('86', '', '5 A - Big', '', 'units', 'Stockable', '', '4', '17', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('87', '', 'Oil filter Ssangyong', '10', 'Box', 'Stockable', '', '5', '10', null, null, '', '2016-07-30 18:14:08', 'admin', '1');
INSERT INTO `product` VALUES ('88', '', 'Oil filter Korando', '', 'Box', 'Stockable', '', '5', '28', null, null, '', '2016-07-30 12:49:34', 'admin', '1');
INSERT INTO `product` VALUES ('89', '', 'Oil filter Sorento', '5', 'Box', 'Stockable', '', '5', '12', null, null, '', '2016-07-28 13:58:37', 'admin', '1');
INSERT INTO `product` VALUES ('90', '', 'Oil filter Sportage', '2', 'Box', 'Stockable', '', '5', '2', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('91', '', 'Oil filter (JS C-809J)', '5', 'Box', 'Stockable', '', '5', '13', null, null, '', '2016-07-25 16:50:42', 'admin', '1');
INSERT INTO `product` VALUES ('92', '', 'Oil filter Honda (G) C-809', '5', 'Box', 'Stockable', '', '5', '13', null, null, '', '2016-07-25 16:53:03', 'admin', '1');
INSERT INTO `product` VALUES ('93', '', 'Oil filter Waggon R (JS C-932)', '', 'Box', 'Stockable', '', '5', '0', null, null, '', '0000-00-00 00:00:00', '', '1');
INSERT INTO `product` VALUES ('94', '', 'Oil filter Waggon R (G) C-932', '5', 'Box', 'Stockable', '', '5', '16', null, null, '', '2016-07-30 14:56:46', 'admin', '1');
INSERT INTO `product` VALUES ('95', '', 'Oil filter Waggon R (Azumi - 932)', '5', 'Box', 'Stockable', '', '5', '8', null, null, '', '2016-07-28 14:33:42', 'admin', '1');
INSERT INTO `product` VALUES ('96', '', 'Oil filter Toyota (G) - 110', '5', 'Box', 'Stockable', '', '5', '10', null, null, '', '2016-07-26 14:45:43', 'admin', '1');
INSERT INTO `product` VALUES ('97', '', 'Oil filter Toyota (G) - 117', '', 'Box', 'Stockable', '', '5', '6', null, null, '', '2016-07-28 13:59:21', 'admin', '1');
INSERT INTO `product` VALUES ('98', '', 'Oil filter Mitsubishi outlander (G)', '', 'Box', 'Stockable', '', '5', '3', null, null, '', '2016-07-25 16:55:00', 'admin', '1');
INSERT INTO `product` VALUES ('99', '', 'Oil filter Mitsubishi Montero Sport (G)', '', 'Box', 'Stockable', '', '5', '2', null, null, '', '2016-07-25 16:55:10', 'admin', '1');
INSERT INTO `product` VALUES ('101', '', 'Oil filter JS - C 415', '', 'Box', 'Stockable', '', '5', '5', null, null, '', '2016-07-25 16:55:34', 'admin', '1');
INSERT INTO `product` VALUES ('102', '', 'Oil filter JS - C 111', '', 'Box', 'Stockable', '', '5', '1', null, null, '', '2016-07-28 14:04:12', 'admin', '1');
INSERT INTO `product` VALUES ('103', '', 'Oil filter JS - OE 116 J (Prado) ', '', 'Box', 'Stockable', '', '5', '3', null, null, '', '2016-07-25 16:57:58', 'admin', '1');
INSERT INTO `product` VALUES ('104', '', 'Air Filter KIA Sportage', '1', 'Box', 'Stockable', '', '1', '2', null, null, null, '2016-07-26 12:58:53', 'admin', '2');
INSERT INTO `product` VALUES ('105', '', 'Upper arm bush', '12', 'units', 'Stockable', '', '6', '52', null, null, null, '2016-07-26 14:24:26', 'admin', '1');
INSERT INTO `product` VALUES ('106', '', 'Link mount bush', '10', 'Units', 'Stockable', '', '6', '23', null, null, null, '2016-07-26 14:24:50', 'admin', '1');
INSERT INTO `product` VALUES ('107', '', 'Lower arm big bush', '12', 'Units', 'Stockable', '', '6', '34', null, null, null, '2016-07-26 14:25:10', 'admin', '1');
INSERT INTO `product` VALUES ('108', '', 'Lower arm small bush', '12', 'Units', 'Stockable', '', '6', '34', null, null, null, '2016-07-26 14:25:24', 'admin', '1');
INSERT INTO `product` VALUES ('109', '', 'Rack end', '10', 'Units', 'Stockable', '', '7', '19', null, null, null, '2016-07-30 16:52:39', 'admin', '1');
INSERT INTO `product` VALUES ('111', '', 'Bumper Mount', '10', 'Units', 'Stockable', '', '7', '10', null, null, null, '2016-07-30 17:40:50', 'admin', '1');
INSERT INTO `product` VALUES ('112', '', 'Bumper Mount', '', 'Units', 'Stockable', '', '7', '3', null, null, null, '2016-07-26 13:05:16', 'admin', '2');
INSERT INTO `product` VALUES ('113', '', 'Rack Boot', '10', 'Units', 'Stockable', '', '7', '12', null, null, null, '2016-07-30 16:51:25', 'admin', '1');
INSERT INTO `product` VALUES ('114', '', 'Lower Arm Ball Joint', '5', 'Units', 'Stockable', '', '7', '15', null, null, null, '2016-07-30 16:51:45', 'admin', '1');
INSERT INTO `product` VALUES ('115', '', 'Upper Arm Bush', '', 'Units', 'Stockable', '', '6', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('116', '', 'Brake pad set Front Discover 4 (G)', '', 'Box', 'Stockable', '', '22', '1', null, null, null, '2016-07-26 13:05:41', 'admin', '1');
INSERT INTO `product` VALUES ('117', '', 'Brake pad set Rear Free Lander 2 - TRW', '', 'Box', 'Stockable', '', '22', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('118', '', 'Mobil 15w 40', '', 'Liters', 'Stockable', '', '8', '151', null, null, null, '2016-07-30 14:58:35', 'admin', '1');
INSERT INTO `product` VALUES ('119', '', 'Castrol 15w 40 - RX', '', 'Liters', 'Stockable', '', '8', '39', null, null, null, '2016-07-30 18:14:35', 'admin', '1');
INSERT INTO `product` VALUES ('120', '', 'Castrol oil petrol 10w 30', '', 'Liters', 'Stockable', '', '8', '50', null, null, null, '2016-07-30 14:57:53', 'admin', '1');
INSERT INTO `product` VALUES ('121', '', 'Air flow sensor Kyron', '1', 'Box', 'Stockable', '', '10', '1', null, null, null, '2016-07-28 14:09:17', 'admin', '1');
INSERT INTO `product` VALUES ('122', '', 'ATF conditioner (BG)', '5', 'Can', 'Stockable', '', '26', '6', null, null, null, '2016-07-30 18:13:37', 'admin', '1');
INSERT INTO `product` VALUES ('123', '', 'Head gasket set - Kyron', '2', 'units', 'Stockable', '', '11', '4', null, null, null, '2016-07-25 17:00:35', 'admin', '1');
INSERT INTO `product` VALUES ('124', '', 'Glass cleaner', '', 'Bottles', 'Stockable', '', '12', '80', null, null, null, '2016-07-30 14:57:05', 'admin', '1');
INSERT INTO `product` VALUES ('125', '', 'Blue coolent - (Wynn\'s)', '', 'liters', 'Stockable', '', '12', '20', null, null, null, '2016-07-25 17:02:45', 'admin', '1');
INSERT INTO `product` VALUES ('126', '', 'Red coolent - (Wynn\'s)', '', 'liters', 'Stockable', '', '12', '6', null, null, null, '2016-07-25 17:03:00', 'admin', '1');
INSERT INTO `product` VALUES ('127', '', 'Green coolant - Sinomax', '', 'liters', 'Stockable', '', '12', '58', null, null, null, '2016-07-30 17:28:19', 'admin', '1');
INSERT INTO `product` VALUES ('128', '', 'Crank oil seal - Kyron Front', '', 'units', 'Stockable', '', '13', '5', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('129', '', 'Crank oil seal - Kyron Rear', '', 'units', 'Stockable', '', '13', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('130', '', 'Crank oil seal - Kyron Front', '', 'units', 'Stockable', '', '13', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('131', '', 'Crank oil seal - Kyron Rear', '', 'units', 'Stockable', '', '13', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('132', '', 'Axle oil seal - Power Up', '', 'units', 'Stockable', '', '13', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('133', '', 'Axle oil seal - Korando', '', 'units', 'Stockable', '', '13', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('134', '', 'Gear box oil seal - 6 speed', '', 'units', 'Stockable', '', '13', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('135', '', 'ABS sensor 4wd', '', 'units', 'Stockable', '', '10', '30', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('136', '', 'Wheel speed sensor 2wd Rear - Ky/Act.', '', 'units', 'Stockable', '', '10', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('137', '', 'Wheel speed sensor 2wd Rear - Ky/Act.', '', 'units', 'Stockable', '', '10', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('138', '', 'Wheel speed sensor 4wd front - Ky / Act.', '', 'units', 'Stockable', '', '10', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('139', '', 'Coolant temperature sensor', '', 'units', 'Stockable', '', '10', '4', null, null, null, '2016-07-25 17:05:29', 'admin', '1');
INSERT INTO `product` VALUES ('140', '', 'Coolant temperature sensor', '', 'units', 'Stockable', '', '10', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('141', '', 'Wheel speed sensor - Output', '', 'Box', 'Stockable', '', '10', '2', null, null, null, '2016-07-25 17:05:17', 'admin', '1');
INSERT INTO `product` VALUES ('142', '', 'Wheel speed sensor - Input', '', 'Box', 'Stockable', '', '10', '1', null, null, null, '2016-07-25 17:05:39', 'admin', '1');
INSERT INTO `product` VALUES ('143', '', 'Cam sensor', '', 'units', 'Stockable', '', '10', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('144', '', 'Knock sensor', '', 'units', 'Stockable', '', '10', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('145', '', 'Crank sensor', '', 'units', 'Stockable', '', '10', '4', null, null, null, '2016-07-25 17:06:20', 'admin', '1');
INSERT INTO `product` VALUES ('146', '', 'Water separator sensor', '', 'units', 'Stockable', '', '10', '0', null, null, null, '2016-07-25 17:06:52', 'admin', '1');
INSERT INTO `product` VALUES ('147', '', 'Booster pressure sensor - Kyron', '', 'Box', 'Stockable', '', '10', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('148', '', 'Booster pressure sensor - Rexton', '', 'Box', 'Stockable', '', '10', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('150', '', 'Head gasket set - Kyron', '', 'units', 'Stockable', '', '11', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('151', '', 'Head gasket set - Rexton', '', 'units', 'Stockable', '', '11', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('152', '', 'Diesel filter - KFT', '5', 'Box', 'Stockable', '', '14', '3', null, null, null, '2016-07-29 13:39:15', 'admin', '2');
INSERT INTO `product` VALUES ('153', '', 'Diesel filter - OEM', '2', 'Box', 'Stockable', '', '14', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('154', '', 'Diesel filter - Korando', '3', 'Box', 'Stockable', '', '14', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('155', '', 'Vacum filter', '', 'units', 'Stockable', '', '15', '183', null, null, null, '2016-07-30 17:24:40', 'admin', '1');
INSERT INTO `product` VALUES ('156', '', 'Sump nut \"14\"', '10', 'units', 'Stockable', '', '15', '7', null, null, null, '2016-07-25 17:07:51', 'admin', '1');
INSERT INTO `product` VALUES ('157', '', 'Sump nut \"17\"', '', 'units', 'Stockable', '', '15', '2', null, null, null, '2016-07-25 17:08:00', 'admin', '1');
INSERT INTO `product` VALUES ('158', '', 'Castrol oil (20W 50)', '', 'liters', 'Stockable', '', '8', '31', null, null, null, '2016-07-25 17:08:43', 'admin', '1');
INSERT INTO `product` VALUES ('159', '', 'Castrol oil (5W 30) Land Rover (Drum)', '', 'liters', 'Stockable', '', '8', '64', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('160', '', 'ATF fluid (3353)', '', 'liters', 'Stockable', '', '8', '12', null, null, null, '2016-07-25 17:10:28', 'admin', '1');
INSERT INTO `product` VALUES ('161', '', 'ATF fluid Synthetic (BG)', '', 'liters', 'Stockable', '', '26', '30', null, null, null, '2016-07-28 13:55:05', 'admin', '1');
INSERT INTO `product` VALUES ('163', '', 'Toyota oil (G) (0W 20)', '', 'liters', 'Stockable', '', '8', '11', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('164', '', 'Toyota oil (G) (10W 30)', '', 'liters', 'Stockable', '', '8', '8', null, null, null, '2016-07-25 17:13:16', 'admin', '1');
INSERT INTO `product` VALUES ('165', '', 'Motul specific oil (5W 30)', '', 'liters', 'Stockable', '', '8', '21', null, null, null, '2016-07-25 17:13:47', 'admin', '1');
INSERT INTO `product` VALUES ('166', '', 'Sinopec GL - 5 (80W 90)', '', 'liters', 'Stockable', '', '8', '15', null, null, null, '2016-07-25 17:14:38', 'admin', '1');
INSERT INTO `product` VALUES ('167', '', 'Honda ATF DW-1 oil (G)', '', 'Can', 'Stockable', '', '8', '2', null, null, null, '2016-07-25 17:17:42', 'admin', '1');
INSERT INTO `product` VALUES ('168', '', 'Syncro shift 2', '', 'liters', 'Stockable', '', '8', '2', null, null, null, '2016-07-25 17:19:20', 'admin', '1');
INSERT INTO `product` VALUES ('169', '', 'Axle shaft', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('170', '', 'Accelarator padel - Kyron', '', 'Box', 'Stockable', '', '15', '1', null, null, null, '2016-07-25 17:19:34', 'admin', '1');
INSERT INTO `product` VALUES ('171', '', 'Accelarator padel - Rexton', '', 'Box', 'Stockable', '', '15', '2', null, null, null, '2016-07-30 12:56:50', 'admin', '1');
INSERT INTO `product` VALUES ('172', '', 'Axle C.V boot', '', 'units', 'Stockable', '', '15', '0', null, null, null, '2016-07-26 13:19:55', 'admin', '2');
INSERT INTO `product` VALUES ('173', '', 'Air filter Rexton (G)', '', 'Box', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('174', '', 'A/C filter Rexton (G)', '', 'Box', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('175', '', 'Air filter KIA', '', 'Box', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('176', '', 'Alternator cluctch (G)', '', 'Box', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('177', '', 'Air cleaner hose', '', 'units', 'Stockable', '', '15', '1', null, null, null, '2016-07-26 16:44:11', 'admin', '2');
INSERT INTO `product` VALUES ('179', '', 'Battery terminal (+)', '', 'units', 'Stockable', '', '15', '3', null, null, null, '2016-07-26 13:20:56', 'admin', '2');
INSERT INTO `product` VALUES ('180', '', 'Battery terminal (-)', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('181', '', 'Brake light switch ', '', 'Box', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('183', '', 'Brake hose Rear - L/H', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('186', '', 'Ball joint nut', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('188', '', 'Brake disk - Front (G)', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('189', '', 'Brake disk - Rear  Kyron (KGC)', '', 'units', 'Stockable', '', '15', '1', null, null, null, '2016-07-25 17:20:21', 'admin', '1');
INSERT INTO `product` VALUES ('191', '', 'Bonnet shock - Rexton', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('192', '', 'Bolt caliper - Front ', '', 'units', 'Stockable', '', '15', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('193', '', 'Brake caliper - Front L/H', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('195', '', 'Booster vacum hose', '', 'units', 'Stockable', '', '15', '14', null, null, null, '2016-07-26 17:05:04', 'admin', '2');
INSERT INTO `product` VALUES ('196', '', 'Bumper hood guide', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('197', '', 'Body mount lower bush', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('198', '', 'Brake caliper clip', '', 'units', 'Stockable', '', '15', '13', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('199', '', 'Body mount washer', '', 'units', 'Stockable', '', '15', '10', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('200', '', 'Body mount upper bush', '', 'units', 'Stockable', '', '15', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('201', '', 'Body mount bracket', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('202', '', 'Benjo nut', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('203', '', 'Bumper pad - Rexton', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('204', '', 'Bumper pad  - Kyron', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('205', '', 'Body mount washer (6011605000)', '', 'units', 'Stockable', '', '15', '20', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('206', '', 'Body mount washer (6014108000)', '', 'units', 'Stockable', '', '15', '20', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('207', '', 'Body mount washer (6013108000)', '', 'units', 'Stockable', '', '15', '20', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('208', '', 'Body mount washer (6011108000)', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('209', '', 'Body mount washer (6012108000)', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('210', '', 'Bolt     (4619806001)', '', 'units', 'Stockable', '', '15', '24', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('211', '', 'Body mount                 (6011208000)', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('214', '', 'Clutch master', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('215', '', 'Clutch kit', '', 'Box', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('216', '', 'Clutch release cylinder', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('217', '', 'Clamp', '', 'units', 'Stockable', '', '15', '7', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('218', '', 'Clutch switch(Inter lock)', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('220', '', 'Complete A/C fan', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('223', '', 'Camber washer', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('224', '', 'Camber nut', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('225', '', 'Camber bolt - big', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('226', '', 'Camber bolt - small', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('228', '', 'Cutter valve', '', 'units', 'Stockable', '', '15', '20', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('229', '', 'brake hose clip', '', 'units', 'Stockable', '', '15', '7', null, null, null, '2016-07-26 15:19:01', 'admin', '2');
INSERT INTO `product` VALUES ('234', '', 'Rear \'D\' bush', '', 'units', 'Stockable', '', '15', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('237', '', 'Door protector', '', 'units', 'Stockable', '', '15', '7', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('245', '', 'Engine mount - Kyron', '', 'units', 'Stockable', '', '15', '1', null, null, null, '2016-07-25 17:28:27', 'admin', '1');
INSERT INTO `product` VALUES ('246', '', 'Engine mount - Rexton', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('248', '', 'EGR pipe', '', 'units', 'Stockable', '', '15', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('251', '', 'Engine oil - Deepstick', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('253', '', '4wd solenoid ', '', 'units', 'Stockable', '', '15', '3', null, null, null, '2016-07-30 17:32:44', 'admin', '2');
INSERT INTO `product` VALUES ('254', '', '4wd one way valve', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('255', '', 'Fog lamp front - Actyon L/H', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('256', '', 'Full return tube', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('257', '', 'Fender signal light - Rexton ', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('258', '', 'Fender signal light - Kyron ', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('259', '', 'Fan assy ', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('261', '', 'Grill - Kyron', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('262', '', 'Gear box mount', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('263', '', ' Gasket manifold', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('264', '', 'Gasket pipe EGR', '', 'units', 'Stockable', '', '15', '6', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('265', '', 'Glass shocks', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('266', '', 'Heater timer - Rexton', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('267', '', 'Heater timer - Kyron', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('268', '', 'Heater plug', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('269', '', 'Hand primer ', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('270', '', 'Head light cover', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('271', '', 'Hanger (Silancer mount)', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('272', '', 'Head lamp cover - Kyron', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('273', '', 'Hub bearing 2wd', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('274', '', 'Iddle pully with groove', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('275', '', 'Iddle pully without groove', '', 'Box', 'Stockable', '', '15', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('276', '', 'Injector bolt', '', 'units', 'Stockable', '', '15', '15', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('277', '', 'Pipe - Intercooler Out', '', 'units', 'Stockable', '', '15', '5', null, null, null, '2016-07-26 17:04:42', 'admin', '2');
INSERT INTO `product` VALUES ('278', '', 'Injector pipe', '', 'units', 'Stockable', '', '15', '10', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('279', '', 'Key ', '', 'units', 'Stockable', '', '15', '7', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('281', '', 'Lower arm small bush', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('284', '', 'Front link bush', '', 'units', 'Stockable', '', '6', '47', null, null, null, '2016-07-26 14:25:50', 'admin', '1');
INSERT INTO `product` VALUES ('285', '', 'Front link - L/H', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('286', '', 'Front link R/H', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('288', '', 'Lower iddle pully - Kyron', '', 'units', 'Stockable', '', '15', '7', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('289', '', 'Link rod - Power up', '', 'units', 'Stockable', '', '15', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('290', '', 'Lower arm small bush - Power up', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('291', '', 'LED mirror cover - Rexton L/H', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('293', '', 'Light cover - Actyon', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('294', '', 'Master pump (Clutch pump) - Rexton', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('296', '', 'Make up hose', '', 'units', 'Stockable', '', '15', '2', null, null, null, '2016-07-26 16:53:51', 'admin', '2');
INSERT INTO `product` VALUES ('297', '', 'Nozzel assy', '', 'units', 'Stockable', '', '15', '13', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('298', '', 'Oil separator ', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('299', '', 'Oil filter cover', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('300', '', 'Oil filler tube cap ', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('301', '', 'oil separator hose ', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('302', '', 'oil cooler hose - Inlet ', '', 'units', 'Stockable', '', '15', '10', null, null, null, '2016-07-28 14:07:37', 'admin', '2');
INSERT INTO `product` VALUES ('303', '', 'Oil cooler \'O\' ring - big', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('304', '', 'Oil filler housing packing', '', 'units', 'Stockable', '', '15', '-1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('305', '', 'Oil return tube - Turbo', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('306', '', 'Power steering pump ', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('308', '', 'Power window switch - Rexton ', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('309', '', 'Power window switch - Kyron', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('310', '', 'Reflector rear - L/H', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('311', '', 'Reflector rear - R/H', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('312', '', 'Radiator hose - Inlet', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('313', '', 'Radiator hose - outlet', '', 'units', 'Stockable', '', '15', '5', null, null, null, '2016-07-26 16:55:43', 'admin', '2');
INSERT INTO `product` VALUES ('314', '', 'Rear bush (4562105000)', '', 'units', 'Stockable', '', '15', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('315', '', 'Rear bush (4542105000)', '', 'units', 'Stockable', '', '15', '6', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('316', '', 'Rear link', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('317', '', 'Rack end', '', 'units', 'Stockable', '', '15', '1', null, null, null, '2016-07-25 15:52:26', 'admin', '2');
INSERT INTO `product` VALUES ('318', '', 'Radiator - Rexton', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('319', '', 'Radiator - Kyron (6 Speed)', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('320', '', 'Radiator - Kyron (5 Speed)', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('321', '', 'Reservior tank', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('323', '', 'Rear balance bar bush - Power Up', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('324', '', 'Rear link - Power Up L/H', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('325', '', 'rear link - Power Up R/H', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('326', '', 'Return hose ', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('328', '', 'Seperator hose ', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('329', '', 'shockabsober - Front - Kyron', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('330', '', 'shockabsober - Rear - Kyron', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('331', '', 'shockabsober - Front - Rexton', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('332', '', 'shockabsober -Rear - Rexton', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('333', '', 'Sun visor - Rex', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('338', '', 'Side mirror glass - Kyron - L/H', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('339', '', 'Side mirror glass - Kyron - R/H', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('340', '', 'Side mirror glass - Rexton - L/H', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('342', '', 'Sump nut', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('343', '', 'Shock mount (Shockabsober)', '', 'units', 'Stockable', '', '15', '6', null, null, null, '2016-07-26 16:34:13', 'admin', '2');
INSERT INTO `product` VALUES ('344', '', ' Shock boot ', '', 'units', 'Stockable', '', '15', '4', null, null, null, '2016-07-26 16:34:34', 'admin', '2');
INSERT INTO `product` VALUES ('345', '', 'Striker assy T- gate', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('346', '', 'Screw plug', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('347', '', 'Selector lever bush', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('349', '', 'Timing chain tensioner', '', 'units', 'Stockable', '', '15', '17', null, null, null, '2016-07-25 17:28:56', 'admin', '1');
INSERT INTO `product` VALUES ('350', '', 'Tie rod end - L/H', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('351', '', 'Tie rod end - R/H', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('352', '', 'Transponder immobilizer', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('353', '', 'Track rod - Power Up', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('354', '', 'Turbo inter cooler', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('355', '', 'Turbo hose', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('356', '', 'Timing chain ', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('357', '', 'Timing chain guard (Guide EGR pipe)', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('358', '', 'Under guard - Kyron', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('359', '', 'Under guard - Rexton', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('360', '', 'Upper arm bush - Power Up', '', 'units', 'Stockable', '', '7', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('361', '', 'Vacum modulator ', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('362', '', 'Vacum hose - Inlet ', '', 'units', 'Stockable', '', '15', '12', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('363', '', 'Vacum hose - Outlet', '', 'units', 'Stockable', '', '15', '17', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('364', '', 'Vacum pump ', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('365', '', 'Vacum pump \'O\' ring', '', 'units', 'Stockable', '', '15', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('368', '', 'Wiper washer tank motor - Kyron', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('369', '', 'Wiper washer tank motor - Rexton', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('371', '', 'Washer (black)', '', 'units', 'Stockable', '', '15', '13', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('372', '', 'Injector washer', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('373', '', 'Wheel Cup - Kyron', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('374', '', 'Wheel Cup - Rexton', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('375', '', 'Radiator washer', '', 'units', 'Stockable', '', '15', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('376', '', 'Wheel nut', '', 'units', 'Stockable', '', '15', '6', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('377', '', 'Wheel arch cover - Rexton', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('378', '', 'Wiper arm front L/H', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('379', '', 'Washer and Tube (6011308000)', '', 'units', 'Stockable', '', '15', '20', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('380', '', 'Washer and Tube (6013308000)', '', 'units', 'Stockable', '', '15', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('381', '', 'Washer and Tube (6015309A00)', '', 'units', 'Stockable', '', '15', '20', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('382', '', 'Washer and Tube (6016308002)', '', 'units', 'Stockable', '', '15', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('383', '', 'Window regulator ', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('384', '', 'Wheel bolt', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('385', '', 'Wiper arm cap', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('387', '', '10 A - Small', '', 'units', 'Stockable', '', '4', '62', null, null, null, '2016-07-25 16:43:27', 'admin', '1');
INSERT INTO `product` VALUES ('388', '', 'Hybrid fuse', '', 'units', 'Stockable', '', '4', '42', null, null, null, '2016-07-25 16:44:09', 'admin', '1');
INSERT INTO `product` VALUES ('389', '', 'Wiper blade (Max clear) \"19\"', '', 'units', 'Stockable', '', '16', '0', null, null, null, '2016-07-30 12:55:58', 'admin', '1');
INSERT INTO `product` VALUES ('390', '', 'Wiper blade (Max clear) \"21\"', '', 'units', 'Stockable', '', '16', '0', null, null, null, '2016-07-30 12:56:08', 'admin', '1');
INSERT INTO `product` VALUES ('391', '', 'Wiper blade (Max clear) \"16\"', '', 'units', 'Stockable', '', '16', '15', null, null, null, '2016-07-25 17:35:16', 'admin', '1');
INSERT INTO `product` VALUES ('392', '', 'Wiper blade (Max clear) \"24\"', '', 'units', 'Stockable', '', '16', '8', null, null, null, '2016-07-25 17:35:45', 'admin', '1');
INSERT INTO `product` VALUES ('393', '', 'Wiper blade (Max clear) \"26\"', '', 'units', 'Stockable', '', '16', '5', null, null, null, '2016-07-25 17:36:01', 'admin', '1');
INSERT INTO `product` VALUES ('394', '', 'Wiper blade (Boneless) \"16\"', '', 'units', 'Stockable', '', '16', '1', null, null, null, '2016-07-25 17:36:14', 'admin', '1');
INSERT INTO `product` VALUES ('395', '', 'Wiper blade (Boneless) \"14\"', '', 'units', 'Stockable', '', '16', '4', null, null, null, '2016-07-25 17:36:31', 'admin', '1');
INSERT INTO `product` VALUES ('396', '', 'Wiper blade (KCW) \"525\"', '', 'units', 'Stockable', '', '16', '0', null, null, null, '2016-07-25 17:29:37', 'admin', '1');
INSERT INTO `product` VALUES ('397', '', 'Wiper blade (KCW) \"450\"', '', 'units', 'Stockable', '', '16', '0', null, null, null, '2016-07-25 17:37:01', 'admin', '1');
INSERT INTO `product` VALUES ('398', '', 'Rear wiper blade (G)', '', 'units', 'Stockable', '', '16', '6', null, null, null, '2016-07-25 17:37:20', 'admin', '1');
INSERT INTO `product` VALUES ('399', '', 'Door lock motor - Rexton Front (L/H)', '', 'Box', 'Stockable', '', '17', '4', null, null, null, '2016-07-30 16:53:10', 'admin', '1');
INSERT INTO `product` VALUES ('400', '', 'Door lock motor - Rexton Front (R/H)', '', 'Box', 'Stockable', '', '17', '1', null, null, null, '2016-07-30 16:53:17', 'admin', '1');
INSERT INTO `product` VALUES ('401', '', 'Door lock motor - Rexton Rear (L/H)', '', 'Box', 'Stockable', '', '17', '3', null, null, null, '2016-07-30 16:53:44', 'admin', '1');
INSERT INTO `product` VALUES ('402', '', 'Door lock motor - Rexton Rear (R/H)', '', 'Box', 'Stockable', '', '17', '5', null, null, null, '2016-07-25 17:38:41', 'admin', '1');
INSERT INTO `product` VALUES ('403', '', 'Door lock motor - Kyron Front (L/H)', '', 'units', 'Stockable', '', '17', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('404', '', 'Door lock motor - Kyron Front (R/H)', '', 'units', 'Stockable', '', '17', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('405', '', 'Door lock motor - Kyron Rear (L/H)', '', 'units', 'Stockable', '', '17', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('406', '', 'Door lock motor - Kyron Rear (R/H)', '', 'units', 'Stockable', '', '17', '1', null, null, null, '2016-07-25 17:41:42', 'admin', '1');
INSERT INTO `product` VALUES ('407', '', 'Door lock motor - Korando Front (L/H)', '', 'units', 'Stockable', '', '17', '0', null, null, null, '2016-07-25 17:42:20', 'admin', '1');
INSERT INTO `product` VALUES ('408', '', 'Door lock motor - Korando Front (R/H)', '', 'units', 'Stockable', '', '17', '2', null, null, null, '2016-07-25 17:39:51', 'admin', '1');
INSERT INTO `product` VALUES ('409', '', 'Door lock motor - Korando Rear (L/H)', '', 'units', 'Stockable', '', '17', '0', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('410', '', 'Door lock motor - Korando Rear (R/H)', '', 'units', 'Stockable', '', '17', '0', null, null, null, '2016-07-25 17:42:46', 'admin', '1');
INSERT INTO `product` VALUES ('411', '', 'Key housing', '', 'units', 'Stockable', '', '15', '16', null, null, null, '2016-07-30 16:52:52', 'admin', '1');
INSERT INTO `product` VALUES ('412', '', 'Heater plug (NGK)', '', 'units', 'Stockable', '', '15', '11', null, null, null, '2016-07-25 17:43:55', 'admin', '1');
INSERT INTO `product` VALUES ('413', '', 'Idle pully with groove - Kyron', '', 'Box', 'Stockable', '', '15', '4', null, null, null, '2016-07-25 17:43:40', 'admin', '1');
INSERT INTO `product` VALUES ('415', '', 'Brake light switch', '', 'Box', 'Stockable', '', '15', '7', null, null, null, '2016-07-30 12:53:26', 'admin', '1');
INSERT INTO `product` VALUES ('416', '', 'Brake light switch wire', '', 'units', 'Stockable', '', '15', '8', null, null, null, '2016-07-30 12:53:34', 'admin', '1');
INSERT INTO `product` VALUES ('417', '', 'Battery water bottle', '', 'bottles', 'Stockable', '', '15', '20', null, null, null, '2016-07-29 14:07:45', 'admin', '1');
INSERT INTO `product` VALUES ('418', '', 'Oil cooler housing packing ', '', 'units', 'Stockable', '', '15', '23', null, null, null, '2016-07-25 17:48:47', 'admin', '1');
INSERT INTO `product` VALUES ('419', '', 'Thermostat valve', '', 'Box', 'Stockable', '', '15', '7', null, null, null, '2016-07-30 17:26:38', 'admin', '1');
INSERT INTO `product` VALUES ('420', '', 'Thermostat valve \"O\" ring', '', 'units', 'Stockable', '', '15', '11', null, null, null, '2016-07-25 17:46:53', 'admin', '1');
INSERT INTO `product` VALUES ('421', '', 'Injector Cleaner (MAX 44)', '', 'Bottles', 'Stockable', '', '15', '35', null, null, null, '2016-07-30 14:58:48', 'admin', '1');
INSERT INTO `product` VALUES ('422', '', 'Oil cooler hose - Inlet', '', 'units', 'Stockable', '', '15', '9', null, null, null, '2016-07-28 14:08:09', 'admin', '1');
INSERT INTO `product` VALUES ('423', '', 'Tail light - Rexton', '', 'units', 'Stockable', '', '7', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('424', '', 'Oil lifter', '', 'units', 'Stockable', '', '15', '27', null, null, null, '2016-07-25 17:48:35', 'admin', '1');
INSERT INTO `product` VALUES ('425', '', 'Injector assy - Kyron', '', 'units', 'Stockable', '', '15', '4', null, null, null, '2016-07-25 17:50:39', 'admin', '1');
INSERT INTO `product` VALUES ('426', '', 'Front link - Korando', '', 'units', 'Stockable', '', '7', '8', null, null, null, '2016-07-25 17:53:17', 'admin', '1');
INSERT INTO `product` VALUES ('427', '', 'Fan belt - Kyron', '', 'units', 'Stockable', '', '15', '11', null, null, null, '2016-07-26 17:11:05', 'admin', '1');
INSERT INTO `product` VALUES ('428', '', 'Fan belt - Rexton', '', 'units', 'Stockable', '', '15', '2', null, null, null, '2016-07-29 14:13:19', 'admin', '1');
INSERT INTO `product` VALUES ('429', '', 'Fan clutch', '', 'units', 'Stockable', '', '7', '3', null, null, null, '2016-07-26 14:42:58', 'admin', '1');
INSERT INTO `product` VALUES ('430', '', 'Gear lever bush - Big', '', 'units', 'Stockable', '', '7', '27', null, null, null, '2016-07-29 14:52:40', 'admin', '1');
INSERT INTO `product` VALUES ('431', '', 'Gear lever bush - Small', '', 'units', 'Stockable', '', '7', '115', null, null, null, '2016-07-29 14:53:22', 'admin', '1');
INSERT INTO `product` VALUES ('432', '', 'Gear lever bush - 4 speed', '', 'units', 'Stockable', '', '15', '9', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('433', '', 'Injector washer', '', 'units', 'Stockable', '', '15', '25', null, null, null, '2016-07-30 12:57:17', 'admin', '1');
INSERT INTO `product` VALUES ('434', '', 'Ring seal ', '', 'units', 'Stockable', '', '7', '3', null, null, null, '2016-07-25 15:30:18', 'admin', '2');
INSERT INTO `product` VALUES ('435', '', 'Clamp washer', '', 'units', 'Stockable', '', '7', '9', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('436', '', 'Tappet cover packing - Kyron', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('437', '', 'Tappet cover packing - Rexton', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('438', '', 'Turbo oil packing (Used)', '', 'units', 'Stockable', '', '18', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('439', '', 'Wire tie - Big', '', 'units', 'Stockable', '', '15', '141', null, null, null, '2016-07-28 14:35:31', 'admin', '1');
INSERT INTO `product` VALUES ('440', '', 'Wire tie - Medium', '', 'units', 'Stockable', '', '15', '368', null, null, null, '2016-07-25 16:02:08', 'admin', '1');
INSERT INTO `product` VALUES ('441', '', 'Wire tie - Small', '', 'units', 'Stockable', '', '15', '211', null, null, null, '2016-07-25 16:06:42', 'admin', '1');
INSERT INTO `product` VALUES ('442', '', 'Brake hose front - (R/H)', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('443', '', 'Brake disk - Power Up Rear (G)', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('444', '', '4wd one way valve', '', 'units', 'Stockable', '', '15', '8', null, null, null, '2016-07-25 18:06:31', 'admin', '1');
INSERT INTO `product` VALUES ('445', '', 'Hose clip (8-16)', '', 'units', 'Stockable', '', '19', '32', null, null, null, '2016-07-25 15:36:26', 'admin', '1');
INSERT INTO `product` VALUES ('446', '', 'Hose clip (8-12)', '', 'units', 'Stockable', '', '19', '25', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('447', '', 'Hose clip (10-16)', '', 'units', 'Stockable', '', '19', '11', null, null, null, '2016-07-28 14:08:40', 'admin', '1');
INSERT INTO `product` VALUES ('448', '', 'Hose clip (16-27)', '', 'units', 'Stockable', '', '19', '60', null, null, null, '2016-07-28 14:08:27', 'admin', '1');
INSERT INTO `product` VALUES ('449', '', 'Hose clip (12-22)', '', 'units', 'Stockable', '', '19', '19', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('450', '', 'Hose clip (20-32)', '', 'units', 'Stockable', '', '19', '9', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('451', '', 'Hose clip (30-45)', '', 'units', 'Stockable', '', '19', '63', null, null, null, '2016-07-25 15:50:15', 'admin', '1');
INSERT INTO `product` VALUES ('452', '', 'Hose clip (32-50)', '', 'units', 'Stockable', '', '19', '16', null, null, null, '2016-07-26 14:44:24', 'admin', '1');
INSERT INTO `product` VALUES ('453', '', 'Hose clip (50-70)', '', 'units', 'Stockable', '', '19', '32', null, null, null, '2016-07-25 15:40:23', 'admin', '1');
INSERT INTO `product` VALUES ('454', '', 'Hose clip (60-80)', '', 'units', 'Stockable', '', '19', '23', null, null, null, '2016-07-25 15:38:00', 'admin', '1');
INSERT INTO `product` VALUES ('455', '', 'Hose clip (70-90)', '', 'units', 'Stockable', '', '19', '22', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('456', '', 'Hose clip (40-60)', '', 'units', 'Stockable', '', '19', '7', null, null, null, '2016-07-25 19:33:29', 'admin', '1');
INSERT INTO `product` VALUES ('457', '', 'Hose clip (80-100)', '', 'units', 'Stockable', '', '19', '7', null, null, null, '2016-07-25 15:47:29', 'admin', '1');
INSERT INTO `product` VALUES ('458', '', 'Camber bolt - Big', '', 'units', 'Stockable', '', '7', '47', null, null, null, '2016-07-25 18:16:10', 'admin', '1');
INSERT INTO `product` VALUES ('459', '', 'Camber bolt - Small', '', 'units', 'Stockable', '', '7', '32', null, null, null, '2016-07-25 18:14:18', 'admin', '1');
INSERT INTO `product` VALUES ('460', '', 'Cotter pin', '', 'units', 'Stockable', '', '7', '63', null, null, null, '2016-07-30 16:52:17', 'admin', '1');
INSERT INTO `product` VALUES ('461', '', 'Ball joint nut', '', 'units', 'Stockable', '', '6', '13', null, null, null, '2016-07-30 16:51:55', 'admin', '1');
INSERT INTO `product` VALUES ('462', '', 'Camber nut', '', 'units', 'Stockable', '', '7', '85', null, null, null, '2016-07-25 19:43:33', 'admin', '1');
INSERT INTO `product` VALUES ('463', '', 'Camber washer', '', 'units', 'Stockable', '', '7', '3', null, null, null, '2016-07-25 19:44:23', 'admin', '1');
INSERT INTO `product` VALUES ('464', '', 'Upper arm ball joint', '', 'units', 'Stockable', '', '7', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('465', '', 'Solenoid switch (Starter motor)', '', 'units', 'Stockable', '', '15', '0', null, null, null, '2016-07-30 12:57:58', 'admin', '1');
INSERT INTO `product` VALUES ('467', '', 'Reskit ', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('468', '', 'Clutch pump', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('469', '', 'Adjuster mid part with non groove pully', '', 'units', 'Stockable', '', '15', '6', null, null, null, '2016-07-29 14:12:12', 'admin', '1');
INSERT INTO `product` VALUES ('470', '', 'Adjuster shock', '', 'units', 'Stockable', '', '15', '36', null, null, null, '2016-07-30 12:57:37', 'admin', '1');
INSERT INTO `product` VALUES ('471', '', 'Alternator clutch', '', 'units', 'Stockable', '', '15', '11', null, null, null, '2016-07-29 14:11:52', 'admin', '1');
INSERT INTO `product` VALUES ('472', '', 'Alternator clutch (INNA)', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('473', '', 'Alternator clutch (G)', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('474', '', 'Air bag control unit', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('475', '', 'Power window motor - Rexton', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('476', '', 'Power window master switch - Rexton', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('478', '', 'Actuator lock hub', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('479', '', 'Power window switch rear - Rexton', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('480', '', 'Power mirror switch - Rexton', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('481', '', 'Oil pressure switch', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('482', '', 'Water separator', '', 'units', 'Stockable', '', '15', '3', null, null, null, '2016-07-29 13:38:28', 'admin', '1');
INSERT INTO `product` VALUES ('483', '', 'I/C Regulator', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('484', '', 'Cross bar bolt', '', 'units', 'Stockable', '', '15', '20', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('485', '', 'Remote arial', '', 'units', '', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('486', '', 'Clutch kit', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('487', '', 'Horn clip', '', 'units', 'Stockable', '', '15', '972', null, null, null, '2016-07-30 12:52:21', 'admin', '1');
INSERT INTO `product` VALUES ('488', '', 'Oil cooler \'O\' ring - Big', '', 'units', 'Stockable', '', '15', '10', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('489', '', 'Oil cooler \'O\' ring - Small', '', 'units', 'Stockable', '', '15', '18', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('490', '', 'Key blade', '', 'units', 'Stockable', '', '15', null, null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('491', '', 'Key', '', 'units', 'Stockable', '', '15', '6', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('492', '', 'Head light holder', '', 'units', 'Stockable', '', '15', '13', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('493', '', 'Fuse holder', '', 'units', 'Stockable', '', '15', '21', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('494', '', 'Gear selector lever', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('495', '', 'Gear selector lever', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('496', '', 'Signal flasher - Rexton', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('497', '', 'Signal flasher - Kyron', '', 'units', 'Service', '', '15', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('498', '', 'Wiper arm cap', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('500', '', 'Caliper clip', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('501', '', 'Wheel stud', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('502', '', 'Door contact switch ', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('503', '', 'Oil filler tube cap', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('504', '', 'Radiator top mount', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('505', '', 'Air cleaner mount', '', 'units', 'Stockable', '', '15', '6', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('506', '', 'Windshield washer nozzle', '', 'units', 'Stockable', '', '15', '11', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('507', '', 'Horn set - Yongwei', '', 'Box', 'Stockable', '', '15', '0', null, null, null, '2016-07-30 12:51:40', 'admin', '1');
INSERT INTO `product` VALUES ('508', '', 'Horn set - Marco', '', 'Box', 'Stockable', '', '15', '0', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('509', '', 'ABS module', '', 'units', 'Stockable', '', '17', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('510', '', 'Water pump', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('511', '', 'Idler housing', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('512', '', 'Gasket - Oil pan', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('513', '', 'Clutch master ', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('515', '', 'Brake booster', '', 'Box', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('516', '', 'Sump - Rexton', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('517', '', 'Front shocks - Kyron (G)', '', 'Box', 'Stockable', '', '20', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('518', '', 'Front shocks - Rexton (G)', '', 'units', 'Stockable', '', '20', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('519', '', 'Rear shock - Kyron', '', 'units', 'Stockable', '', '20', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('520', '', 'Rear shock - Rexton', '', 'units', 'Stockable', '', '20', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('521', '', 'Rear shock - Rexton', '', 'units', 'Stockable', '', '20', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('522', '', 'Rear shock - Rexton Power Up', '', 'units', 'Stockable', '', '20', '8', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('523', '', 'Rear shock - Korando', '', 'units', 'Stockable', '', '20', '2', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('524', '', 'Bonnet shock - Kyron', '', 'units', 'Stockable', '', '20', '3', null, null, null, '2016-07-26 15:16:22', 'admin', '1');
INSERT INTO `product` VALUES ('525', '', 'Dicky shock - Kyron', '', 'units', 'Stockable', '', '20', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('526', '', 'Dicky shock - Rexton', '', 'units', 'Stockable', '', '20', '10', null, null, null, '2016-07-26 15:16:53', 'admin', '1');
INSERT INTO `product` VALUES ('527', '', 'Front shocks - Kyron (Britpart)', '', 'units', 'Stockable', '', '20', '0', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('528', '', 'Dicky shock - Kyron', '', 'units', 'Stockable', '', '20', '4', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('529', '', 'Glass shocks', '', 'units', 'Stockable', '', '20', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('530', '', 'Glass shocks', '', 'units', 'Stockable', '', '20', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('531', '', 'Hood shock', '', 'units', 'Stockable', '', '20', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('532', '', 'Yoke bush', '', 'units', 'Stockable', '', '6', '35', null, null, null, '2016-07-26 14:26:20', 'admin', '1');
INSERT INTO `product` VALUES ('533', '', 'Front \'D\' bush', '', 'units', 'Stockable', '', '7', '22', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('534', '', 'Front link bush', '', 'units', 'Stockable', '', '7', null, null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('535', '', 'Upper arm complete set - (L/H)', '', 'units', 'Stockable', '', '7', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('536', '', 'Upper arm complete set - (R/H)', '', 'units', 'Stockable', '', '7', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('537', '', 'Tie rod end (L/H)', '', 'units', 'Stockable', '', '7', '5', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('538', '', 'Tie rod end (R/H)', '', 'units', 'Stockable', '', '7', '5', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('539', '', 'Front link (R/H)', '', 'units', 'Stockable', '', '7', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('540', '', 'Front \'D\' bush bracket', '', 'units', 'Stockable', '', '7', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('541', '', 'Rack mount', '', 'units', 'Stockable', '', '7', '7', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('542', '', 'Silancer mount - Rear ', '', 'units', 'Stockable', '', '15', '11', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('543', '', 'Silancer mount - Center', '', 'units', 'Stockable', '', '15', '10', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('544', '', 'Turbo - Kyron', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('545', '', 'Turbo - Rexton', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('546', '', 'Turbo - Rexton', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('547', '', 'Bonnet badge ', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('548', '', 'Radiator upper mount', '', 'units', 'Stockable', '', '15', '10', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('549', '', 'Radiator lower mount', '', 'units', 'Stockable', '', '15', '10', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('550', '', 'Silancer mount - Upper', '', 'units', 'Stockable', '', '15', '10', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('551', '', 'Wheel cup - Kyron', '', 'units', 'Stockable', '', '15', '0', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('552', '', 'Wheel cup - Rexton', '', 'units', 'Stockable', '', '15', '5', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('553', '', 'Oil cooler ', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('554', '', 'Brake oil - Dot 4', '', 'units', 'Stockable', '', '8', '67', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('555', '', 'RF-7 oil treatment (BG)', '', 'units', 'Stockable', '', '8', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('556', '', 'Radiator cooling system cleaner (BG)', '', 'units', 'Stockable', '', '8', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('557', '', 'Brake oil - Dot 3', '', 'units', 'Stockable', '', '8', '6', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('558', '', 'Diesel filter Free Lander 2 - (Coopers)', '', 'Box', 'Stockable', '', '22', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('559', '', 'Diesel filter Free Lander 2 - (County)', '', 'units', 'Stockable', '', '22', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('560', '', 'Diesel filter - Discovery 4  (G) 2.7L & 3L', '', 'Box', 'Stockable', '', '22', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('561', '', 'Air filter Free Lander 2 (MAHLE)', '', 'units', 'Stockable', '', '1', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('562', '', 'Selant (ABRO)', '', 'units', 'Stockable', '', '15', '10', null, null, null, '2016-07-30 12:55:07', 'admin', '1');
INSERT INTO `product` VALUES ('563', '', 'Petrol injector cleaner CF5 (BG)', '', 'Bottles', 'Stockable', '', '15', '12', null, null, null, '2016-07-25 15:23:18', 'admin', '1');
INSERT INTO `product` VALUES ('564', '', 'Oil filter - Prado JS (OE-116)', '', 'units', 'Stockable', '', '5', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('565', '', 'Brake pad set Front - Prado 150 (NIBK)', '', 'Box', 'Stockable', '', '15', '3', null, null, null, '2016-07-26 15:12:52', 'admin', '1');
INSERT INTO `product` VALUES ('566', '', 'Axle C.V boot - Prado', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('567', '', 'Vaccum Modulator', '', 'Box', 'Stockable', '', '15', '5', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('568', '', '10 mm bolt', '', 'units', 'Stockable', '', '23', '118', null, null, null, '2016-07-28 13:55:42', 'admin', '1');
INSERT INTO `product` VALUES ('569', '', '10 mm bolt (1.5 inch)', '', 'units', 'Stockable', '', '23', '4', null, null, null, '2016-07-30 13:15:01', 'admin', '1');
INSERT INTO `product` VALUES ('570', '', '10 mm washer', '', 'units', 'Stockable', '', '23', '69', null, null, null, '2016-07-30 13:15:14', 'admin', '1');
INSERT INTO `product` VALUES ('571', '', '10 mm nut', '', 'units', 'Stockable', '', '23', '111', null, null, null, '2016-07-30 13:16:20', 'admin', '1');
INSERT INTO `product` VALUES ('572', '', '10 mm washer - Big', '', 'units', 'Stockable', '', '23', '91', null, null, null, '2016-07-26 14:36:42', 'admin', '1');
INSERT INTO `product` VALUES ('573', '', '8 mm bolt', '', 'units', 'Stockable', '', '23', '36', null, null, null, '2016-07-30 15:12:03', 'admin', '1');
INSERT INTO `product` VALUES ('574', '', '8 mm nut', '', 'units', 'Stockable', '', '23', '31', null, null, null, '2016-07-30 15:12:11', 'admin', '1');
INSERT INTO `product` VALUES ('575', '', '8 mm washer', '', 'units', 'Stockable', '', '23', '39', null, null, null, '2016-07-26 14:39:19', 'admin', '1');
INSERT INTO `product` VALUES ('576', '', '5 mm Allen key bolt', '', 'units', 'Stockable', '', '23', '29', null, null, null, '2016-07-26 14:38:06', 'admin', '1');
INSERT INTO `product` VALUES ('577', '', 'Fan clutch bolt', '', 'units', 'Stockable', '', '23', '12', null, null, null, '2016-07-28 14:03:29', 'admin', '1');
INSERT INTO `product` VALUES ('578', '', 'Adjuster washer ', '', 'units', 'Stockable', '', '23', '82', null, null, null, '2016-07-26 20:30:02', 'admin', '1');
INSERT INTO `product` VALUES ('579', '', '12 mm washer', '', 'units', 'Stockable', '', '23', '21', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('580', '', '10 mm bolt (2 inch)', '', 'units', 'Stockable', '', '23', '24', null, null, null, '2016-07-26 14:36:58', 'admin', '1');
INSERT INTO `product` VALUES ('581', '', 'LED capless bulb', '', 'bulbs', 'Stockable', '', '3', '59', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('582', '', 'LED capless bulb - Blue', '', 'bulbs', 'Stockable', '', '3', '28', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('583', '', 'Dash board light LED - Blue', '', 'bulbs', 'Stockable', '', '3', '9', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('584', '', 'Dash board light LED - Yellow', '', 'bulbs', 'Stockable', '', '3', '9', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('587', '', 'Contact cleaner', '', 'Can', 'Stockable', '', '24', '1', null, null, null, '2016-07-26 19:55:54', 'admin', '1');
INSERT INTO `product` VALUES ('589', '', 'Carb & Choke cleaner ', '', 'Can', 'Stockable', '', '25', '6', null, null, null, '2016-07-26 20:12:25', 'admin', '1');
INSERT INTO `product` VALUES ('590', '', 'WD 40', '', 'Can', 'Stockable', '', '25', '10', null, null, null, '2016-07-26 20:09:14', 'admin', '1');
INSERT INTO `product` VALUES ('591', '', 'Max clean', '', 'Can', 'Stockable', '', '25', null, null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('592', '', 'Hand brake shoe', '', 'Box', 'Stockable', '', '15', '5', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('593', '', 'High pressure pump (fuel pump)', '', 'Box', 'Stockable', '', '15', null, null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('594', '', 'Power steering hose', '', 'Feet', 'Stockable', '', '15', '9', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('595', '', 'ATF fluid (Textamatic)', '', 'liters', 'Stockable', '', '8', '40', null, null, null, '2016-07-30 18:13:12', 'admin', '1');
INSERT INTO `product` VALUES ('596', '', 'MP 90 (Diff oil)', '', 'liters', 'Stockable', '', '8', '33', null, null, null, '2016-07-30 18:12:47', 'admin', '1');
INSERT INTO `product` VALUES ('597', '', 'Hub - Korando', '', 'Box', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('598', '', 'Hub - Korando', '', 'Box', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('599', '', 'Turbo timer (HKS)', '', 'Box', 'Stockable', '', '15', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('600', '', 'Brake pad set Front - KIA sportage (Mando)', '', 'units', 'Stockable', '', '2', '8', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('601', '', 'Brake pad set Rear - KIA sportage (Mando)', '', 'units', 'Stockable', '', '2', '5', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('602', '', 'Brake pad set -Front HI-Q korando & Sorento', '', 'Box', 'Stockable', '', '2', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('603', '', 'Brake pad set - Rear korando (G)', '', 'Box', 'Stockable', '', '2', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('604', '', 'Hub raser 2wd front with ABS-actiyon/kyron', '2', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('605', '', 'Hub raser 2wd front non ABS-actiyon/kyron', '2', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('606', '', 'Key blade', '2', 'units', 'Stockable', '', '15', '6', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('607', '', 'Lower arm ball joint', '10', 'Units', 'Stockable', '', '7', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('608', '', 'Front D bush', '15', 'units', 'Stockable', '', '6', '32', null, null, null, '2016-07-26 14:26:33', 'admin', '1');
INSERT INTO `product` VALUES ('609', '', 'Link rear power up', '5', 'units', 'Stockable', '', '7', '12', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('610', '', 'Rear \'D\' bush', '', 'units', 'Stockable', '', '15', '8', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('611', '', 'Link power up', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('612', '', 'Front shocks - Kyron (G)', '', 'Box', 'Stockable', '', '20', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('613', '', 'ABS Sensor adopter 4wd', '', 'units', 'Stockable', '', '10', '8', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('614', '', 'Air bag ribbon - Korando', '', 'Box', 'Stockable', '', '15', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('615', '', 'Side mirror glass - Kyron L/H', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('616', '', 'Side mirror glass - Rex L/H', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('617', '', 'Oil seperator', '', 'Box', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('618', '', 'Gear box oil filter - 5 Speed', '', 'Box', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('619', '', 'Hub raser 2wd front with ABS - Ky /Act.', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('620', '', 'Hub raser 2wd front with Non ABS - Ky /Act.', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('621', '', 'Wheel speed sensor 2wd Front - Ky/Act.', '', 'units', 'Stockable', '', '10', '0', null, null, null, '2016-07-26 13:31:49', 'admin', '1');
INSERT INTO `product` VALUES ('622', '', 'Wheel speed sensor Rear non ABS - Ky/Act.', '', 'units', 'Stockable', '', '10', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('623', '', 'Thermostat valve housing', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('624', '', 'ABS sensor 4wd', '', 'units', 'Stockable', '', '10', '7', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('625', '', 'A/C receiver drier', '', 'Box', 'Stockable', '', '15', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('626', '', 'A/C receiver drier (Used)', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('627', '', 'Air bag ribbon - Kyron', '', 'Box', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('628', '', 'Cam shaft', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('629', '', 'Timing chain guard (Guide LWR)', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('630', '', 'Timing chain guard (Guide Clamping)', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('631', '', 'Oil pump chain guard - Tentioner ', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('632', '', 'Air flow sensor Rexton - Petrol (Used)', '', 'Box', 'Stockable', '', '10', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('633', '', 'Air flow sensor Power Up', '', 'Box', 'Stockable', '', '10', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('634', '', 'EGR valve', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '2');
INSERT INTO `product` VALUES ('636', '', 'Brake disk - Front Kyron (KGC)', '', 'units', 'Stockable', '', '15', '2', null, null, null, '2016-07-25 17:27:18', 'admin', '1');
INSERT INTO `product` VALUES ('637', '', 'Brake disk - Rear Kyron / Rexton (G)', '', 'units', 'Stockable', '', '15', '2', '2.00', null, null, '2016-07-25 17:27:57', 'admin', '1');
INSERT INTO `product` VALUES ('638', '', 'C V Grease tube', '', 'units', 'Stockable', '', '15', '12', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('639', '', 'Rubber grease', '', 'Box', 'Stockable', '', '15', '11', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('640', '', 'Grease Cup - Sinopec', '', 'units', 'Stockable', '', '15', '1', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('641', '', 'Fix right ', '', 'units', 'Stockable', '', '15', '4', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('642', '', 'Tape - Black', '', 'units', 'Stockable', '', '15', '14', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('644', '', 'Thread seal', '', 'units', 'Stockable', '', '15', '2', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('645', '', 'Vacum Modulator', '', 'Box', 'Stockable', '', '15', '5', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('646', '', 'Idle pully without groove - Kyron', '', 'Box', 'Stockable', '', '15', '5', null, null, null, '2016-07-25 14:04:51', 'admin', '2');
INSERT INTO `product` VALUES ('647', '', 'Touch idle pully', '', 'units', 'Stockable', '', '15', '3', null, null, null, null, null, '1');
INSERT INTO `product` VALUES ('651', '', 'Gear selector ', '', 'Box', 'Stockable', null, '15', '0', null, null, '1', '2016-07-25 19:35:42', 'admin', '1');
INSERT INTO `product` VALUES ('652', '', 'Wiper washer tank - Rex (Recondition)', '', 'units', 'Stockable', null, '15', '2', null, null, '1', '2016-07-25 19:35:27', 'admin', '1');
INSERT INTO `product` VALUES ('653', '', 'Adjuster mid part without pully', '', 'units', 'Stockable', null, '15', '2', null, null, '1', null, null, '1');
INSERT INTO `product` VALUES ('654', '', 'A/C filter Suzuki waggon - R', '', 'Box', 'Stockable', null, '1', '5', null, null, '1', '2016-07-25 20:15:05', 'admin', '1');
INSERT INTO `product` VALUES ('655', '', 'Upper arm ball joint', '', 'units', 'Stockable', null, '7', '3', null, null, '1', '2016-07-26 13:04:34', 'admin', '1');
INSERT INTO `product` VALUES ('656', '', 'Battery terminal (+)', '', 'units', 'Stockable', null, '15', '10', null, null, '1', null, null, '1');
INSERT INTO `product` VALUES ('657', '', 'Battery terminal (-)', '', 'units', 'Stockable', null, '15', '10', null, null, '1', null, null, '1');
INSERT INTO `product` VALUES ('658', '', 'Rear \'D\' bush - Kyron', '', 'units', 'Stockable', null, '7', '9', null, null, '1', null, null, '1');
INSERT INTO `product` VALUES ('659', '', 'Radiator hose - Inlet (Kyron)', '', 'units', 'Stockable', null, '15', '11', null, null, '1', '2016-07-26 14:53:19', 'admin', '1');
INSERT INTO `product` VALUES ('660', '', 'Radiator hose - Inlet (Rexton)', '', 'units', 'Stockable', null, '15', '3', null, null, '1', null, null, '1');
INSERT INTO `product` VALUES ('661', '', 'A/C filter - Korando', '', 'Box', 'Stockable', null, '15', '2', null, null, '1', '2016-07-26 15:02:13', 'admin', '1');
INSERT INTO `product` VALUES ('662', '', 'Fly wheel (G)', '', 'Box', 'Stockable', null, '15', '1', null, null, '1', null, null, '1');
INSERT INTO `product` VALUES ('663', '', 'Tail light - Rexton (L/H)', '', 'Box', 'Stockable', null, '15', '2', null, null, '1', '2016-07-26 15:05:38', 'admin', '1');
INSERT INTO `product` VALUES ('664', '', 'Tail light - Rexton (R/H)', '', 'Box', 'Stockable', null, '15', '2', null, null, '1', null, null, '1');
INSERT INTO `product` VALUES ('665', '', 'Tail light - Kyron (L/H)', '', 'Box', 'Stockable', null, '15', '2', null, null, '1', '2016-07-26 15:06:32', 'admin', '1');
INSERT INTO `product` VALUES ('666', '', 'Tail light - Kyron (R/H)', '', 'Box', 'Stockable', null, '15', '2', null, null, '1', '2016-07-26 15:06:57', 'admin', '1');
INSERT INTO `product` VALUES ('667', '', 'Brake pad set Rear - Power Up (HI-Q)', '', 'Box', 'Stockable', null, '15', '2', null, null, '1', '2016-07-26 15:10:49', 'admin', '1');
INSERT INTO `product` VALUES ('668', '', 'Brake pad set Rear - Power Up (G)', '', 'units', 'Stockable', null, '15', '1', null, null, '1', '2016-07-26 15:11:55', 'admin', '1');
INSERT INTO `product` VALUES ('669', '', 'Hose - T/C to I/C', '', 'units', 'Stockable', null, '15', '2', null, null, '1', '2016-07-26 16:38:03', 'admin', '1');
INSERT INTO `product` VALUES ('670', '', 'Air cleaner hose', '', 'units', 'Stockable', null, '15', '1', null, null, '1', '2016-07-26 16:44:57', 'admin', '1');
INSERT INTO `product` VALUES ('671', '', 'Oil seperator hose', '', 'units', 'Stockable', null, '15', '1', null, null, '1', '2016-07-26 16:45:45', 'admin', '1');
INSERT INTO `product` VALUES ('672', '', 'Oil seperator hose', '', 'units', 'Stockable', null, '15', '1', null, null, '1', '2016-07-26 16:46:13', 'admin', '2');
INSERT INTO `product` VALUES ('673', '', 'Axle boot C.V', '', 'units', 'Stockable', null, '15', '3', null, null, '1', '2016-07-26 16:47:06', 'admin', '2');
INSERT INTO `product` VALUES ('674', '', 'Axle boot C.V', '', 'units', 'Stockable', null, '15', '10', null, null, '1', '2016-07-26 16:48:05', 'admin', '1');
INSERT INTO `product` VALUES ('675', '', 'Duct assy T/C to I/C', '', 'units', 'Stockable', null, '15', '1', null, null, '1', '2016-07-26 16:50:20', 'admin', '1');
INSERT INTO `product` VALUES ('676', '', 'Hose - T/C to Duct', '', 'units', 'Stockable', null, '15', '3', null, null, '1', '2016-07-26 16:51:05', 'admin', '2');
INSERT INTO `product` VALUES ('677', '', 'Hose assy 3way coolant', '', 'units', 'Stockable', null, '15', '2', null, null, '1', '2016-07-26 16:55:00', 'admin', '2');
INSERT INTO `product` VALUES ('678', '', 'Hose T/C to Engine', '', 'units', 'Stockable', null, '15', '3', null, null, '1', '2016-07-26 17:01:40', 'admin', '1');
INSERT INTO `product` VALUES ('679', '', 'Hose - T/C to I/C (Rexton)', '', 'units', 'Stockable', null, '15', '2', null, null, '1', '2016-07-26 17:02:30', 'admin', '1');
INSERT INTO `product` VALUES ('680', '', 'Hose - T/C to I/C (Kyron)', '', 'units', 'Stockable', null, '15', '1', null, null, '1', '2016-07-26 17:03:26', 'admin', '1');
INSERT INTO `product` VALUES ('681', '', 'Hose - T/C to I/C (Kyron)', '', 'units', 'Stockable', null, '15', '1', null, null, '1', '2016-07-26 17:03:54', 'admin', '2');
INSERT INTO `product` VALUES ('682', '', 'Valve seal', '', 'units', 'Stockable', null, '15', '22', null, null, '1', '2016-07-30 12:58:52', 'admin', '1');
INSERT INTO `product` VALUES ('683', '', 'A/C filter - Kyron', '', 'Box', 'Stockable', null, '15', '7', null, null, '1', null, null, '1');
INSERT INTO `product` VALUES ('684', '', 'A/C filter - Kyron', '', 'Box', 'Stockable', null, '15', '7', null, null, '1', '2016-07-26 17:22:52', 'admin', '1');
INSERT INTO `product` VALUES ('685', '', 'A/C filter - Rexton', '', 'Box', 'Stockable', null, '15', '10', null, null, '1', null, null, '2');
INSERT INTO `product` VALUES ('686', '', 'A/C filter - Rexton', '', 'Box', 'Stockable', null, '15', '10', null, null, '1', '2016-07-26 17:23:49', 'admin', '2');
INSERT INTO `product` VALUES ('687', '', 'A/C filter - Rexton (HCC)', '', 'Box', 'Stockable', null, '15', '2', null, null, '1', '2016-07-28 14:32:46', 'admin', '1');
INSERT INTO `product` VALUES ('688', '', 'Horn sleevings', '', 'units', 'Stockable', null, '15', '818', null, null, '1', '2016-07-30 12:52:38', 'admin', '1');
INSERT INTO `product` VALUES ('689', '', 'Injector pipe (CYL 3)', '', 'units', 'Stockable', null, '15', '9', null, null, '1', null, null, '1');
INSERT INTO `product` VALUES ('690', '', 'Injector pipe (CYL 1)', '', 'units', 'Stockable', null, '15', '2', null, null, '1', '2016-07-26 19:18:14', 'admin', '2');
INSERT INTO `product` VALUES ('691', '', 'Injector pipe (CYL 5)', '', 'units', 'Stockable', null, '15', '1', null, null, '1', '2016-07-26 19:19:58', 'admin', '2');
INSERT INTO `product` VALUES ('692', '', 'Injector pipe (CYL 1-3)', '', 'units', 'Stockable', null, '15', '4', null, null, '1', '2016-07-26 19:22:03', 'admin', '2');
INSERT INTO `product` VALUES ('693', '', 'Injector pipe (CYL 2 - 4)', '', 'units', 'Stockable', null, '15', '2', null, null, '1', '2016-07-26 19:23:50', 'admin', '2');
INSERT INTO `product` VALUES ('694', '', 'Carburetor & Injector cleaner  ', '', 'Can', 'Stockable', null, '24', '10', null, null, '1', '2016-07-26 19:55:30', 'admin', '1');
INSERT INTO `product` VALUES ('695', '', 'HHS 2000', '', 'Can', 'Stockable', null, '24', '1', null, null, '1', '2016-07-26 19:58:15', 'admin', '1');
INSERT INTO `product` VALUES ('696', '', 'Silicon spray', '', 'Can', 'Stockable', null, '24', '9', null, null, '1', '2016-07-26 19:57:49', 'admin', '1');
INSERT INTO `product` VALUES ('697', '', 'Coolant - Red (Wurth)', '', 'Bottles', 'Stockable', null, '24', '1', null, null, '1', '2016-07-26 19:59:34', 'admin', '1');
INSERT INTO `product` VALUES ('698', '', 'Coolant - Green (Wurth)', '', 'Bottles', 'Stockable', null, '24', '1', null, null, '1', '2016-07-26 20:00:15', 'admin', '1');
INSERT INTO `product` VALUES ('699', '', 'Keilriemen Belt spray', '', 'Can', 'Stockable', null, '24', '1', null, null, '1', '2016-07-26 20:01:08', 'admin', '1');
INSERT INTO `product` VALUES ('700', '', 'Radiator cooling system cleaner (BG)', '', 'Bottles', 'Stockable', null, '26', '4', null, null, '1', '2016-07-26 20:15:05', 'admin', '1');
INSERT INTO `product` VALUES ('701', '', 'Radiator hose - Inlet (Actyon)', '', 'units', 'Stockable', null, '15', '4', null, null, '1', null, null, '1');
INSERT INTO `product` VALUES ('702', '', 'Door winder - Front Rexton (R/H) ', '', 'units', 'Stockable', null, '15', '1', null, null, '1', '2016-07-28 13:13:41', 'admin', '2');
INSERT INTO `product` VALUES ('703', '', 'Air bag ribbon - Sorento KIA (G)', '', 'Box', 'Stockable', null, '15', '0', null, null, '1', '2016-07-28 14:06:05', 'admin', '1');
INSERT INTO `product` VALUES ('704', '', 'Air bag ribbon - Sorento KIA ', '', 'Box', 'Stockable', null, '15', '1', null, null, '1', '2016-07-28 14:05:53', 'admin', '1');
INSERT INTO `product` VALUES ('705', '', 'Clip', '', 'units', 'Stockable', null, '15', '109', null, null, '1', '2016-07-28 14:20:20', 'admin', '1');
INSERT INTO `product` VALUES ('706', '', '4wd solenoid', '', 'units', 'Stockable', null, '15', '2', null, null, '1', '2016-07-30 17:32:56', 'admin', '1');
INSERT INTO `product` VALUES ('707', '', 'Clip - Big', '', 'units', 'Stockable', null, '15', '10', null, null, '1', '2016-07-28 15:08:28', 'admin', '1');
INSERT INTO `product` VALUES ('708', '', 'Crank pully - Sorento', '', 'Box', 'Stockable', null, '15', '0', null, null, '1', null, null, '1');
INSERT INTO `product` VALUES ('709', '', 'Radiator over flow hose (Korando)', '', 'units', 'Stockable', null, '15', '0', null, null, '1', '2016-07-29 20:19:41', 'admin', '1');

-- ----------------------------
-- Table structure for `purchases_detail`
-- ----------------------------
DROP TABLE IF EXISTS `purchases_detail`;
CREATE TABLE `purchases_detail` (
  `idpurchases_detail` int(11) NOT NULL AUTO_INCREMENT,
  `Purchasing_Quantity` varchar(45) DEFAULT NULL,
  `Purchasing_Price` varchar(45) DEFAULT NULL,
  `Selling_Price` varchar(45) DEFAULT NULL,
  `Total_Amount` varchar(45) DEFAULT NULL,
  `product` int(11) NOT NULL,
  `purchases_header` int(11) NOT NULL,
  PRIMARY KEY (`idpurchases_detail`),
  KEY `fk_purchases_detail_product1_idx` (`product`) USING BTREE,
  KEY `fk_purchases_detail_purchases_header1_idx` (`purchases_header`) USING BTREE,
  CONSTRAINT `purchases_detail_ibfk_1` FOREIGN KEY (`product`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `purchases_detail_ibfk_2` FOREIGN KEY (`purchases_header`) REFERENCES `purchases_header` (`idpurchases_header`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of purchases_detail
-- ----------------------------
INSERT INTO `purchases_detail` VALUES ('7', '12', '', '', '', '122', '5');
INSERT INTO `purchases_detail` VALUES ('8', '12', '', '', '', '122', '6');
INSERT INTO `purchases_detail` VALUES ('9', '100', '', '', '', '124', '7');
INSERT INTO `purchases_detail` VALUES ('10', '10', '', '', '', '160', '8');
INSERT INTO `purchases_detail` VALUES ('11', '12', '', '', '', '161', '9');
INSERT INTO `purchases_detail` VALUES ('12', '20', '', '', '', '55', '10');
INSERT INTO `purchases_detail` VALUES ('13', '4', '', '', '', '32', '11');
INSERT INTO `purchases_detail` VALUES ('14', '10', '', '', '', '3', '12');
INSERT INTO `purchases_detail` VALUES ('16', '20', '', '', '', '424', '14');
INSERT INTO `purchases_detail` VALUES ('17', '10', '', '', '', '471', '15');
INSERT INTO `purchases_detail` VALUES ('18', '10', '', '', '', '349', '15');
INSERT INTO `purchases_detail` VALUES ('19', '2', '', '', '', '171', '15');
INSERT INTO `purchases_detail` VALUES ('20', '60', '', '', '', '127', '16');
INSERT INTO `purchases_detail` VALUES ('21', '3', '', '', '', '152', '17');
INSERT INTO `purchases_detail` VALUES ('22', '5', '', '', '', '3', '17');
INSERT INTO `purchases_detail` VALUES ('23', '2', '', '', '', '152', '18');
INSERT INTO `purchases_detail` VALUES ('24', '1', '', '', '', '429', '18');
INSERT INTO `purchases_detail` VALUES ('25', '1', '', '', '', '614', '18');
INSERT INTO `purchases_detail` VALUES ('26', '1', '', '', '', '142', '18');
INSERT INTO `purchases_detail` VALUES ('27', '1', '', '', '', '141', '18');
INSERT INTO `purchases_detail` VALUES ('28', '50', '', '', '', '448', '18');
INSERT INTO `purchases_detail` VALUES ('29', '1', '', '', '', '428', '18');
INSERT INTO `purchases_detail` VALUES ('30', '100', '', '', '', '124', '18');
INSERT INTO `purchases_detail` VALUES ('31', '24', '', '', '', '161', '18');
INSERT INTO `purchases_detail` VALUES ('32', '7', '', '', '', '119', '18');
INSERT INTO `purchases_detail` VALUES ('33', '4', '', '', '', '701', '19');
INSERT INTO `purchases_detail` VALUES ('34', '20', '', '', '', '417', '20');
INSERT INTO `purchases_detail` VALUES ('35', '2', '', '', '', '152', '21');
INSERT INTO `purchases_detail` VALUES ('36', '95', '', '', '', '118', '22');
INSERT INTO `purchases_detail` VALUES ('37', '20', '', '', '', '88', '23');
INSERT INTO `purchases_detail` VALUES ('38', '10', '', '', '', '113', '23');
INSERT INTO `purchases_detail` VALUES ('39', '2', '', '', '', '429', '23');
INSERT INTO `purchases_detail` VALUES ('40', '1', '', '', '', '121', '23');
INSERT INTO `purchases_detail` VALUES ('41', '2', '', '', '', '482', '23');
INSERT INTO `purchases_detail` VALUES ('42', '1', '', '', '', '510', '23');
INSERT INTO `purchases_detail` VALUES ('43', '3', '', '', '', '401', '23');
INSERT INTO `purchases_detail` VALUES ('44', '2', '', '', '', '402', '23');
INSERT INTO `purchases_detail` VALUES ('45', '3', '', '', '', '425', '23');

-- ----------------------------
-- Table structure for `purchases_header`
-- ----------------------------
DROP TABLE IF EXISTS `purchases_header`;
CREATE TABLE `purchases_header` (
  `idpurchases_header` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `purchases_no` varchar(45) DEFAULT NULL,
  `Total_Amount` varchar(45) DEFAULT NULL,
  `Date_Added` datetime DEFAULT NULL,
  `Added_By` varchar(45) DEFAULT NULL,
  `Date_Updated` datetime DEFAULT NULL,
  `Updated_By` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `supplier_idsupplier` int(11) NOT NULL,
  PRIMARY KEY (`idpurchases_header`),
  KEY `fk_purchases_header_supplier1_idx` (`supplier_idsupplier`) USING BTREE,
  CONSTRAINT `purchases_header_ibfk_1` FOREIGN KEY (`supplier_idsupplier`) REFERENCES `supplier` (`idsupplier`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of purchases_header
-- ----------------------------
INSERT INTO `purchases_header` VALUES ('5', '2016-06-22', '11:42:19', 'PN000001\n', null, '2016-06-22 11:42:19', '1', '2016-06-22 11:42:19', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('6', '2016-06-23', '03:33:37', 'PN000002\n', null, '2016-06-23 03:33:37', '1', '2016-06-23 03:33:37', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('7', '2016-07-04', '14:30:55', 'PN000003\n', null, '2016-07-04 14:30:55', '1', '2016-07-04 14:30:55', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('8', '2016-07-04', '15:26:16', 'PN000004\n', null, '2016-07-04 15:26:16', '1', '2016-07-04 15:26:16', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('9', '2016-07-04', '15:26:49', 'PN000005\n', null, '2016-07-04 15:26:49', '1', '2016-07-04 15:26:49', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('10', '2016-07-04', '15:27:25', 'PN000006\n', null, '2016-07-04 15:27:25', '1', '2016-07-04 15:27:25', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('11', '2016-07-04', '15:28:39', 'PN000007\n', null, '2016-07-04 15:28:39', '1', '2016-07-04 15:28:39', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('12', '2016-07-04', '17:10:02', 'PN000008\n', null, '2016-07-04 17:10:02', '1', '2016-07-04 17:10:02', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('14', '2016-07-07', '15:35:01', 'PN000009\n', null, '2016-07-07 15:35:01', '1', '2016-07-07 15:35:01', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('15', '2016-07-25', '21:03:37', 'PN000010\n', null, '2016-07-25 21:03:37', '1', '2016-07-25 21:03:37', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('16', '2016-07-25', '21:04:04', 'PN000011\n', null, '2016-07-25 21:04:04', '1', '2016-07-25 21:04:04', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('17', '2016-07-26', '13:15:29', 'PN000012\n', null, '2016-07-26 13:15:29', '1', '2016-07-26 13:15:29', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('18', '2016-07-28', '13:06:35', 'PN000013\n', null, '2016-07-28 13:06:35', '1', '2016-07-28 13:06:35', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('19', '2016-07-28', '13:15:30', 'PN000014\n', null, '2016-07-28 13:15:30', '1', '2016-07-28 13:15:30', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('20', '2016-07-28', '20:07:35', 'PN000015\n', null, '2016-07-28 20:07:35', '1', '2016-07-28 20:07:35', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('21', '2016-07-29', '13:22:51', 'PN000016\n', null, '2016-07-29 13:22:51', '1', '2016-07-29 13:22:51', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('22', '2016-07-30', '17:25:42', 'PN000017\n', null, '2016-07-30 17:25:42', '1', '2016-07-30 17:25:42', '1', 'DONE', '1');
INSERT INTO `purchases_header` VALUES ('23', '2016-07-30', '18:11:44', 'PN000018\n', null, '2016-07-30 18:11:44', '1', '2016-07-30 18:11:44', '1', 'DONE', '1');

-- ----------------------------
-- Table structure for `sub_category`
-- ----------------------------
DROP TABLE IF EXISTS `sub_category`;
CREATE TABLE `sub_category` (
  `idsub_category` int(11) NOT NULL AUTO_INCREMENT,
  `sub_category_name` varchar(45) DEFAULT NULL,
  `category` int(11) NOT NULL,
  PRIMARY KEY (`idsub_category`),
  KEY `fk_sub_category_category_idx` (`category`) USING BTREE,
  CONSTRAINT `sub_category_ibfk_1` FOREIGN KEY (`category`) REFERENCES `category` (`idcategory`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sub_category
-- ----------------------------
INSERT INTO `sub_category` VALUES ('1', 'Air filter', '1');
INSERT INTO `sub_category` VALUES ('2', 'Brake pads', '1');
INSERT INTO `sub_category` VALUES ('3', 'Bulbs', '1');
INSERT INTO `sub_category` VALUES ('4', 'Fuse', '1');
INSERT INTO `sub_category` VALUES ('5', 'Oil filter', '1');
INSERT INTO `sub_category` VALUES ('6', 'Suspension bush', '1');
INSERT INTO `sub_category` VALUES ('7', 'Suspension parts', '1');
INSERT INTO `sub_category` VALUES ('8', 'Oils', '1');
INSERT INTO `sub_category` VALUES ('10', 'Sesnsors', '1');
INSERT INTO `sub_category` VALUES ('11', 'Gaskets', '1');
INSERT INTO `sub_category` VALUES ('12', 'Coolent', '1');
INSERT INTO `sub_category` VALUES ('13', 'Oil seals', '1');
INSERT INTO `sub_category` VALUES ('14', 'Diesel filters', '1');
INSERT INTO `sub_category` VALUES ('15', 'Ssangyong Parts', '1');
INSERT INTO `sub_category` VALUES ('16', 'Wiper blades', '1');
INSERT INTO `sub_category` VALUES ('17', 'Door lock motors', '1');
INSERT INTO `sub_category` VALUES ('18', 'Used Parts', '1');
INSERT INTO `sub_category` VALUES ('19', 'Hose clips', '1');
INSERT INTO `sub_category` VALUES ('20', 'Shockabsober', '1');
INSERT INTO `sub_category` VALUES ('22', 'Land Rover', '1');
INSERT INTO `sub_category` VALUES ('23', 'Nut, Bolt & Washer', '1');
INSERT INTO `sub_category` VALUES ('24', 'Spray (WURTH)', '1');
INSERT INTO `sub_category` VALUES ('25', 'Spray Can', '1');
INSERT INTO `sub_category` VALUES ('26', 'BG Products', '1');

-- ----------------------------
-- Table structure for `supplier`
-- ----------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier` (
  `idsupplier` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(45) DEFAULT NULL COMMENT 'Supplier Name',
  `address` varchar(245) DEFAULT NULL,
  `contact_person` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `Fax` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsupplier`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of supplier
-- ----------------------------
INSERT INTO `supplier` VALUES ('1', 'Advanced', '1452/6, Malabe rd, Pannipitiya', '', '', '', '');
INSERT INTO `supplier` VALUES ('2', 'Missex International', 'Pagoda', '', '', '', '');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE,
  UNIQUE KEY `email` (`email`) USING BTREE,
  UNIQUE KEY `password_reset_token` (`password_reset_token`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'Dt8N6klRpIjtaHilHmFB-_in0Y7Z_6b-', '$2y$13$43u7l5HgrgaJ7aidNWZg4.iR/9/qt5h0L93NH364S/E8QrMPI5IZ2', null, 'akilamaxi@gmail.com', '10', '1465754738', '1465754738');

-- ----------------------------
-- Table structure for `warehouse`
-- ----------------------------
DROP TABLE IF EXISTS `warehouse`;
CREATE TABLE `warehouse` (
  `idwarehouse` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idwarehouse`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of warehouse
-- ----------------------------
INSERT INTO `warehouse` VALUES ('1', 'Advanced', 'A');
INSERT INTO `warehouse` VALUES ('2', 'Missex International', 'M');
